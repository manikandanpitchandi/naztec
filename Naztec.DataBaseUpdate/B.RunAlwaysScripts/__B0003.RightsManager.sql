-- /****************  RIGHTS MANAGER **************/

-- SecurityRights Insert Scripts 


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRights] WHERE Id = '1f538a63-4993-41d4-895c-9c2a0be43b4d')
   BEGIN
      INSERT INTO [dbo].[SecurityRights] ([Id], [SecurityRightDefinition], [IsHidden], [ApplicationSecurity], [DisplayOrder], [ParentRightId])
      VALUES (N'1f538a63-4993-41d4-895c-9c2a0be43b4d', N'Add other Users', 1, 0, 0, NULL)
   END

