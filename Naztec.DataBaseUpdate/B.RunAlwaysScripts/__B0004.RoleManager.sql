-- /****************  ROLE MANAGER **************/

-- SecurityRoles Insert Scripts 


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoles] WHERE Id = 'f4a18fc8-92e0-4152-902a-ea7447ed0aa0')
   BEGIN
      INSERT INTO [dbo].[SecurityRoles] ([Id], [Name])
      VALUES (N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'Admin')
   END




-- SecurityRoleMenu Insert Scripts 


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoleMenu] WHERE Id = '0571d536-f343-407b-8c12-067e1094b74d')
   BEGIN
      INSERT INTO [dbo].[SecurityRoleMenu] ([Id], [RoleId],[MenuId])
      VALUES (N'0571d536-f343-407b-8c12-067e1094b74d', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'51f9785a-8fc2-4b4c-a379-8290c61d1104')
   END


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoleMenu] WHERE Id = '267407d4-7c10-4140-b680-1a4838777f7d')
   BEGIN
      INSERT INTO [dbo].[SecurityRoleMenu] ([Id], [RoleId],[MenuId])
      VALUES (N'267407d4-7c10-4140-b680-1a4838777f7d', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'46e42a3c-739f-4c22-862c-6cfe24a58924')
   END


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoleMenu] WHERE Id = 'f0812913-6b2c-466b-a139-81d13c2f27fd')
   BEGIN
      INSERT INTO [dbo].[SecurityRoleMenu] ([Id], [RoleId],[MenuId])
      VALUES (N'f0812913-6b2c-466b-a139-81d13c2f27fd', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'9eb8cfb6-27aa-448f-837a-e99ea22007b4')
   END


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoleMenu] WHERE Id = 'bf686132-815a-4938-abf3-988a225518a4')
   BEGIN
      INSERT INTO [dbo].[SecurityRoleMenu] ([Id], [RoleId],[MenuId])
      VALUES (N'bf686132-815a-4938-abf3-988a225518a4', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'977c46e7-93d1-426e-9385-3b9bcb29f909')
   END


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoleMenu] WHERE Id = '2095abbc-0524-42ff-8d08-9e5676cd9e8d')
   BEGIN
      INSERT INTO [dbo].[SecurityRoleMenu] ([Id], [RoleId],[MenuId])
      VALUES (N'2095abbc-0524-42ff-8d08-9e5676cd9e8d', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'b751cfc1-50b8-43e5-b311-16676e20037f')
   END


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoleMenu] WHERE Id = '541cb50c-8403-4e56-9dab-ad03af71936f')
   BEGIN
      INSERT INTO [dbo].[SecurityRoleMenu] ([Id], [RoleId],[MenuId])
      VALUES (N'541cb50c-8403-4e56-9dab-ad03af71936f', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'b35d4d23-0cc7-4a84-af48-fa815bd6a81d')
   END


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoleMenu] WHERE Id = 'af872838-f1c3-483b-ae32-b9c48cf200c8')
   BEGIN
      INSERT INTO [dbo].[SecurityRoleMenu] ([Id], [RoleId],[MenuId])
      VALUES (N'af872838-f1c3-483b-ae32-b9c48cf200c8', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'17461ce1-6e6e-4f25-bc37-cf676194a978')
   END


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoleMenu] WHERE Id = 'aaa0961b-6f83-4bfe-bcec-bc31f2e5c3d9')
   BEGIN
      INSERT INTO [dbo].[SecurityRoleMenu] ([Id], [RoleId],[MenuId])
      VALUES (N'aaa0961b-6f83-4bfe-bcec-bc31f2e5c3d9', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'1085b5bf-efd0-42aa-9819-0a1abf9d3873')
   END


IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoleMenu] WHERE Id = '8d2253b8-adcd-402d-a232-e4dfbb1ba352')
   BEGIN
      INSERT INTO [dbo].[SecurityRoleMenu] ([Id], [RoleId],[MenuId])
      VALUES (N'8d2253b8-adcd-402d-a232-e4dfbb1ba352', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'39feef69-ccf6-43f0-82a5-104919335dc7')
   END



-- SecurityRoleRights Insert Scripts 

IF NOT EXISTS(SELECT * FROM [dbo].[SecurityRoleRights] WHERE Id = '657cb33a-87a9-4497-a5b7-124ea56f0f8c')
   BEGIN
      INSERT INTO [dbo].[SecurityRoleRights] ([Id], [SecurityRoleId],[SecurityRightId])
      VALUES (N'657cb33a-87a9-4497-a5b7-124ea56f0f8c', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', N'1f538a63-4993-41d4-895c-9c2a0be43b4d')
   END


-- SecurityUserRoles Insert Scripts 




IF NOT EXISTS(SELECT * FROM [dbo].[SecurityUserRoles] WHERE UserId = 'f7b9c817-a044-4f51-83d2-d09acf8b1975' AND RoleId = 'f4a18fc8-92e0-4152-902a-ea7447ed0aa0')
   BEGIN
      INSERT INTO [dbo].[SecurityUserRoles] ([UserId], [RoleId],[ExpiresOn])
      VALUES (N'f7b9c817-a044-4f51-83d2-d09acf8b1975', N'f4a18fc8-92e0-4152-902a-ea7447ed0aa0', NULL)
   END

