-- /****************  MENU MANAGER **************/

-- AppMenus Insert Scripts 


IF NOT EXISTS(SELECT * FROM [dbo].[AppMenus] WHERE Id = '977c46e7-93d1-426e-9385-3b9bcb29f909')
   BEGIN
      INSERT INTO [dbo].[AppMenus] ([Id], [ParentId], [Text], [Slug], [CssClass], [DisplayOrder], [IsActive])
      VALUES (N'977c46e7-93d1-426e-9385-3b9bcb29f909', NULL, N'Admin',NULL, N'fa fa-cogs', 2, 1)
   END


IF NOT EXISTS(SELECT * FROM [dbo].[AppMenus] WHERE Id = '1085b5bf-efd0-42aa-9819-0a1abf9d3873')
   BEGIN
      INSERT INTO [dbo].[AppMenus] ([Id], [ParentId], [Text], [Slug], [CssClass], [DisplayOrder], [IsActive])
      VALUES (N'1085b5bf-efd0-42aa-9819-0a1abf9d3873', N'977c46e7-93d1-426e-9385-3b9bcb29f909', N'Lists',N'/Admin/List/Manage', N'', 0, 1)
   END


IF NOT EXISTS(SELECT * FROM [dbo].[AppMenus] WHERE Id = '9eb8cfb6-27aa-448f-837a-e99ea22007b4')
   BEGIN
      INSERT INTO [dbo].[AppMenus] ([Id], [ParentId], [Text], [Slug], [CssClass], [DisplayOrder], [IsActive])
      VALUES (N'9eb8cfb6-27aa-448f-837a-e99ea22007b4', N'977c46e7-93d1-426e-9385-3b9bcb29f909', N'Logs',N'/Admin/Log/Manage', N'', 3, 1)
   END


IF NOT EXISTS(SELECT * FROM [dbo].[AppMenus] WHERE Id = '17461ce1-6e6e-4f25-bc37-cf676194a978')
   BEGIN
      INSERT INTO [dbo].[AppMenus] ([Id], [ParentId], [Text], [Slug], [CssClass], [DisplayOrder], [IsActive])
      VALUES (N'17461ce1-6e6e-4f25-bc37-cf676194a978', N'977c46e7-93d1-426e-9385-3b9bcb29f909', N'Menu',N'/Admin/Menu/Manage', N'', 2, 1)
   END


IF NOT EXISTS(SELECT * FROM [dbo].[AppMenus] WHERE Id = '51f9785a-8fc2-4b4c-a379-8290c61d1104')
   BEGIN
      INSERT INTO [dbo].[AppMenus] ([Id], [ParentId], [Text], [Slug], [CssClass], [DisplayOrder], [IsActive])
      VALUES (N'51f9785a-8fc2-4b4c-a379-8290c61d1104', N'977c46e7-93d1-426e-9385-3b9bcb29f909', N'Security',NULL, N'', 1, 1)
   END


IF NOT EXISTS(SELECT * FROM [dbo].[AppMenus] WHERE Id = 'b35d4d23-0cc7-4a84-af48-fa815bd6a81d')
   BEGIN
      INSERT INTO [dbo].[AppMenus] ([Id], [ParentId], [Text], [Slug], [CssClass], [DisplayOrder], [IsActive])
      VALUES (N'b35d4d23-0cc7-4a84-af48-fa815bd6a81d', N'51f9785a-8fc2-4b4c-a379-8290c61d1104', N'Rights',N'/Admin/Security/Rights', N'', 2, 1)
   END


IF NOT EXISTS(SELECT * FROM [dbo].[AppMenus] WHERE Id = '46e42a3c-739f-4c22-862c-6cfe24a58924')
   BEGIN
      INSERT INTO [dbo].[AppMenus] ([Id], [ParentId], [Text], [Slug], [CssClass], [DisplayOrder], [IsActive])
      VALUES (N'46e42a3c-739f-4c22-862c-6cfe24a58924', N'51f9785a-8fc2-4b4c-a379-8290c61d1104', N'Roles',N'/Admin/Security/Roles', N'', 1, 1)
   END


IF NOT EXISTS(SELECT * FROM [dbo].[AppMenus] WHERE Id = '39feef69-ccf6-43f0-82a5-104919335dc7')
   BEGIN
      INSERT INTO [dbo].[AppMenus] ([Id], [ParentId], [Text], [Slug], [CssClass], [DisplayOrder], [IsActive])
      VALUES (N'39feef69-ccf6-43f0-82a5-104919335dc7', N'51f9785a-8fc2-4b4c-a379-8290c61d1104', N'Users',N'/Admin/Security/Users', N'', 0, 1)
   END


IF NOT EXISTS(SELECT * FROM [dbo].[AppMenus] WHERE Id = 'b751cfc1-50b8-43e5-b311-16676e20037f')
   BEGIN
      INSERT INTO [dbo].[AppMenus] ([Id], [ParentId], [Text], [Slug], [CssClass], [DisplayOrder], [IsActive])
      VALUES (N'b751cfc1-50b8-43e5-b311-16676e20037f', NULL, N'Dashboard',N'/Dashboard/Index', N'fa fa-tachometer', 0, 1)
   END


