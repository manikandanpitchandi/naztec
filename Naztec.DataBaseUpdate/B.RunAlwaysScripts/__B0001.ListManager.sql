-- /****************  LIST MANAGER **************/

-- ListMaster Insert Scripts 


IF NOT EXISTS(SELECT * FROM [dbo].[ListMaster] WHERE Id = '63D3919F-69E2-40F1-A9F1-63C6CA476F55')
   BEGIN
      INSERT INTO [dbo].[ListMaster] ([Id], [MasterName], [ShowInListManager])
      VALUES(N'63D3919F-69E2-40F1-A9F1-63C6CA476F55', N'Country', 1)
   END


IF NOT EXISTS(SELECT * FROM [dbo].[ListDetail] WHERE Id = 'a6fe0adf-9534-4254-8bdd-09d4c32b65f1')
   BEGIN
      INSERT INTO [dbo].[ListDetail] ([Id], [MasterId], [Code], [Description])
      VALUES(N'29EE3A47-216A-4CC7-A41B-DEBDFF5741B5', N'63D3919F-69E2-40F1-A9F1-63C6CA476F55', N'123', N'Chennai')
   END


