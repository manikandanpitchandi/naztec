using System;
using System.Configuration;
using System.Linq;
using DbUp;
using System.Reflection;
using DbUp.Helpers;
using DbUp.Engine;

namespace Naztec.DataBaseUpdate
{
    class Program
    {
        public static int Main(string[] args)
        {
            var connectionString = args.FirstOrDefault() ?? ConfigurationManager.ConnectionStrings["BaseConnection"].ConnectionString;

            EnsureDatabase.For.SqlDatabase(connectionString);

            var resultA = CreateRunOnceEngine(connectionString).PerformUpgrade();

            if (!resultA.Successful)
            {
                OuputError(resultA);
                return -1;
            }

            var resultC = CreateReleaseScriptsEngine(connectionString).PerformUpgrade();

            if (!resultC.Successful)
            {
                OuputError(resultC);
                return -1;
            }

            var resultB = CreateRunAlwaysEngine(connectionString).PerformUpgrade();

            if (!resultB.Successful)
            {
                OuputError(resultB);
                return -1;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Success!");
            Console.ResetColor();
            return 0;
        }

        private static UpgradeEngine CreateRunOnceEngine(string connectionString)
        {
            var runOnceScriptBuilder =
                             DeployChanges.To
                                 .SqlDatabase(connectionString)
                                 .WithScriptsEmbeddedInAssembly(
                                 Assembly.GetExecutingAssembly(),
                                   s => s.Contains("__A")
                                );


            runOnceScriptBuilder.Configure(c =>
            {
                c.ScriptExecutor.ExecutionTimeoutSeconds = 30 * 60;
                Console.WriteLine($"Configure RunOnceEngine Timeout: {c.ScriptExecutor.ExecutionTimeoutSeconds} Seconds.");
            });

            return runOnceScriptBuilder.WithTransaction().LogToConsole().Build();
        }

        private static UpgradeEngine CreateRunAlwaysEngine(string connectionString)
        {
            var runAlwaysScriptBuilder =
                             DeployChanges.To
                                 .SqlDatabase(connectionString)
                                 .WithScriptsEmbeddedInAssembly(
                                 Assembly.GetExecutingAssembly(),
                                   s => s.Contains("__B")
                                ).JournalTo(new NullJournal());


            runAlwaysScriptBuilder.Configure(c =>
            {
                c.ScriptExecutor.ExecutionTimeoutSeconds = 30 * 60;
                Console.WriteLine($"Configure RunAlwaysEngine Timeout: {c.ScriptExecutor.ExecutionTimeoutSeconds} Seconds.");
            });

            return runAlwaysScriptBuilder.WithTransaction().LogToConsole().Build();
        }

        private static UpgradeEngine CreateReleaseScriptsEngine(string connectionString)
        {
            var releaseScriptsBuilder =
                             DeployChanges.To
                                 .SqlDatabase(connectionString)
                                 .WithScriptsEmbeddedInAssembly(
                                 Assembly.GetExecutingAssembly(),
                                   s => s.Contains("__C")
                                );


            releaseScriptsBuilder.Configure(c =>
            {
                c.ScriptExecutor.ExecutionTimeoutSeconds = 30 * 60;
                Console.WriteLine($"Configure Release Scripts Timeout: {c.ScriptExecutor.ExecutionTimeoutSeconds} Seconds.");
            });

            return releaseScriptsBuilder.WithTransaction().LogToConsole().Build();
        }

        private static void OuputError(DatabaseUpgradeResult result)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(result.Error);
            Console.ResetColor();

            #if DEBUG
            Console.ReadLine();
            #endif
        }
    }
}
