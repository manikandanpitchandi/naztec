-- Create Log Tables

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='Log' AND xtype='U')
BEGIN

	CREATE TABLE [dbo].[Log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Thread] [varchar](255) NOT NULL,
	[Level] [varchar](50) NOT NULL,
	[Logger] [varchar](255) NOT NULL,
	[Message] [varchar](4000) NOT NULL,
	[Exception] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END