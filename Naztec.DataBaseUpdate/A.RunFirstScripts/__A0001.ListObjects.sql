
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='ListMaster' AND xtype='U')
	BEGIN
		CREATE TABLE [dbo].[ListMaster](
			[Id] [uniqueidentifier] NOT NULL Default NEWID(),
			[MasterName] [nvarchar](256) NULL,
			[ShowInListManager] [bit] NOT NULL DEFAULT 0,
			CONSTRAINT [PK_ListMaster] PRIMARY KEY CLUSTERED 
			(
				[Id] ASC
			) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]
	END

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='ListDetail' AND xtype='U')
	BEGIN
		CREATE TABLE [dbo].[ListDetail](
			[Id] [uniqueidentifier] NOT NULL DEFAULT NEWID(),
			[MasterId] [uniqueidentifier] NOT NULL,
			[Code] [nvarchar](20) NULL,
			[Description] [nvarchar](256) NULL,
			CONSTRAINT [PK_ListDetail] PRIMARY KEY CLUSTERED 
			(
				[Id] ASC
			) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
			 CONSTRAINT [UK_ListDetail_MasterId_Code] UNIQUE NONCLUSTERED 
			(
				[MasterId] ASC,
				[Code] ASC
			) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
			 CONSTRAINT [UK_ListDetail_MasterId_Code_Description] UNIQUE NONCLUSTERED 
			(
				[MasterId] ASC,
				[Code] ASC,
				[Description] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		) ON [PRIMARY]		

		ALTER TABLE [dbo].[ListDetail]  
		WITH CHECK ADD CONSTRAINT [FK_ListDetail_ListMaster] FOREIGN KEY([MasterId])
		REFERENCES [dbo].[ListMaster] ([Id])

		ALTER TABLE [dbo].[ListDetail] CHECK CONSTRAINT [FK_ListDetail_ListMaster]
	END

