-- Create Identity Tables

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='SecurityRoles' AND xtype='U')
BEGIN

	CREATE TABLE [dbo].[SecurityRoles] (
		[Id]  [uniqueidentifier] NOT NULL DEFAULT (newid()),
		[Name] NVARCHAR (256) NOT NULL,
		CONSTRAINT [PK_dbo.SecurityRoles] PRIMARY KEY CLUSTERED ([Id] ASC)
	);

	CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex]
		ON [dbo].[SecurityRoles]([Name] ASC);

END

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='SecurityUsers' AND xtype='U')
BEGIN

	CREATE TABLE [dbo].[SecurityUsers](
		[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
		[Email] [nvarchar](256) NULL,
		[EmailConfirmed] [bit] NOT NULL,
		[PasswordHash] [nvarchar](max) NULL,
		[SecurityStamp] [nvarchar](max) NULL,
		[PhoneNumber] [nvarchar](max) NULL,
		[PhoneNumberConfirmed] [bit] NOT NULL,
		[TwoFactorEnabled] [bit] NOT NULL,
		[LockoutEndDateUtc] [datetime] NULL,
		[LockoutEnabled] [bit] NOT NULL,
		[AccessFailedCount] [int] NOT NULL,
		[UserName] [nvarchar](256) NOT NULL,
		[IsPasswordChanged] [bit] NOT NULL DEFAULT ((0)),
	 CONSTRAINT [PK_dbo.SecurityUsers] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex]
		ON [dbo].[SecurityUsers]([UserName] ASC);

END

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='SecurityUserClaims' AND xtype='U')
BEGIN

	CREATE TABLE [dbo].[SecurityUserClaims](
		[Id] [uniqueidentifier] NOT NULL,
		[UserId] [uniqueidentifier] NOT NULL,
		[ClaimType] [nvarchar](max) NULL,
		[ClaimValue] [nvarchar](max) NULL,
		CONSTRAINT [PK_dbo.SecurityUserClaims] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	ALTER TABLE [dbo].[SecurityUserClaims] ADD  DEFAULT (newid()) FOR [Id]

	ALTER TABLE [dbo].[SecurityUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SecurityUserClaims_dbo.SecurityUsers_UserId] FOREIGN KEY([UserId])
	REFERENCES [dbo].[SecurityUsers] ([Id])
	ON DELETE CASCADE


	ALTER TABLE [dbo].[SecurityUserClaims] CHECK CONSTRAINT [FK_dbo.SecurityUserClaims_dbo.SecurityUsers_UserId]

END

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='SecurityUserLogins' AND xtype='U')
BEGIN

	CREATE TABLE [dbo].[SecurityUserLogins](
		[LoginProvider] [nvarchar](128) NOT NULL,
		[ProviderKey] [nvarchar](128) NOT NULL,
		[UserId] [uniqueidentifier] NOT NULL,
	 CONSTRAINT [PK_dbo.SecurityUserLogins] PRIMARY KEY CLUSTERED 
	(
		[LoginProvider] ASC,
		[ProviderKey] ASC,
		[UserId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[SecurityUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SecurityUserLogins_dbo.SecurityUsers_UserId] FOREIGN KEY([UserId])
	REFERENCES [dbo].[SecurityUsers] ([Id])
	ON DELETE CASCADE

	ALTER TABLE [dbo].[SecurityUserLogins] CHECK CONSTRAINT [FK_dbo.SecurityUserLogins_dbo.SecurityUsers_UserId]

END

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='SecurityUserRoles' AND xtype='U')
BEGIN

	CREATE TABLE [dbo].[SecurityUserRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[ExpiresOn] [datetime] NULL,
	CONSTRAINT [PK_dbo.SecurityUserRoles] PRIMARY KEY CLUSTERED 
	(
	[UserId] ASC,
	[RoleId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[SecurityUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SecurityUserRoles_dbo.SecurityUsers_UserId] FOREIGN KEY([UserId])
	REFERENCES [dbo].[SecurityUsers] ([Id])
	ON DELETE CASCADE

	ALTER TABLE [dbo].[SecurityUserRoles] CHECK CONSTRAINT [FK_dbo.SecurityUserRoles_dbo.SecurityUsers_UserId]

	ALTER TABLE [dbo].[SecurityUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_SecurityUserRoles_SecurityRoles] FOREIGN KEY([RoleId])
	REFERENCES [dbo].[SecurityRoles] ([Id])

	ALTER TABLE [dbo].[SecurityUserRoles] CHECK CONSTRAINT [FK_SecurityUserRoles_SecurityRoles]

END

-- App tables

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='AppUsers' AND xtype='U')
BEGIN
		CREATE TABLE [dbo].[AppUsers](
		[Id] [uniqueidentifier] NOT NULL,
		[FirstName] [varchar](100) NOT NULL,
		[LastName] [varchar](100) NULL,
		[PhoneNumber] [varchar](12) NULL,
		[ImageId] [uniqueidentifier] NULL,
		[ProfileImage] [image] NULL,
		[Timezone][varchar](200) NULL,
	 CONSTRAINT [PK_AppUsers] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


	SET ANSI_PADDING OFF

	ALTER TABLE [dbo].[AppUsers]  WITH CHECK ADD  CONSTRAINT [FK_AppUsers_SecurityUsers] FOREIGN KEY([Id])
	REFERENCES [dbo].[SecurityUsers] ([Id])

	ALTER TABLE [dbo].[AppUsers] CHECK CONSTRAINT [FK_AppUsers_SecurityUsers]
END

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='AppMenus' AND xtype='U')
BEGIN
	CREATE TABLE [dbo].[AppMenus](
	[Id] [uniqueidentifier] NOT NULL,
	[ParentId] [uniqueidentifier] NULL,
	[Text] [varchar](50) NOT NULL,
	[Slug] [varchar](50) NULL,
	[CssClass] [varchar](100) NULL,
	[DisplayOrder] [tinyint] NOT NULL CONSTRAINT [DF_AppMenus_DisplayOrder]  DEFAULT ((0)),
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_AppMenus_IsActive]  DEFAULT ((1)),
	CONSTRAINT [PK_AppMenus] PRIMARY KEY CLUSTERED 
	(
	[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	SET ANSI_PADDING OFF

	ALTER TABLE [dbo].[AppMenus]  WITH CHECK ADD  CONSTRAINT [FK_AppMenus_AppMenus] FOREIGN KEY([ParentId])
	REFERENCES [dbo].[AppMenus] ([Id])

	ALTER TABLE [dbo].[AppMenus] CHECK CONSTRAINT [FK_AppMenus_AppMenus]

END

-- Security Rights

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='SecurityRights' AND xtype='U')
BEGIN
	
	CREATE TABLE [dbo].[SecurityRights](
		[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
		[SecurityRightDefinition] [nvarchar](1024) NOT NULL,
		[IsHidden] [bit] NOT NULL DEFAULT ((0)),
		[ApplicationSecurity] [bit] NOT NULL DEFAULT ((0)),
		[DisplayOrder] [int] NOT NULL DEFAULT ((0)),
		[ParentRightId] [uniqueidentifier] NULL,
		CONSTRAINT [PK_SecurityRights] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[SecurityRights]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRights_ParentRightId] FOREIGN KEY([ParentRightId])
	REFERENCES [dbo].[SecurityRights] ([Id])


	ALTER TABLE [dbo].[SecurityRights] CHECK CONSTRAINT [FK_SecurityRights_ParentRightId]

END

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='SecurityRoleRights' AND xtype='U')
BEGIN
	CREATE TABLE [dbo].[SecurityRoleRights](
		[Id] [uniqueidentifier] NOT NULL DEFAULT (newid()),
		[SecurityRoleId] [uniqueidentifier] NOT NULL,
		[SecurityRightId] [uniqueidentifier] NOT NULL,
	 CONSTRAINT [PK_SecurityRoleRights] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	ALTER TABLE [dbo].[SecurityRoleRights]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRoleRights_SecurityRightId] FOREIGN KEY([SecurityRightId])
	REFERENCES [dbo].[SecurityRights] ([Id])

	ALTER TABLE [dbo].[SecurityRoleRights] CHECK CONSTRAINT [FK_SecurityRoleRights_SecurityRightId]

	ALTER TABLE [dbo].[SecurityRoleRights]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRoleRights_SecurityRoleId] FOREIGN KEY([SecurityRoleId])
	REFERENCES [dbo].[SecurityRoles] ([Id])

	ALTER TABLE [dbo].[SecurityRoleRights] CHECK CONSTRAINT [FK_SecurityRoleRights_SecurityRoleId]

END

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name='SecurityRoleMenu' AND xtype='U')
BEGIN
	CREATE TABLE [dbo].[SecurityRoleMenu](
		[Id] [uniqueidentifier] NOT NULL,
		[RoleId] [uniqueidentifier] NOT NULL,
		[MenuId] [uniqueidentifier] NOT NULL,
	 CONSTRAINT [PK_SecurityRoleMenu] PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]


	ALTER TABLE [dbo].[SecurityRoleMenu]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRoleMenu_AppMenus] FOREIGN KEY([MenuId])
	REFERENCES [dbo].[AppMenus] ([Id])


	ALTER TABLE [dbo].[SecurityRoleMenu] CHECK CONSTRAINT [FK_SecurityRoleMenu_AppMenus]


	ALTER TABLE [dbo].[SecurityRoleMenu]  WITH CHECK ADD  CONSTRAINT [FK_SecurityRoleMenu_SecurityRoles] FOREIGN KEY([RoleId])
	REFERENCES [dbo].[SecurityRoles] ([Id])

	ALTER TABLE [dbo].[SecurityRoleMenu] CHECK CONSTRAINT [FK_SecurityRoleMenu_SecurityRoles]

END
