﻿using System;

namespace ECBase.Common.Library
{
    public static class Guard
    {
        public static void ForNullOrWhitespace(string value, string paramName)
        {
            if (value.IsNullOrWhiteSpace())
                throw new ArgumentException("Value cannot be null, empty, or whitespace", paramName);
        }

        public static void ForNullOrEmpty(string value, string paramName)
        {
            if (value.IsNullOrEmpty())
                throw new ArgumentException("Value cannot be null or empty", paramName);
        }

        public static void ForEmpty(Guid value, string paramName)
        {
            if (value == Guid.Empty)
                throw new ArgumentException("Value cannot be empty", paramName);
        }

        public static void ForNull(object value, string paramName)
        {
            if (value == null)
                throw new ArgumentNullException(paramName);
        }

        public static void ForEquality(object value1, object value2, string message)
        {
            if (value1.Equals(value2))
                throw new ArgumentException(message);
        }

        public static void ForInequality(object value1, object value2, string message)
        {
            if (!value1.Equals(value2))
                throw new ArgumentException(message);
        }

        public static void ForCondition(bool condition, string message, string paramName = null)
        {
            if (condition)
                throw new ArgumentException(message, paramName);
        }
    }
}
