﻿namespace Naztec.Automation
{
    using System;
    using System.Configuration;
    using System.IO;
    using Naztec.Automation.DependencyResolution;
    using Naztec.Domain.Contracts;
    using Naztec.Domain.Implementations;
    using Naztec.Domain.Implementations.AppStart;
    using Hangfire;
    using StructureMap;

    public class Program
    {
        private static IContainer container = new Container();

        static readonly string TemplateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "EmailTemplates");

        public enum DailyJobType
        {
            WelcomeMail = 2
        }

        public static IContainer Container
        {
            get { return container; }
        }

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += ProcessExitHandler;

            Initialize();

            SendProcessStartedEmail();

            using (var server = new BackgroundJobServer())
            {
                Console.WriteLine("================================================================================");
                Console.WriteLine("             Naztec Automation APP is running : Dont press any key.");
                Console.WriteLine("================================================================================");

                Console.ReadKey();
            }
        }

        static void ProcessExitHandler(object sender, EventArgs e)
        {
            SendProcessEndedEmail();
        }

        private static void Initialize()
        {
            AutoMapperConfiguration.Initialize(Program.Container);

            var hangfireConnectionString = ConfigurationManager.ConnectionStrings["HangfireConnection"].ConnectionString;
            var connectionString = ConfigurationManager.ConnectionStrings["BaseConnection"].ConnectionString;

            Ioc.Configure(Program.Container, connectionString);
            Program.Container.Configure(x => x.AddRegistry(new DefaultRegistry(connectionString)));

            HangfireWrapper hangfireWrapper = new HangfireWrapper();
            GlobalConfiguration.Configuration.UseSqlServerStorage(hangfireConnectionString);
            GlobalConfiguration.Configuration.UseActivator(new StructureMapJobActivator(Program.Container));

        }

        private static void SendProcessStartedEmail()
        {
            BackgroundJob.Enqueue<IEmailService>(x => x.AutomationAppStartEmail());
        }

        private static void SendProcessEndedEmail()
        {
            var emailService = Program.Container.GetInstance<IEmailService>();
            emailService.AutomationAppEndEmail();
        }
    }
}
