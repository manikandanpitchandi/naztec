﻿namespace Naztec.Automation.DependencyResolution
{
    using Naztec.Data.Implementations.UnitOfWorks;
    using Naztec.Domain.Implementations.Security;
    using StructureMap.Configuration.DSL;

    public class DefaultRegistry : Registry
    {
        private string connectionString;

        public DefaultRegistry(string connectionString) : base()
        {
            this.connectionString = connectionString;

            For<SecurityIdentityContext>().Use(() => this.GetIdentityContext());
            For<SecurityRoleRepository>().Use(() => this.GetRoleRepository());
            For<SecurityRoleServices>().Use(() => this.GetRoleServices());
            For<SecurityUserRepository>().Use(() => this.GetUserRepository());
            For<SecurityUserServices>().Use(() => this.GetUserServices());
        }

        private SecurityUserServices GetUserServices()
        {
            return new SecurityUserServices(this.GetUserRepository());
        }

        private SecurityRoleServices GetRoleServices()
        {
            return new SecurityRoleServices(this.GetRoleRepository());
        }

        private SecurityRoleRepository GetRoleRepository()
        {
            return new SecurityRoleRepository(this.GetIdentityContext());
        }

        private SecurityUserRepository GetUserRepository()
        {
            return new SecurityUserRepository(this.GetIdentityContext());
        }

        private SecurityIdentityContext GetIdentityContext()
        {
            return SecurityIdentityContext.Create(this.connectionString);
        }
    }
}
