﻿namespace Naztec.Domain.Contracts
{
    using Naztec.Core.Entities;
    using Naztec.Domain.Entities;
    using System;

    public interface IProfileService
    {
        NaztecResponse SaveProfile(UserProfile item);

        UserProfile GetProfile(string email);

        NaztecCUDResponse<ProfileImageItem> AddImages(ProfileImageItem item);

        UserProfile GetProfileImageByImageName(Guid ImageName);

        byte[] GetImageinByteArray(string imageString);
    }
}
