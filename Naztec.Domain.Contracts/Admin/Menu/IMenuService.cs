﻿namespace Naztec.Domain.Contracts
{
    using System.Collections.Generic;
    using Naztec.Core.Entities;
    using Naztec.Domain.Entities;
    using System;

    public interface IMenuService
    {
        List<MenuDTO> GetMenu();

        NaztecCUDResponse<MenuDTO> SaveMenu(MenuDTO dto);

        NaztecResponse DeleteMenu(Guid id);

        NaztecCUDResponse<MenuDTO> MoveMenu(Guid menuId, bool isMoveUp);
    }
}
