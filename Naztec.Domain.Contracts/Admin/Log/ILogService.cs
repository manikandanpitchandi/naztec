﻿namespace Naztec.Domain.Contracts
{
    using Naztec.Domain.Entities;
    using System.Collections.Generic;

    public interface ILogService
    {
        List<LogDTO> GetLog();
    }
}
