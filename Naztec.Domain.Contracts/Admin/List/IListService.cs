﻿namespace Naztec.Domain.Contracts
{
    using System;
    using System.Collections.Generic;
    using Naztec.Core.Entities;
    using Naztec.Domain.Entities;

    public interface IListService
    {
        List<ListMasterDTO> GetListMasters();        

        List<ListDetailDTO> GetListDetailsByMasterId(Guid masterId);


        NaztecCUDResponse<ListMasterDTO> SaveListMaster(ListMasterDTO dto);

        NaztecCUDResponse<ListDetailDTO> SaveListDetail(ListDetailDTO dto);


        NaztecCUDResponse<ListMasterDTO> DeleteListMasterById(Guid id);

        NaztecCUDResponse<ListDetailDTO> DeleteListDetailById(Guid id);
    }
}
