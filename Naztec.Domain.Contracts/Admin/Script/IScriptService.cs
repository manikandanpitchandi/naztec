﻿namespace Naztec.Domain.Contracts
{
    public interface IScriptService
    {
        string GenerateSQL(int moduleId);

        string GenerateCSharp(int moduleId);

        string GenerateJavascript(int moduleId);
    }
}
