﻿namespace Naztec.Domain.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Naztec.Core.Entities;
    using Naztec.Domain.Entities;
    using Entities.Admin.Security;

    public interface ISecurityManagementService
    {
        // Security Rights

        List<SecurityRightDTO> GetSecurityRights();

        NaztecCUDResponse<SecurityRightDTO> SaveSecurityRights(SecurityRightDTO dto);

        NaztecResponse DeleteRight(Guid id);

        NaztecCUDResponse<SecurityRightDTO> MoveRight(Guid rightId, bool isMoveUp);

        // Security Roles

        NaztecCUDResponse<SecurityRoleDTO> DeleteSecurityRole(Guid id);

        NaztecCUDResponse<SecurityRoleDTO> SaveSecurityRole(SecurityRoleDTO dto);

        ICollection<SecurityRoleDTO> GetSecurityRoles();

        ICollection<SecurityMenuDisplayDTO> GetSecurityRoleMenu(Guid roleId);

        NaztecResponse SaveSecurityRoleMenus(SecurityRoleMenuDTO dto);

        ICollection<NaztecGuidList> GetSecurityRoleUsers(Guid roleId);

        ICollection<NaztecGuidList> GetUnAssignedUsers(Guid roleId);

        NaztecCUDResponse<NaztecGuidList> RemoveUserFromRole(SecurityRoleUserDTO dto);

        NaztecCUDResponse<NaztecGuidList> AddUserToRole(SecurityRoleUserDTO dto);

        ICollection<SecurityRightDisplayDTO> GetSecurityRights(Guid roleId);

        NaztecResponse SaveSecurityRoleRights(SecurityRoleRightsDTO dto);

        // Security Menu

        ICollection<AppMenuDTO> GetMenu(List<Guid> roleId);

        List<UserDTO> GetUsers();

        Task<NaztecCUDResponse<UserDTO>> CreateUser(UserDTO item);

        NaztecCUDResponse<UserDTO> EditUser(UserDTO userDTO);

    }
}
