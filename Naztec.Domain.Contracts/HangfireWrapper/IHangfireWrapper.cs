﻿namespace Naztec.Domain.Contracts
{
    using Hangfire.Annotations;
    using System;
    using System.Linq.Expressions;

    public interface IHangfireWrapper
    {
        string Schedule<T>(Expression<Action<T>> methodCall, DateTimeOffset enqueueAt);

        string Enqueue<T>(Expression<Action<T>> methodCall);

        string Schedule<T>([InstantHandle][NotNull]Expression<Action<T>> methodCall, TimeSpan delay);
    }
}
