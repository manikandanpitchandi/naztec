﻿namespace Naztec.Domain.Contracts
{
    using System.Collections.Generic;
    using Naztec.Domain.Entities;

    public interface IInventoryService
    {
        List<InventoryDTO> GetInventory();
    }
}
