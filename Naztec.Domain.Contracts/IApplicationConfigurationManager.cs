﻿namespace Naztec.Domain.Contracts
{
    public interface IApplicationConfigurationManager
    {
        T GetValue<T>(string key);
    }
}
