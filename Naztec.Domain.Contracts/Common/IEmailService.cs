﻿namespace Naztec.Domain.Contracts
{
    using Naztec.Core.Entities;
    using Naztec.Domain.Entities;

    public interface IEmailService
    {
        NaztecResponse SendForgotPasswordEmail(ForgetPasswordEmail forgetPasswordEmail);

        NaztecResponse AutomationAppStartEmail();

        NaztecResponse AutomationAppEndEmail();
    }
}
