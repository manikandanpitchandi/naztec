using Naztec.Core.Entities;
using Naztec.Domain.Entities;
using System;
using System.Threading.Tasks;

namespace Naztec.Domain.Contracts
{
    public interface IAuthService
    {
        Task<LoginResponse> AppLogin(LoginRequest item);

        Task<ForgotPasswordResponse> ForgotPassword(ForgotPassword item);

        string EmailBodyForForgotPassword(string userName, string callbackUrl);

        Task<ResetPasswordResponse> ResetPassword(ResetPassword item);

        Task<LoginResponse> ChangePassword(ChangePassword item);

        Task<LoginResponse> WriteToUserClaims(Guid securityUserId, string claimType, string claimValue);
    }
}
