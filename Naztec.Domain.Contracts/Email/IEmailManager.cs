﻿namespace Naztec.Domain.Contracts
{
    using System.Collections.Generic;
    using Naztec.Core.Entities;
    using Entities;

    public interface IEmailManager
    {
        NaztecResponse SendMail(string sender, string recipient, string subject, string message, bool isHtml);

        NaztecResponse SendMail(string sender, string recipient, List<string> copyTo, string subject, string message, bool isHtml);

        NaztecResponse SendMail(NaztecEmailItem item);
    }
}
