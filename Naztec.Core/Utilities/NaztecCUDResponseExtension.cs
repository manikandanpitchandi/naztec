﻿namespace Naztec.Core.Utilities
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Naztec.Core.Entities;
    using FluentValidation;

    public static class NaztecCUDResponseExtension
    {
        public static NaztecCUDResponse<T> TryExecute<T>(this NaztecCUDResponse<T> response, Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                response.Error(ex.Message);
            }

            return response;
        }

        public static async Task<NaztecCUDResponse<T>> TryExecuteAsync<T>(this NaztecCUDResponse<T> response, Func<Task> action)
        {
            try
            {
                await action();
            }
            catch (Exception ex)
            {
                response.Error(ex.Message);
            }

            return response;
        }

        public static NaztecCUDResponse<T> On<T>(this NaztecCUDResponse<T> response, Func<bool> criteria, Action action)
        {
            if (criteria() == true)
            {
                response.TryExecute(action);
            }

            return response;
        }

        public static async Task<NaztecCUDResponse<T>> OnAsync<T>(this NaztecCUDResponse<T> response, Func<Task<bool>> criteria, Func<Task> action)
        {
            if ((await criteria()) == true)
            {
                await response.TryExecuteAsync(action);
            }

            return response;
        }

        public static NaztecCUDResponse<T> OnSuccess<T>(this NaztecCUDResponse<T> response, Action action)
        {
            if (response.IsOk)
            {
                response.TryExecute(action);
            }

            return response;
        }

        public static NaztecCUDResponse<T> OnSuccess<T>(this NaztecCUDResponse<T> response, string successMessage)
        {
            if (response.IsOk)
            {
                response.Success(successMessage);
            }

            return response;
        }

        public static async Task<NaztecCUDResponse<T>> OnSuccessAsync<T>(this NaztecCUDResponse<T> response, Func<Task> action)
        {
            if (response.IsOk)
            {
                await response.TryExecuteAsync(action);
            }

            return response;
        }

        public static NaztecCUDResponse<T> OnFail<T>(this NaztecCUDResponse<T> response, Action action)
        {
            if (!response.IsOk)
            {
                response.TryExecute(action);
            }

            return response;
        }

        public static NaztecCUDResponse<T> OnFail<T>(this NaztecCUDResponse<T> response, string failureMessage)
        {
            if (!response.IsOk)
            {
                response.Error(failureMessage);
            }

            return response;
        }

        public static async Task<NaztecCUDResponse<T>> OnFailAsync<T>(this NaztecCUDResponse<T> response, Func<Task> action)
        {
            if (!response.IsOk)
            {
                await response.TryExecuteAsync(action);
            }

            return response;
        }

        public static NaztecCUDResponse<T> OnWarning<T>(this NaztecCUDResponse<T> response, Action action)
        {
            if (response.ResponseType == ResponseTypes.Warning)
            {
                response.TryExecute(action);
            }

            return response;
        }

        public static NaztecCUDResponse<T> OnWarning<T>(this NaztecCUDResponse<T> response, string warningMessage)
        {
            if (response.ResponseType == ResponseTypes.Warning)
            {
                response.Warning(warningMessage);
            }

            return response;
        }

        public static async Task<NaztecCUDResponse<T>> OnWarningAsync<T>(this NaztecCUDResponse<T> response, Func<Task> action)
        {
            if (response.ResponseType == ResponseTypes.Warning)
            {
                await response.TryExecuteAsync(action);
            }

            return response;
        }

        public static NaztecCUDResponse<T> FailWhen<T>(this NaztecCUDResponse<T> response, bool criteria, string failureMessage)
        {
            if (criteria == false)
            {
                response.Error(failureMessage);
            }

            return response;
        }

        public static NaztecCUDResponse<T> FailWhen<T>(this NaztecCUDResponse<T> response, Func<bool> criteria, string failureMessage)
        {
            if (criteria() == false)
            {
                response.Error(failureMessage);
            }

            return response;
        }

        public static async Task<NaztecCUDResponse<T>> FailWhenAsync<T>(this NaztecCUDResponse<T> response, Func<Task<bool>> criteria, string failureMessage)
        {
            if ((await criteria()) == false)
            {
                response.Error(failureMessage);
            }

            return response;
        }

        public static bool HasData<T>(this NaztecCUDResponse<T> response)
        {
            return response.Item != null;
        }

        public static NaztecCUDResponse<T> WhenHasNoData<T>(this NaztecCUDResponse<T> response, string message)
        {
            response.FailWhen(response.HasData(), message);
            return response;
        }

        public static NaztecCUDResponse<T> Validate<T, TEntity>(this NaztecCUDResponse<T> response, AbstractValidator<TEntity> validator, TEntity entity)
        {
            response.TryExecute(() =>
            {
                var result = validator.Validate(entity);

                foreach (var error in result.Errors)
                {
                    response.Error(error.PropertyName, error.ErrorMessage);
                }
            });

            return response;
        }

        public static async Task<NaztecCUDResponse<T>> ValidateAsync<T, TEntity>(this NaztecCUDResponse<T> response, AbstractValidator<TEntity> validator, TEntity entity, CancellationToken cancellationToken = default(CancellationToken))
        {
            await response.TryExecuteAsync(async () =>
            {
                var result = await validator.ValidateAsync(entity, cancellationToken);

                foreach (var error in result.Errors)
                {
                    response.Error(error.PropertyName, error.ErrorMessage);
                }
            });

            return response;
        }
    }
}
