﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Naztec.Core.Utilities
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string s)
        {
            return String.IsNullOrEmpty(s);
        }

        public static bool IsNullOrWhiteSpace(this string s)
        {
            return String.IsNullOrWhiteSpace(s);
        }

        public static string WithDefault(this string s, string defaultValue)
        {
            return String.IsNullOrWhiteSpace(s) ? defaultValue : s;
        }

        public static String ToBase64String(this String s)
        {
            return s.IsNullOrEmpty() ? s : Convert.ToBase64String(Encoding.Unicode.GetBytes(s));
        }

        public static String DecodeBase64String(this String s)
        {
            return s.IsNullOrEmpty() ? s : Encoding.Unicode.GetString(Convert.FromBase64String(s));
        }

        public static String NullWhenEmpty(this String s)
        {
            return (s != null && s.Length == 0) ? null : s;
        }

        public static string CleanApostrophe(this String s)
        {
            if (s != null && s!="")
            {
                return s.Replace("'", "''");
            }
            else
            {
                return s;
            }
        }

        public static byte[] GetBytes(this string s)
        {
            var bytes = new byte[s.Length * sizeof(char)];
            Buffer.BlockCopy(s.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetNULLOrGuid(this Guid? s)
        {
            return (s == null) ? "NULL" : $"N'{s.Value.ToString()}'";
        }

        public static string GetNULLOrDate(this DateTime? s)
        {
            return (s == null) ? "NULL" : $"N'{s}'";
        }

        public static string GetBoolValue(this string s)
        {
            return (s == "True") ? "1" : "0";
        }

        public static string GetNULLOrString(this string s)
        {
            return (s == null) ? "NULL" : $"N'{s.ToString()}'";
        }

    }
}
