﻿namespace Naztec.Core.Utilities
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Naztec.Core.Entities;
    using FluentValidation;

    public static class NaztecResponseExtensions
    {
        public static NaztecResponse TryExecute(this NaztecResponse response, Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                response.Error(ex.Message);
            }

            return response;
        }

        public static async Task<NaztecResponse> TryExecuteAsync(this NaztecResponse response, Func<Task> action)
        {
            try
            {
                await action();
            }
            catch (Exception ex)
            {
                response.Error(ex.Message);
            }

            return response;
        }

        public static NaztecResponse On(this NaztecResponse response, Func<bool> criteria, Action action)
        {
            if (criteria() == true)
            {
                response.TryExecute(action);
            }

            return response;
        }

        public static async Task<NaztecResponse> OnAsync(this NaztecResponse response, Func<Task<bool>> criteria, Func<Task> action)
        {
            if ((await criteria()) == true)
            {
                await response.TryExecuteAsync(action);
            }

            return response;
        }

        public static NaztecResponse OnSuccess(this NaztecResponse response, Action action)
        {
            if (response.IsOk)
            {
                response.TryExecute(action);
            }

            return response;
        }

        public static NaztecResponse OnSuccess(this NaztecResponse response, string successMessage)
        {
            if (response.IsOk)
            {
                response.Success(successMessage);
            }

            return response;
        }

        public static async Task<NaztecResponse> OnSuccessAsync(this NaztecResponse response, Func<Task> action)
        {
            if (response.IsOk)
            {
                await response.TryExecuteAsync(action);
            }

            return response;
        }

        public static NaztecResponse OnFail(this NaztecResponse response, Action action)
        {
            if (!response.IsOk)
            {
                response.TryExecute(action);
            }

            return response;
        }

        public static NaztecResponse OnFail(this NaztecResponse response, string failureMessage)
        {
            if (!response.IsOk)
            {
                response.Error(failureMessage);
            }

            return response;
        }

        public static async Task<NaztecResponse> OnFailAsync(this NaztecResponse response, Func<Task> action)
        {
            if (!response.IsOk)
            {
                await response.TryExecuteAsync(action);
            }

            return response;
        }

        public static NaztecResponse OnWarning(this NaztecResponse response, Action action)
        {
            if (response.ResponseType == ResponseTypes.Warning)
            {
                response.TryExecute(action);
            }

            return response;
        }

        public static NaztecResponse OnWarning(this NaztecResponse response, string warningMessage)
        {
            if (response.ResponseType == ResponseTypes.Warning)
            {
                response.Warning(warningMessage);
            }

            return response;
        }

        public static async Task<NaztecResponse> OnWarningAsync(this NaztecResponse response, Func<Task> action)
        {
            if (response.ResponseType == ResponseTypes.Warning)
            {
                await response.TryExecuteAsync(action);
            }

            return response;
        }

        public static NaztecResponse FailWhen(this NaztecResponse response, bool criteria, string failureMessage)
        {
            if (criteria == false)
            {
                response.Error(failureMessage);
            }

            return response;
        }

        public static NaztecResponse FailWhen(this NaztecResponse response, Func<bool> criteria, string failureMessage)
        {
            if (criteria() == false)
            {
                response.Error(failureMessage);
            }

            return response;
        }

        public static async Task<NaztecResponse> FailWhenAsync(this NaztecResponse response, Func<Task<bool>> criteria, string failureMessage)
        {
            if ((await criteria()) == false)
            {
                response.Error(failureMessage);
            }

            return response;
        }

        public static NaztecResponse Validate<TEntity>(this NaztecResponse response, AbstractValidator<TEntity> validator, TEntity entity)
        {
            response.TryExecute(() =>
            {
                var result = validator.Validate(entity);

                foreach (var error in result.Errors)
                {
                    response.Error(error.PropertyName, error.ErrorMessage);
                }
            });

            return response;
        }

        public static async Task<NaztecResponse> ValidateAsync<TEntity>(this NaztecResponse response, AbstractValidator<TEntity> validator, TEntity entity, CancellationToken cancellationToken = default(CancellationToken))
        {
            await response.TryExecuteAsync(async () =>
            {
                var result = await validator.ValidateAsync(entity, cancellationToken);

                foreach (var error in result.Errors)
                {
                    response.Error(error.PropertyName, error.ErrorMessage);
                }
            });

            return response;
        }
    }
}
