namespace Naztec.Core.Utilities
{
    using System;

    public class SystemTime
    {
        private static Func<DateTime> now = () => DateTime.Now;
        private static Func<DateTime> utcNow = () => DateTime.UtcNow;

        private static Func<DateTimeOffset> offsetNow = () => DateTimeOffset.Now;
        private static Func<DateTimeOffset> offsetUtcNow = () => DateTimeOffset.UtcNow;

        public static Func<DateTime> Now
        {
            get { return now; }
            set { now = value; }
        }

        public static Func<DateTime> UtcNow
        {
            get { return utcNow; }
            set { utcNow = value; }
        }

        public static Func<DateTimeOffset> OffsetNow
        {
            get { return offsetNow; }
            set { offsetNow = value; }
        }

        public static Func<DateTimeOffset> OffsetUtcNow
        {
            get { return offsetUtcNow; }
            set { offsetUtcNow = value; }
        }
    }
}
