namespace Naztec.Core.Entities
{
    public enum ResponseTypes
    {
        Success = 0,
        Warning = 1,
        Error = 2
    }
}
