namespace Naztec.Core.Entities
{
    public class NaztecErrorMessage
    {
        public string Name { get; set; }

        public string Message { get; set; }
    }
}
