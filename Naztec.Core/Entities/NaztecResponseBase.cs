namespace Naztec.Core.Entities
{
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.AspNet.Identity;

    public abstract class NaztecResponseBase
    {
        private string successMessage;
        private string warningMessage;
        private List<NaztecErrorMessage> errorMessages;

        protected NaztecResponseBase()
        {
            this.errorMessages = new List<NaztecErrorMessage>();
        }

        public List<NaztecErrorMessage> ErrorMessages
        {
            get
            {
                return this.errorMessages;
            }

            private set
            {
            }
        }

        public bool IsOk
        {
            get { return this.ErrorMessages.Count == 0; }

            private set { }
        }

        public ResponseTypes ResponseType
        {
            get
            {
                if (this.IsOk)
                {
                    if (!string.IsNullOrWhiteSpace(this.successMessage))
                    {
                        return ResponseTypes.Success;
                    }
                    else if (!string.IsNullOrWhiteSpace(this.warningMessage))
                    {
                        return ResponseTypes.Warning;
                    }
                }

                return ResponseTypes.Error;
            }

            private set
            {
            }
        }

        public string GetHtmlMessage()
        {
            switch (this.ResponseType)
            {
                case ResponseTypes.Success:
                    return this.successMessage;

                case ResponseTypes.Warning:
                    return this.warningMessage;

                case ResponseTypes.Error:

                    var htmlMessage = new StringBuilder();

                    htmlMessage.Append("<ul>");

                    foreach (var item in this.ErrorMessages)
                    {
                        htmlMessage.AppendFormat("<li>{0}</li>", item.Message);
                    }

                    htmlMessage.Append("</ul>");

                    return htmlMessage.ToString();

                default:
                    return string.Empty;
            }
        }

        public string GetTextMessage()
        {
            switch (this.ResponseType)
            {
                case ResponseTypes.Success:
                    return this.successMessage;

                case ResponseTypes.Warning:
                    return this.warningMessage;

                case ResponseTypes.Error:

                    var textMessage = new StringBuilder();

                    foreach (var item in this.ErrorMessages)
                    {
                        textMessage.AppendFormat("{0}", item.Message).AppendLine();
                    }

                    return textMessage.ToString();

                default:
                    return string.Empty;
            }
        }

        public void Error(string message)
        {
            this.errorMessages.Add(new NaztecErrorMessage { Name = string.Empty, Message = message });
        }

        public void Error(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.Error(error);
            }
        }

        public void Error(string name, string message)
        {
            this.errorMessages.Add(new NaztecErrorMessage { Name = name, Message = message });
        }

        public void Success(string message)
        {
            this.successMessage = message;
        }

        public void Warning(string message)
        {
            this.warningMessage = message;
        }
    }
}
