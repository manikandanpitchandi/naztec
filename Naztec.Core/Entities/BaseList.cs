﻿namespace Naztec.Core.Entities
{
    public class BaseList<TId, TValue>
    {
        public TId Id { get; set; }

        public TValue Value { get; set; }

        public bool IsSelected { get; set; }
    }
}
