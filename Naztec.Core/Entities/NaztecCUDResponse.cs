namespace Naztec.Core.Entities
{
    using System;

    public class NaztecCUDResponse<T> : NaztecResponseBase
    {
        public NaztecCUDResponse()
        {
        }

        public Guid Id { get; set; }

        public T Item { get; set; }
    }
}
