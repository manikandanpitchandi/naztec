﻿namespace Naztec.Core.Helpers
{
    using System;

    public static class Helpers
    {
        public static string GetRandomPassword()
        {
            return RandomString.GenerateRandomAlphaNumeric(5, RandomString.AlphaUpper, RandomString.AlphaLower + RandomString.Digits) + RandomString.GenerateRandomAlphaNumeric(3, RandomString.AlphaLower, RandomString.AlphaUpper + RandomString.Digits) + RandomString.GenerateRandomAlphaNumeric(3, "~!@#$%^&*()+-", RandomString.AlphaLower + RandomString.AlphaUpper + RandomString.Digits) + RandomString.GenerateRandomNumeric(4);
        }

        public static Guid ToGuid(this string value)
        {
            Guid returnValue = Guid.Empty;
            Guid.TryParse(value, out returnValue);

            return returnValue;
        }
    }
}
