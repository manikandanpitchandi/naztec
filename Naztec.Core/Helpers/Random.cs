﻿namespace Naztec.Core.Helpers
{
    using System;

    public class RandomString
    {
        public const string AlphaUpper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public const string AlphaLower = "abcdefghijklmnopqrstuvwxyz";
        public const string Digits = "0123456789";

        private static Random random;

        public static string GenerateRandomAlphaNumeric(int length, string additionalCharacters = "", string excludedCharacters = "")
        {
            if (random == null)
            {
                random = new Random();
            }

            string chars = AlphaLower + AlphaUpper + Digits + additionalCharacters;

            foreach (var ch in excludedCharacters.ToCharArray())
            {
                chars = chars.Replace(ch.ToString(), string.Empty);
            }

            string result = string.Empty;

            while (result.Length < length)
            {
                int i = random.Next(0, chars.Length);

                result += chars.ToCharArray()[i];
            }

            return result;
        }

        public static string GenerateRandomNumeric(int length)
        {
            return GenerateRandomAlphaNumeric(length, string.Empty, AlphaUpper + AlphaLower);
        }
    }
}
