﻿namespace Naztec.Core.Helpers
{
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class TimeZoneHelper
    {
        public static DateTime ConvertDateTimeToUTC(DateTime dateTime, string timeZone)
        {
            var userTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZone);

            var utcDate = TimeZoneInfo.ConvertTimeToUtc(DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified), userTimeZone);

            return utcDate;
        }

        public static DateTime ConvertDateTimeFromUTC(DateTime dateTime, string timeZone)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, TimeZoneInfo.FindSystemTimeZoneById(timeZone));
        }

        public static List<NaztecStringItem> GetTimeZoneList()
        {
            return TimeZoneHelper.GetTimeZoneList(string.Empty);
        }

        public static List<NaztecStringItem> GetTimeZoneList(string selectedTimeZone)
        {
            var usZones = TimeZoneHelper.GetDefaultTimeZones();

            var timeZoneList = usZones.Select(x => new NaztecStringItem() { Id = x.Id, Value = x.StandardName }).ToList();

            if (!string.IsNullOrWhiteSpace(selectedTimeZone))
            {
                timeZoneList.Where(x => x.Value == selectedTimeZone).Select(x => x).FirstOrDefault().IsSelected = true;
            }
            else
            {
                timeZoneList.Where(x => x.Value == "Eastern Standard Time").Select(x => x).FirstOrDefault().IsSelected = true;
            }

            return timeZoneList;
        }

        public static string GetTzAbbreviation(string timeZoneName)
        {
            string output = string.Empty;

            string[] timeZoneWords = timeZoneName.Split(' ');
            foreach (string timeZoneWord in timeZoneWords)
            {
                if (timeZoneWord[0] != '(')
                {
                    output += timeZoneWord[0];
                }
                else
                {
                    output += timeZoneWord;
                }
            }

            return output;
        }

        private static List<TimeZoneInfo> GetDefaultTimeZones()
        {
            return new List<TimeZoneInfo>
                                                {
                                                    TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"),
                                                    TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"),
                                                    TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time"),
                                                    TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time"),
                                                    TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"),
                                                    TimeZoneInfo.FindSystemTimeZoneById("E. Europe Standard Time"),
                                                    TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time"),
                                                    TimeZoneInfo.FindSystemTimeZoneById("W. Europe Standard Time")
                                                };
        }
    }
}
