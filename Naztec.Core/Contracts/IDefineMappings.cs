namespace Naztec.Core.Contracts
{
    using AutoMapper;

    public interface IDefineMappings
    {
        void CreateMappings(IConfiguration configuration);
    }
}
