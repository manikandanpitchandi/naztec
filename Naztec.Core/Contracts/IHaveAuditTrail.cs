namespace Naztec.Core.Contracts
{
    using System;

    public interface IHaveAuditTrail
    {
        int CreatedBy { get; set; }

        DateTime CreatedOn { get; set; }

        bool IsDeleted { get; set; }

        int? DeletedBy { get; set; }

        DateTime? DeletedOn { get; set; }
    }
}
