namespace Naztec.Data.Contracts
{
    using System;
    using Naztec.Core.Entities;

    public interface IUnitOfWork : IDisposable
    {
        NaztecResponse SaveChanges();

        void Dispose(bool disposing);

        IRepository<T> Repository<T>() where T : class;

        void BeginTransaction();

        NaztecResponse Commit();

        void Rollback();
    }
}
