namespace Naztec.Data.Contracts
{
    public interface IMultiTenantUnitOfWorkFactory<Tkey>
    {
        IUnitOfWork GetUnitOfWork(Tkey key);
    }
}
