namespace Naztec.Data.Contracts
{
    public interface IConnectionStringProvider<TKey>
    {
        string GetConnectionString(TKey key);
    }
}
