namespace Naztec.Data.Contracts
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IFluentQueryable<T> where T : class
    {
        IFluentQueryable<T> OrderBy(Func<IQueryable<T>, IOrderedQueryable<T>> orderBy);

        IFluentQueryable<T> Include(Expression<Func<T, object>> expression);

        IQueryable<T> SelectPage(int page, int pageSize, out int totalCount);

        IQueryable<TResult> Select<TResult>(Expression<Func<T, TResult>> selector = null);

        IQueryable<T> Select();

        IQueryable<T> SqlQuery(string query, params object[] parameters);
    }
}
