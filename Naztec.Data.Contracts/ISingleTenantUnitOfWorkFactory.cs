namespace Naztec.Data.Contracts
{
    public interface ISingleTenantUnitOfWorkFactory
    {
        IUnitOfWork GetUnitOfWork();
    }
}
