﻿namespace Naztec.Domain.Entities
{
    public class ForgotPassword
    {
        public string Email { get; set; }
    }
}
