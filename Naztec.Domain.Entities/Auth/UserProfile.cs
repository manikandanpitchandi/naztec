﻿namespace Naztec.Domain.Entities
{
    using System;

    public class UserProfile
    {
        public Guid Id { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public Guid? ImageId { get; set; }

        public byte[] ProfileImage { get; set; }
    }
    
}
