﻿namespace Naztec.Domain.Entities
{
    using System;

    public class ProfileImageItem
    {
        public Guid AppUserId { get; set; }

        public Guid? ImageName { get; set; }

        public Guid? PreviousImageName { get; set; }

        public byte[] ImageBinaryData { get; set; }
    }
}
