namespace Naztec.Domain.Entities
{
    using System;
    using System.Security.Claims;
    using Naztec.Core.Entities;
    using System.Collections.Generic;

    public class LoginResponse : NaztecResponseBase
    {
        public string FullName { get; set; }

        public Guid SecurityUserId { get; set; }        

        public ClaimsIdentity UserClaims { get; set; }

        public Guid? ImageId { get; set; }

        public List<Guid> Roles { get; set; }
    }
}
