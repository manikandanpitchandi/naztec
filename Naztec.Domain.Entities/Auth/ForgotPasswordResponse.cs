﻿namespace Naztec.Domain.Entities
{
    using Naztec.Core.Entities;

    public class ForgotPasswordResponse : NaztecResponseBase
    {
        public string UserName { get; set; }

        public string Email { get; set; }

        public string Token { get; set; }
    }
}
