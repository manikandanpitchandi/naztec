﻿namespace Naztec.Domain.Entities
{
    using System;

    public class MenuDTO : BaseDTO
    {
        public Guid Id { get; set; }

        public bool IsActive { get; set; }

        public byte DisplayOrder { get; set; }

        public Guid? ParentId { get; set; }

        public string CssClass { get; set; }

        public string Slug { get; set; }

        public string Text { get; set; }
    }
}
