﻿namespace Naztec.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class ListDetailDTO : BaseDTO
    {
        public ListDetailDTO()
        {            
        }

        public Guid Id { get; set; }

        public Guid MasterId { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public ListMasterDTO ListMaster { get; set; }
    }
}
