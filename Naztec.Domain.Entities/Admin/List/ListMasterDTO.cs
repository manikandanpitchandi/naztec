﻿using System;
using System.Collections.Generic;

namespace Naztec.Domain.Entities
{
    public class ListMasterDTO : BaseDTO
    {
        public Guid Id { get; set; }

        public string MasterName { get; set; }

        public bool ShowInListManager { get; set; }
    }
}
