﻿namespace Naztec.Domain.Entities
{
    using System;

    public class SecurityRightDisplayDTO : BaseDTO
    {
        public SecurityRightDisplayDTO()
        {
            this.State = new StateDTO();
        }

        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public string Parent
        {
            get { return this.ParentId != null ? Convert.ToString(this.ParentId) : "#"; }
        }

        public string Description { get; set; }

        public int DisplayOrder { get; set; }

        public StateDTO State { get; set; }
    }
}
