﻿namespace Naztec.Domain.Entities.Admin.Security
{
    using System;

    public class SecurityRoleUserDTO : BaseDTO
    {
        public Guid RoleId { get; set; }

        public Guid UserId { get; set; }
    }
}
