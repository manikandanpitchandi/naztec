﻿namespace ECBase.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using ECBase.Core.Entities;

    public class SecurityGroupRightsDTO : BaseDTO
    {
        public Guid SecurityGroupId { get; set; }

        public List<ECGuidList> SecurityRightsList { get; set; }
    }
}
