﻿namespace Naztec.Domain.Entities
{
    public class StateDTO : BaseDTO
    {
        public bool Selected { get; set; }
    }
}
