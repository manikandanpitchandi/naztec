﻿

namespace Naztec.Domain.Entities
{
    using System;

    public partial class AppMenuDTO : BaseDTO
    {
        public Guid Id { get; set; }

        public string Text { get; set; }

        public string Description { get; set; }

        public Guid? ParentMenuId { get; set; }

        public string MenuType { get; set; }

        public int? PageId { get; set; }

        public int? MenuOrder { get; set; }

        public bool? IsActive { get; set; }

        public string Class { get; set; }

        public string Slug { get; set; }

        public bool IsSelected { get; set; }

        public bool IsActiveParent { get; set; }

        public bool IsRendered { get; set; }
    }
}
