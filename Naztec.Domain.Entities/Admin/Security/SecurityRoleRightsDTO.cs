﻿namespace Naztec.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    using Naztec.Core.Entities;

    public class SecurityRoleRightsDTO : BaseDTO
    {
        public Guid SecurityRoleId { get; set; }

        public List<NaztecGuidList> SecurityRightsList { get; set; }
    }
}
