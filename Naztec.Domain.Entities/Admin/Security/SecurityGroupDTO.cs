﻿namespace ECBase.Domain.Entities.Admin.Security
{
    using System;

    public class SecurityGroupDTO : BaseDTO
    {
        public Guid Id { get; set; }
        public string SecurityGroupDescription { get; set; }
        public string SecurityGroupName { get; set; }
    }
}
