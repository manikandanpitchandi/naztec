﻿namespace ECBase.Domain.Entities.Admin.Security
{
    using System;

    public class SecurityGroupUserDTO : BaseDTO
    {
        public Guid Id { get; set; }

        public Guid SecurityGroupId { get; set; }

        public Guid SecurityUserId { get; set; }
    }
}
