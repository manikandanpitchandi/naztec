﻿using System;

namespace Naztec.Domain.Entities
{
    public class SecurityMenuDisplayDTO : BaseDTO
    {
        public SecurityMenuDisplayDTO()
        {
            this.State = new StateDTO();
        }

        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        public string Parent
        {
            get { return this.ParentId != null ? Convert.ToString(this.ParentId) : "#"; }
        }

        public string Text { get; set; }

        public int DisplayOrder { get; set; }

        public StateDTO State { get; set; }
    }
}
