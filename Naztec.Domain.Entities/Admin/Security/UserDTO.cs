﻿namespace Naztec.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class UserDTO : BaseDTO
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public Guid? ImageId { get; set; }
        public string Email { get; set; }
        public List<string> Roles { get; set; }
        public string Timezone { get; set; }
    }
}
