﻿namespace Naztec.Domain.Entities
{
    using System;

    public class SecurityRoleDTO : BaseDTO
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
