﻿namespace Naztec.Domain.Entities
{
    using System;

    public class SecurityRightDTO : BaseDTO
    {
        public Guid Id { get; set; }

        public bool ApplicationSecurity { get; set; }

        public bool IsHidden { get; set; }

        public Guid? ParentRightId { get; set; }

        public int DisplayOrder { get; set; }

        public string SecurityRightDefinition { get; set; }
    }
}
