﻿namespace Naztec.Domain.Entities.Admin.Security
{
    using System;
    using System.Collections.Generic;
    using Naztec.Core.Entities;

    public class SecurityRoleMenuDTO : BaseDTO
    {
        public Guid SecurityRoleId { get; set; }

        public List<NaztecGuidList> MenuList { get; set; }
    }
}
