﻿namespace Naztec.Domain.Entities
{
    using System;
    public class InventoryDTO : BaseDTO
    {
        public InventoryDTO()
        {
        }

        public Guid InventoryId { get; set; }

        public string PartNumber { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }
    }
}
