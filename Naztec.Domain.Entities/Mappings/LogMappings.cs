﻿namespace Naztec.Domain.Entities.Mappings
{
    using AutoMapper;
    using Naztec.Core.Contracts;
    using Naztec.Data.Entities;

    public class LogMappings : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<Log, LogDTO>();
            configuration.CreateMap<LogDTO, Log>();

            configuration.CreateMap<Inventory, InventoryDTO>();
            configuration.CreateMap<InventoryDTO, Inventory>();
        }
    }
}
