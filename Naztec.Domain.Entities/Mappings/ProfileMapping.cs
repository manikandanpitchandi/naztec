﻿namespace Naztec.Domain.Entities.Mappings
{
    using AutoMapper;
    using Data.Entities;
    using Naztec.Core.Contracts;

    public class ProfileMapping : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<UserProfile, AppUser>();
            configuration.CreateMap<AppUser, UserProfile>();
        }
    }
}
