namespace Naztec.Domain.Entities.Mappings
{
    using AutoMapper;
    using Naztec.Core.Contracts;
    using Naztec.Data.Entities;

    public class ListMappings : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<ListDetail, ListDetailDTO>();
            configuration.CreateMap<ListDetailDTO, ListDetail>();

            configuration.CreateMap<ListMaster, ListMasterDTO>();
            configuration.CreateMap<ListMasterDTO, ListMaster>();
        }
    }
}
