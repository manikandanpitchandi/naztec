﻿namespace Naztec.Domain.Entities.Mappings
{
    using Admin.Security;
    using AutoMapper;
    using Naztec.Data.Entities;
    using Naztec.Core.Contracts;
    using Entities;

    public class SecurityManagementMapping : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<SecurityRight, SecurityRightDTO>();
            configuration.CreateMap<SecurityRightDTO, SecurityRight>();

            configuration.CreateMap<SecurityUser, UserDTO>();
            configuration.CreateMap<UserDTO, SecurityUser>();

            configuration.CreateMap<AppUser, UserDTO>();
            configuration.CreateMap<UserDTO, AppUser>();

            configuration.CreateMap<SecurityUserRole, SecurityRoleUserDTO>();
            configuration.CreateMap<SecurityRoleUserDTO, SecurityUserRole>();

            configuration.CreateMap<SecurityRole, SecurityRoleDTO>();
            configuration.CreateMap<SecurityRoleDTO, SecurityRole>();

            configuration.CreateMap<SecurityRoleMenu, SecurityRoleMenuDTO>();
            configuration.CreateMap<SecurityRoleMenuDTO, SecurityRoleMenu>();
            
        }
    }
}
