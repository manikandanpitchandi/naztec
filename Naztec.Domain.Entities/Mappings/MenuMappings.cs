﻿namespace Naztec.Domain.Entities.Mappings
{
    using AutoMapper;
    using Naztec.Data.Entities;
    using Naztec.Core.Contracts;

    public class MenuMappings : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<AppMenu, MenuDTO>();
            configuration.CreateMap<MenuDTO, AppMenu>();
        }
    }
}
