namespace Naztec.Domain.Entities
{
    public abstract class BaseKvpDTO<TKey, TValue>
    {
        public TKey Key { get; set; }

        public TValue Value { get; set; }

        public bool IsSelected { get; set; }
    }
}
