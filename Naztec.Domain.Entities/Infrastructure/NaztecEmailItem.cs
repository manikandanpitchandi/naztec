﻿namespace Naztec.Domain.Entities
{
    using System;
    using System.Collections.Generic;

    public class NaztecEmailItem : ICloneable
    {
        public NaztecEmailItem()
        {
            this.CC = new List<string>();
        }

        public string Sender { get; set; }

        public string Recipient { get; set; }

        public List<string> CC { get; set; }

        public string Subject { get; set; }

        public string MessageBody { get; set; }

        public bool IsHtml { get; set; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
