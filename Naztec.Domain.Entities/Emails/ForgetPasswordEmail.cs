﻿namespace Naztec.Domain.Entities
{
    public class ForgetPasswordEmail
    {
        public string Email { get; set; }

        public string CallbackUrl { get; set; }
    }
}
