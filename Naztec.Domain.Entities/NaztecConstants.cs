using System;

namespace Naztec.Domain.Entities
{
    public static class NaztecConstants
    {
        public static class Claims
        {
            public static string SkinTheme
            {
                get
                {
                    return "SKIN_THEME";
                }
            }
        }

        public static string FromEmail
        {
            get
            {
                return "FromEmail";
            }
        }

        public static string IsTestEnvironment
        {
            get
            {
                return "IsTestEnvironment";
            }
        }

        public static string TestSMTPServer
        {
            get
            {
                return "TestSMTPServer";
            }
        }

        public static string TestSMTPServerPort
        {
            get
            {
                return "TestSMTPServerPort";
            }
        }

        public static string TestSMTPServerUsername
        {
            get
            {
                return "TestSMTPServerUsername";
            }
        }

        public static string TestSMTPServerPassword
        {
            get
            {
                return "TestSMTPServerPassword";
            }
        }

        public static string DefaultApproverEmail
        {
            get
            {
                return "DefaultApproverEmail";
            }
        }

        public static string SMTPServer
        {
            get
            {
                return "SMTPServer";
            }
        }

        public static string SMTPServerPort
        {
            get
            {
                return "SMTPServerPort";
            }
        }

        public static class ProfileImage
        {
            public static string ServerImagePath
            {
                get { return "~/Images/profile/"; }
            }

            public static string JsImagePath
            {
                get { return "Images/profile/"; }
            }

            public static string DefaultImage
            {
                get { return "noimage.jpg"; }
            }
        }

        public static class EmailTemplateTypes
        {
            public static Guid WelcomeEmail
            {
                get
                {
                    return Guid.Parse("5DACABFD-836C-4002-9B25-2EE7018950CC");
                }
            }

            public static Guid ForgotPasswordManagerEmail
            {
                get
                {
                    return Guid.Parse("4B00240B-370C-4827-A9F6-D7B639581489");
                }
            }
        }

        public static class TimeInterval
        {
            public static string NotificationMailTimeInterval
            {
                get
                {
                    return "NotificationMailTimeInterval";
                }
            }

            public static string ReminderMailTimeInterval
            {
                get
                {
                    return "ReminderMailTimeInterval";
                }
            }
        }

        public static string SMTPServerUsername
        {
            get
            {
                return "SMTPServerUsername";
            }
        }

        public static string SMTPServerPassword
        {
            get
            {
                return "SMTPServerPassword";
            }
        }

        public static string FromEMail
        {
            get
            {
                return "FromEMail";
            }
        }

        public static string ApplicationSiteUrl
        {
            get
            {
                return "ApplicationSiteURL";
            }
        }

        public static string JobDashboardClaim
        {
            get
            {
                return "Naztec.JobDashboard";
            }
        }
    }
}
