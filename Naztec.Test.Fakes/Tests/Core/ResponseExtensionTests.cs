﻿namespace Naztec.Test.Tests.Core
{
    using System;
    using System.Threading.Tasks;
    using Naztec.Core.Entities;
    using Naztec.Core.Utilities;
    using FluentValidation;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ResponseExtensionTests
    {
        [TestMethod]
        public Task ResponseExtentionTest_Excercise_1()
        {
            var response = new NaztecResponse();

            response.TryExecute(() => { throw new NotImplementedException(); });

            Assert.IsFalse(response.IsOk);

            return Task.FromResult(1);
        }

        [TestMethod]
        public async Task ResponseExtentionTest_Excercise_2()
        {
            var response = new NaztecResponse();

            await response.TryExecuteAsync(() => { throw new NotImplementedException(); });

            Assert.IsFalse(response.IsOk);
        }


        [TestMethod]
        public Task ResponseExtentionTest_Excercise_3()
        {
            var response = new NaztecResponse();

            response.Error("Operation failed");

            response.OnSuccess(() => response.Success("Operation Success"));

            Assert.IsFalse(response.IsOk);

            return Task.FromResult(1);
        }

        [TestMethod]
        public Task ResponseExtensionTest_Excercise_4()
        {
            var response = new NaztecResponse();
            var value = -1;
            response.TryExecute(() => { throw new Exception(); })
                    .OnSuccess(() => response.Success("Executed successfully"))
                    .OnFail("Failed operation")
                    .OnWarning("Warning message")
                    .On(() => true, () =>
                    {
                        
                    })
                    .FailWhen(value < 0, "Value should be grater than zero")
                    .FailWhen(() => false, "Failure message from lambda");

            Assert.IsFalse(response.IsOk);

            return Task.FromResult(1);
        }

        private enum OrderStatusType
        {
            Placed,
            Packed,
            Shipped,
            Delivered,
            Returned
        }

        private class Order
        {
            public Guid OrderId { get; set; }
            public Guid CustomerId { get; set; }
            public OrderStatusType Status { get; set; }
        }

        private class OrderValidator : AbstractValidator<Order>
        {
            public OrderValidator(Order currentOrder)
            {
                RuleFor(x => x.CustomerId).NotEmpty().WithMessage("Customer Id is required");
                RuleFor(x => x.Status).Must(x => x == OrderStatusType.Packed).When(x => currentOrder != null && x.Status == OrderStatusType.Placed);
            }
        }

        public Task ResponseExtensionTests_Excercise_5()
        {
            var order = new Order();
            var currentOrder = new Order();

            var response = new NaztecResponse();

            response.Validate(new OrderValidator(order), currentOrder);

            Assert.IsFalse(response.IsOk);

            return Task.FromResult(1);
        }
    }
}
