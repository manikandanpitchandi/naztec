namespace Naztec.Test.Fakes
{
    using Naztec.Data.Contracts;
    using Moq;

    public class BaseTest
    {
        private Mock<TType> GetMultiTenantUnitOfWorkFactory<TType, TKey, TContext>(TContext context)
                                                           where TType : class, IMultiTenantUnitOfWorkFactory<TKey>
                                                           where TContext : FakeDbContext, new()
        {
            IUnitOfWork fakeUow;

            if (context == null)
            {
                fakeUow = new FakeUnitOfWork(new TContext());
            }
            else
            {
                fakeUow = new FakeUnitOfWork(context);
            }

            var mockUnitOfWorkFactory = new Mock<TType>();

            mockUnitOfWorkFactory.Setup(m => m.GetUnitOfWork(It.IsAny<TKey>())).Returns(fakeUow);

            return mockUnitOfWorkFactory;
        }

        private Mock<TType> GetSingleTenantUnitOfWorkFactory<TType, TContext>(TContext context)
                                                            where TType : class, ISingleTenantUnitOfWorkFactory
                                                            where TContext : FakeDbContext, new()
        {
            IUnitOfWork fakeUow;

            if (context == null)
            {
                fakeUow = new FakeUnitOfWork(new TContext());
            }
            else
            {
                fakeUow = new FakeUnitOfWork(context);
            }

            var mockUnitOfWorkFactory = new Mock<TType>();

            mockUnitOfWorkFactory.Setup(m => m.GetUnitOfWork()).Returns(fakeUow);

            return mockUnitOfWorkFactory;
        }
    }
}
