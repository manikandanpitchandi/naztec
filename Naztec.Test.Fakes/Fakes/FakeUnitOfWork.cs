namespace Naztec.Test.Fakes
{
    using System;
    using System.Collections.Generic;
    using Core.Entities;
    using Naztec.Data.Contracts;

    public class FakeUnitOfWork : IUnitOfWork
    {
        private readonly FakeDbContext context;
        private bool disposed;
        private Dictionary<string, object> repositories;

        public FakeUnitOfWork(FakeDbContext context)
        {
            this.context = context;
        }

        public NaztecResponse SaveChanges()
        {
            return new NaztecResponse();
        }

        public IRepository<T> Repository<T>() where T : class
        {
            if (this.repositories == null)
            {
                this.repositories = new Dictionary<string, object>();
            }

            var type = typeof(T).Name;

            if (this.repositories.ContainsKey(type))
            {
                return (IRepository<T>)this.repositories[type];
            }

            var repositoryType = typeof(FakeRepository<>);

            this.repositories.Add(type, Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), this.context));

            return (IRepository<T>)this.repositories[type];
        }

        public void BeginTransaction()
        {
        }

        public NaztecResponse Commit()
        {
            return new NaztecResponse();
        }

        public void Rollback()
        {
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.context.Dispose();
                }
            }

            this.disposed = true;
        }
    }
}
