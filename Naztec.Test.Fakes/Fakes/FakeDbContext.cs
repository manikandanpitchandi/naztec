namespace Naztec.Test.Fakes
{

    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    public class FakeDbContext : DbContext, IFakeDbContext
    {
        private readonly Dictionary<Type, Type> fakeDbSetTypes;
        private readonly Dictionary<Type, object> fakeDbSets;

        public FakeDbContext()
        {
            this.fakeDbSetTypes = new Dictionary<Type, Type>();
            this.fakeDbSets = new Dictionary<Type, object>();
        }

        public override int SaveChanges()
        {
            return default(int);
        }

        public override DbSet<T> Set<T>()
        {
            return (DbSet<T>)this.fakeDbSets[typeof(T)];
        }

        public void AddFakeDbSet<TEntity, TFakeDbSet>()
            where TEntity : class, new()
            where TFakeDbSet : FakeDbSet<TEntity>, IDbSet<TEntity>, new()
        {
            var fakeDbSet = Activator.CreateInstance<TFakeDbSet>();
            this.fakeDbSetTypes.Add(typeof(TEntity), typeof(TFakeDbSet));
            this.fakeDbSets.Add(typeof(TEntity), fakeDbSet);
        }

        public void Seed<T>(ICollection<T> items) where T : class, new()
        {
            var repo = this.fakeDbSets[typeof(T)];

            ((FakeDbSet<T>)repo).AddRange(items);
        }

        public void Seed<T>(T item) where T : class, new()
        {
            var repo = this.fakeDbSets[typeof(T)];

            ((FakeDbSet<T>)repo).Add(item);
        }

        public void Clear()
        {
            foreach (var dbSet in this.fakeDbSetTypes)
            {
                this.fakeDbSets[dbSet.Key] = Activator.CreateInstance(this.fakeDbSetTypes[dbSet.Key]);
            }
        }
    }
}
