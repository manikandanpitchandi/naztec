namespace Naztec.Test.AutoFixture
{
    using Ploeh.AutoFixture;

    public class OmitVirtualsCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customizations.Add(new OmitVirtuals());
        }
    }
}
