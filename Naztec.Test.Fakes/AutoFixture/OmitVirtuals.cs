namespace Naztec.Test.AutoFixture
{
    using System;
    using System.Reflection;
    using Ploeh.AutoFixture.Kernel;

    public class OmitVirtuals : ISpecimenBuilder
    {
        public object Create(object request, ISpecimenContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            var propInfo = request as PropertyInfo;
            if (propInfo == null)
            {
                return new NoSpecimen();
            }

            if (propInfo.GetGetMethod().IsVirtual)
            {
                return null;
            }

            return new NoSpecimen();
        }
    }
}
