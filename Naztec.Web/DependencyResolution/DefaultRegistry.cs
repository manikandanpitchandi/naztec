// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DefaultRegistry.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Naztec.Web.DependencyResolution
{
    using System.Web;
    using Infrastructure.Contracts;
    using Infrastructure.Implementations;
    using Data.Implementations.UnitOfWorks;
    using Domain.Implementations.Security;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;

    using StructureMap.Configuration.DSL;
    using StructureMap.Graph;

    public class DefaultRegistry : Registry {
        #region Constructors and Destructors

        public DefaultRegistry() {
            Scan(
                scan => {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
					scan.With(new ControllerConvention());
                });

            For<IApplicationStore>().Use<ApplicationStore>();
            For<IAuthenticationManager>().Use(() => HttpContext.Current.GetOwinContext().Authentication);
            For<SecurityIdentityContext>().Use(() => HttpContext.Current.GetOwinContext().Get<SecurityIdentityContext>());
            For<SecurityRoleServices>().Use(() => HttpContext.Current.GetOwinContext().Get<SecurityRoleServices>());
            For<SecuritySignInServices>().Use(() => HttpContext.Current.GetOwinContext().Get<SecuritySignInServices>());
            For<SecurityUserServices>().Use(() => HttpContext.Current.GetOwinContext().Get<SecurityUserServices>());
            For<ISessionStore>().Use<SessionStore>();

        }

        #endregion
    }
}
