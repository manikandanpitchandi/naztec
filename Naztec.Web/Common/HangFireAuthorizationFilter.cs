﻿namespace Naztec.Web.Common
{
    using System.Security.Claims;
    using System.Web;
    using Naztec.Domain.Entities;
    using Hangfire.Annotations;
    using Hangfire.Dashboard;

    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            if (HttpContext.Current.User == null)
            {
                return false;
            }

            if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var claimsIdentity = HttpContext.Current.User.Identity as ClaimsIdentity;
                if (claimsIdentity.HasClaim(NaztecConstants.JobDashboardClaim, bool.TrueString))
                {
                    return true;
                }

            }
            return false;
        }
    }
}