﻿function profileManager(settings) {

    naztec.profileManager = naztec.profileManager || {};

    Vue.use(window.vuelidate.default);
    const localValidators = { required, maxLength, minLength, sameAs,  numeric } = window.validators;

    naztec.profileManager = {
        controls: {
            entry: {
                uploadFrame: $("#upload-demo"),
                uploadButton: $("#upload"),
                uploadResult: $(".upload-result"),
            }
        }
    };

    var profileApp = new Vue({
        el: '#profileApp',
        data: {
            profile: settings.profile,
            roles: settings.roles,          
            skins: [
                {
                    className: 'skin-blue',
                    name: 'Blue',
                    logoBg: '#367fa9',
                    headerBg: '#3c8dbc',
                    menuBg: '#222d32',
                    contentBg: '#f4f5f7'
                },
                {
                    className: 'skin-black',
                    name: 'Black',
                    logoBg: '#fefefe',
                    headerBg: '#fefefe',
                    menuBg: '#222222',
                    contentBg: '#f4f5f7'
                },
                {
                    className: 'skin-purple',
                    name: 'Purple',
                    logoBg: '#555299',
                    headerBg: '#605ca8',
                    menuBg: '#222d32',
                    contentBg: '#f4f5f7'
                },
                {
                    className: 'skin-green',
                    name: 'Green',
                    logoBg: '#008d4c',
                    headerBg: '#00a65a',
                    menuBg: '#222d32',
                    contentBg: '#f4f5f7'
                },
                {
                     className: 'skin-red',
                     name: 'Red',
                     logoBg: '#d33724',
                     headerBg: '#dd4b39',
                     menuBg: '#222d32',
                     contentBg: '#f4f5f7'
                },
                {
                    className: 'skin-yellow',
                    name: 'Yellow',
                    logoBg: '#db8b0b',
                    headerBg: '#f39c12',
                    menuBg: '#222d32',
                    contentBg: '#f4f5f7'
                },
                 {
                     className: 'skin-light-blue',
                     name: 'Blue Light',
                     logoBg: '#367fa9',
                     headerBg: '#3c8dbc',
                     menuBg: '#f9fafc',
                     contentBg: '#f4f5f7'
                 },
                 {
                     className: 'skin-black-light',
                     name: 'Black Light',
                     logoBg: '#fefefe',
                     headerBg: '#fefefe',
                     menuBg: '#f9fafc',
                     contentBg: '#f4f5f7'
                 },
                 {
                     className: 'skin-purple-light',
                     name: 'Purple Light',
                     logoBg: '#555299',
                     headerBg: '#605ca8',
                     menuBg: '#f9fafc',
                     contentBg: '#f4f5f7'
                 },
                 {
                     className: 'skin-green-light',
                     name: 'Blue Light',
                     logoBg: '#008d4c',
                     headerBg: '#00a65a',
                     menuBg: '#f9fafc',
                     contentBg: '#f4f5f7'
                 },
                 {
                     className: 'skin-red-light',
                     name: 'Red Light',
                     logoBg: '#d33724',
                     headerBg: '#dd4b39',
                     menuBg: '#f9fafc',
                     contentBg: '#f4f5f7'
                 },
                 {
                     className: 'skin-yellow-light',
                     name: 'Yellow Light',
                     logoBg: '#db8b0b',
                     headerBg: '#f39c12',
                     menuBg: '#f9fafc',
                     contentBg: '#f4f5f7'
                 },
            ],
            changepassword: settings.changepassword,
            isUnderChangePassword: false,
            profileBeforeEdit: '',
        },
        validations: {
            profile: {
                FirstName: {
                    required
                },
                LastName: {
                    required
                },
                PhoneNumber: {
                    numeric,
                    minLength: minLength(10),
                    maxLength: maxLength(10)
                }
            },
            changepassword: {
                OldPassword: {
                    required,
                },
                Password: {
                    required,
                },
                ConfirmPassword: {
                    required,
                    sameAsPassword: sameAs('Password')
                }
            }
        },
        computed: {
            profileImageUrl: function () {
                //if (this.profile.ImageId != naztec.globals.emptyGuid) {
                    return '/Image/Picture?imageid=' + this.profile.ImageId;
                //}
                //else {
                //    return naztec.utilities.ui.generateProfilePicture(this.profile.FirstName + ' ' + this.profile.LastName, 90);
                //}
            }
        },
        methods: {
            changeProfileImage: function () {
                $('#uploadPictureModal').modal();
            },
            showNewListModal: function () {
                $('#newProfileModal').modal();
            },
            editProfile: function () {
                var that = this;
                that.isUnderEdit = true;
                that.profileBeforeEdit = _.cloneDeep(that.profile);
            },
            saveProfile: function () {
                this.$v.profile.$touch();

                var that = this;
                if (!this.$v.profile.$invalid) {
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "Profile/SaveProfile",
                        data: { profile: this.profile },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                that.isUnderEdit = false;
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                }
            },
            cancelProfile: function () {
                var that = this;
                that.isUnderEdit = false;
                that.profile = that.profileBeforeEdit;
            },
            changePassword: function () {
                this.$v.changepassword.$touch();

                var that = this;
                if (!this.$v.changepassword.$invalid) {
                    var model = {
                        OldPassword: that.changepassword.OldPassword,
                        Password: that.changepassword.Password,
                        ConfirmPassword: that.changepassword.ConfirmPassword
                    };
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "Auth/ChangePassword",
                        data: model,
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                
                                that.$v.changepassword.$reset();

                                that.changepassword.OldPassword = '';
                                that.changepassword.Password = '';
                                that.changepassword.ConfirmPassword = '';
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                }
            },
            cancelPasswordChange: function () {
                var that = this;
                that.isUnderChangePassword = false;
                this.$v.changepassword.$reset();
            },
            selectTheme: function (themeCss) {
                var that = this;
                _.forEach(that.skins, function (item) {
                    $('body').removeClass(item.className);
                });

                $('body').addClass(themeCss);

                $.ajax({
                    method: "POST",
                    url: settings.rootDir + "Profile/SaveTheme",
                    data: { theme: themeCss },
                    //success: function (data) {
                    //    if (data.success === true) {
                    //        toastr.success(data.message);
                    //    }
                    //    else {
                    //        toastr.error(data.message);
                    //    }
                    //}
                })
                
            }
        },
        
        created: function () {
        },
        watch: {

        }
    });

    $(function () {

        var isImageSelected = false;

        var $uploadCrop;

        $uploadCrop = naztec.profileManager.controls.entry.uploadFrame.croppie({
            viewport: {
                width: 190,
                height: 190,
            },
            enableExif: true,
            boundary: {
                width: 200,
                height: 200
            },
            enableZoom: true,
            showZoomer: true,
            mouseWheelZoom: true
        });

        $uploadCrop.croppie('bind', {
            url: settings.rootDir + settings.imageUrl,
        });

        function readFile(input) {

            var ext = $(naztec.profileManager.controls.entry.uploadButton).val().split('.').pop().toLowerCase();

            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                toastr.error('invalid Image File!');
                return false;
            }
            else {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $uploadCrop.croppie('bind', {
                            url: e.target.result
                        });
                        naztec.profileManager.controls.entry.uploadFrame.addClass('ready');
                    }
                    reader.readAsDataURL(input.files[0]);
                    isImageSelected = true;
                }
            }
        }

        naztec.profileManager.controls.entry.uploadButton.on('change', function () { readFile(this); });

        naztec.profileManager.controls.entry.uploadResult.on('click', function (ev) {

            if (!isImageSelected) {
                $('#uploadPictureModal').modal('hide');
                return false;
            }

            $uploadCrop.croppie('result', {
                type: 'base64',
                size: { width: 200, height: 200 }
            }).then(function (resp) {
                $('#imagebase64').val(resp);

                $.ajax({
                    method: 'POST',
                    url: "/Profile/AddProfileImage",
                    data: { imageString: resp },
                    success: function (data) {
                        if (data.success === true) {

                            $('#uploadPictureModal').modal('hide');

                            toastr.success(data.message);

                            $uploadCrop.croppie('bind', {
                                url: settings.rootDir + data.imageUrl,
                            });

                            $(".img-circle").attr('src', settings.rootDir + data.imageUrl);

                            return false;

                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                })
            });
        });
    });
}