(function ($) {
    "use strict";

    var periodSelector = function ($el, options) {
        this.container = $el;
        var that = this;
        
        this.dateFormat = options.dateFormat;
        this.periodText = options.periodText;

        // Navigation methods to get data out of the plugin.        
        this.navigateToPeriod = options.navigateToPeriod;
        this.dateSelected = options.dateSelected;

        this.showCalendar = (options.showCalendar != undefined) ? options.showCalendar : true;

        // Plugin Constructor
        function createPlugin() {            
            var pluginTemplate = "";
            pluginTemplate += " <div class=\'btn-group pull-right\'>";
            pluginTemplate += "     <div class=\'btn btn-default has-tooltip\' id=\'btnPreviousPeriod\' title=\'Previous period\' data-toggle=\'tooltip\' data-placement=\'bottom\'>";
            pluginTemplate += "         <i class=\'fa fa-chevron-circle-left\'><\/i>";
            pluginTemplate += "     <\/div>";
            pluginTemplate += "     <div class=\'btn btn-default\' id=\'dtPeriodPicker\' style=\'width: 200px;\'>";
            pluginTemplate += "         <input type=\'hidden\' id=\'periodDatepicker\'>";
            pluginTemplate += "         <div class=\'trigger\'><i class=\'fa fa-calendar\'><\/i><span id=\'spnPeriodText\' style=\'padding-left: 8px;\'></span><\/div>";
            pluginTemplate += "     <\/div>";
            pluginTemplate += "     <div class=\'btn btn-default has-tooltip\' id=\'btnNextPeriod\' title=\'Next period\' data-toggle=\'tooltip\' data-placement=\'bottom\'>";
            pluginTemplate += "         <i class=\'fa fa-chevron-circle-right\'><\/i>";
            pluginTemplate += "     <\/div>";
            pluginTemplate += " <\/div>";
            return pluginTemplate;
        }
        that.container.html(createPlugin); // Initialize the Plugin

        // Controls
        this.controls = {
            dateTrigger: $('.trigger'),
            periodDatePicker: $('#periodDatepicker'),
            periodText: $('#spnPeriodText'),
            prevPeriod: $('#btnPreviousPeriod'),
            nextPeriod: $('#btnNextPeriod')
        }

        // Plugin methods
        this.methods = {
            init: function () {
                if (that.showCalendar) {
                    var dateSelectedMethod = that.dateSelected;
                    that.controls.dateTrigger.click(function () {
                        //that.controls.periodDatePicker.datepicker({
                        //    dateFormat: that.dateFormat,
                        //    numberOfMonths: 1,
                        //    changeYear: true,
                        //    changeMonth: true,
                        //    beforeShow: function () {
                        //        $('#ui-datepicker-div').css('z-index', 2);
                        //    },
                        //    onClose: function (selected) {
                        //        if (selected != '') {
                        //            dateSelectedMethod(selected);                                    
                        //        }
                        //    }
                        //});
                        //that.controls.periodDatePicker.datepicker('show');

                        alert('asddsad');
                    });
                }

                that.controls.periodText.text(that.periodText);

                that.controls.prevPeriod.click(function () {
                    if ($(this).hasClass("disabled")) {
                        return false;
                    }
                    else {
                        that.navigateToPeriod('prev');
                    }
                });

                that.controls.nextPeriod.click(function () {
                    if ($(this).hasClass("disabled")) {
                        return false;
                    }
                    else {
                        that.navigateToPeriod('next');
                    }
                });
            }
        }

        this.methods.init();
    };

    periodSelector.prototype.changeText = function (text, hideButton) {
        var scope = this;
        scope.controls.periodText.text(text);

        if (hideButton == 'prev') {
            scope.controls.prevPeriod.addClass('disabled');
        }
        else if(hideButton == 'next') {
            scope.controls.nextPeriod.addClass('disabled');
        }
        else if (hideButton == 'none'){
            scope.controls.prevPeriod.removeClass('disabled');
            scope.controls.nextPeriod.removeClass('disabled');
        }
    };   

    $.fn.periodSelector = function (option) {
        if (this.length > 1) {
            $.error('Unable to initialize on multiple target');
        }

        var data = this.data('periodSelector'),
		options = (typeof option == 'object' && option) || {};

        if (!data && option == 'destroy') {
            return this;
        }

        if (!data || option == null) {
            this.data('periodSelector', new periodSelector(this, options));
        }

        if (typeof option == 'string' && data != null) {
            return data[option].apply(data, Array.prototype.slice.call(arguments, 1));
        }

        return this;
    };

}(jQuery));
