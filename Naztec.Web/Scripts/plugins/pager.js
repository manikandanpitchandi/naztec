﻿(function(){
    Vue.component("ecb-pager", {
        template: '<div class="col-md-12 text-center"> \
                   <ul class="pagination"> \
                       <li :class="{\'disabled\' : isFirst}" @click="!isFirst && first()"> \
                           <a href="javascript:void(0);"><i class="fa fa-fast-backward"></i></a> \
                       </li> \
                       <li :class="{\'disabled\' : isFirst}" @click="!isFirst && previous()"> \
                           <a href="javascript:void(0);"><i class="fa fa-backward"></i></a> \
                       </li> \
                       <li> \
                           <input type="text" class="input-sm" size="2" v-model="toPage" number @keyup.enter="toPageChanged" @keyup.tab="toPageChanged"/> of {{totalPages}} \
                       </li> \
                       <li :class="{\'disabled\' : isLast}" @click="!isLast && next()"> \
                           <a href="javascript:void(0);"><i class="fa fa-forward"></i></a> \
                       </li> \
                       <li :class="{\'disabled\' : isLast}" @click="!isLast && last()"> \
                           <a href="javascript:void(0);"><i class="fa fa-fast-forward"></i></a> \
                       </li> \
                       <li> \
                           <select class="padding-5" v-model="pageSize" @change="pageSizeChanged"> \
                               <option v-for="row in currentRowList" :value="row" :key=row>{{row}}</option> \
                           </select> \
                       </li> \
                       <li > \
                           &nbsp; \
                       </li > \
                       <li> \
                           <strong>{{viewRecords}}</strong> \
                       </li> \
                   </ul> \
               </div>',
        props: ["currentPage", "currentRecordCount", "totalRecordCount", "viewText", "rowList",],
        data: function () {
            return {
                currentViewText: this.viewText || "Viewing {0} of {1}",
                totalPages: 0,
                pageSize: 10,
                toPage: this.currentPage,
                currentRowList: this.rowList || ["10", "20", "40"]
            }
        },
        methods: {
            first: function () {
                this.loadRecords(1);
            },
            previous: function () {
                this.loadRecords(this.currentPage - 1);
            },
            next: function () {
                this.loadRecords(this.currentPage + 1);
            },
            last: function () {
                this.loadRecords(this.totalPages);
            },
            loadRecords: function (page) {
                this.$emit("ecb-pager-load-records", page, this.pageSize);
            },
            pageSizeChanged: function () {
                this.totalPages = Math.ceil(this.totalRecordCount / this.pageSize);
                this.loadRecords(1);
            },
            toPageChanged: function () {
                var toPage = this.toPage <= 0 ? 1 : this.toPage > this.totalPages ? this.totalPages : this.toPage;
                this.loadRecords(toPage);
            }
        },
        computed: {
            isFirst: function () {
                return this.currentPage <= 1;
            },
            isLast: function () {
                return this.currentPage >= this.totalPages;
            },
            viewRecords: function () {
                var startCount = ((this.currentPage - 1) * this.pageSize) + 1;
                var endCount = (parseInt(this.currentRecordCount) + startCount) - 1;
                endCount = (endCount > this.totalRecordCount) ? this.totalRecordCount : endCount;
                return this.currentViewText.format(startCount + " - " + endCount, this.totalRecordCount);
            }
        },
        created: function(){
            console.log(this.currentPage + ' ' + this.currentRecordCount + ' ' + this.totalRecordCount);

            this.totalPages = Math.ceil(this.totalRecordCount / this.pageSize);
        },
        watch: {
            totalRecordCount: function (newValue) {
                this.totalPages = Math.ceil(newValue / this.pageSize);
            }, 
            currentPage: function (newValue) {
                this.toPage = newValue;
            }
        }
    });
})();
