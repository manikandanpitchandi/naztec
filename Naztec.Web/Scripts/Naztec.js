var naztec = {};

naztec.enums = {
    saveType: {
        save: 0,
        saveNew: 1,
        saveClose: 2
    }
}

naztec.globals = {
    rootUrl: "",
    emptyGuid: "00000000-0000-0000-0000-000000000000"
}

if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    };
}

naztec.init = function (rootUrl) {
    naztec.globals.rootUrl = rootUrl;
};

naztec.initSecured = function () {
    jQuery.ajaxSetup({
        cache: false,
        global: true,
        beforeSend: function (xhr, options) {
            var originalSuccess = options.success
            options.success = function (data, textStatus, jqXhr) {
                if (data.success === false) {
                    if (data.isSessionExpired === true) {
                        window.location.href = wayPoint.globals.rootUrl + "Auth/Login";
                        return;
                    }
                    else if (data.isAccessDenied === true) {
                        window.location.href = wayPoint.globals.rootUrl + "Auth/AccessDenied";
                        return;
                    }
                }

                if (originalSuccess) {
                    originalSuccess.apply(this, arguments)
                }
            }
        }
    });

    setInterval(function () {
        //toastr.success('Firing HeartBeat!');
        $.post(naztec.globals.rootUrl + "Auth/HeartBeat", null, function (data) {
        });
    }, 1020000);
}

naztec.utilities = {
    common: {
        findAndRemove: function (array, property, value) {
            array.forEach(function (result, index) {
                if (result[property] === value) {
                    array.splice(index, 1);
                }
            });
        },
        jsonCompare: function (sourceArr, destArr, sortArrays) {
            function sort(object) {
                if (sortArrays === true && Array.isArray(object)) {
                    return object.sort();
                }
                else if (typeof object !== "object" || object === null) {
                    return object;
                }

                return Object.keys(object).sort().map(function (key) {
                    return {
                        key: key,
                        value: sort(object[key])
                    };
                });
            }

            return !(JSON.stringify(sort(sourceArr)) === JSON.stringify(sort(destArr)));
        },
        postify: function (value) {
            var result = {};

            var buildResult = function (object, prefix) {
                for (var key in object) {

                    var postKey = isFinite(key)
                        ? (prefix != "" ? prefix : "") + "[" + key + "]"
                        : (prefix != "" ? prefix + "." : "") + key;

                    switch (typeof (object[key])) {
                        case "number": case "string": case "boolean":
                            result[postKey] = object[key];
                            break;

                        case "object":
                            if (object[key] == null || object[key] == undefined) {
                                result[postKey] = object[key];
                            }
                            else if (object[key].toUTCString) {
                                result[postKey] = object[key].toUTCString().replace("UTC", "GMT");
                            }
                            else {
                                buildResult(object[key], postKey != "" ? postKey : key);
                            }
                    }
                }
            };

            buildResult(value, "");

            return result;
        }
    },
    ui: {
        generateProfilePicture: function (name, size) {

            // Canvas Creation
            var canvas = document.createElement('canvas');
            canvas.style.display = 'none';
            canvas.width = size;
            canvas.height = size;
            document.body.appendChild(canvas);

            // Name Extraction
            var first, last, final;
            var firstChar = '-';
            var lastChar = '-';
            name = name.trim();
            if (name != '') {
                var result = name.split(' ');
                first = result[0];
                firstChar = first[0];
                if (result.length > 1) {
                    last = result[1];
                    lastChar = last[0];
                }
                else {
                    lastChar = '';
                }
            }

            final = firstChar + lastChar;

            // Context Creation
            var context = canvas.getContext('2d');
            context.fillStyle = '#438DD0'; //'#' + ("000000" + Math.random().toString(16).slice(2, 8).toUpperCase()).slice(-6);  //"#275B89";
            context.fillRect(0, 0, canvas.width, canvas.height);
            context.fillStyle = "#fff";

            context.textAlign = 'center';
            if (size === 45) {
                context.font = "18px Montserrat";
                context.fillText(final.toUpperCase(), 22, 28);
            }
            else if (size === 80) {
                context.font = "28px Montserrat";
                context.fillText(final.toUpperCase(), 40, 50);
            }
            else if (size === 90) {
                context.font = "32px Montserrat";
                context.fillText(final.toUpperCase(), 44, 54);
            }
            else if (size === 150) {
                context.font = "54px Montserrat";
                context.fillText(final.toUpperCase(), 74, 90);
            }

            var data = canvas.toDataURL();
            document.body.removeChild(canvas);
            return data;
        }
    },
    select2: {
        matcher: function (params, data) {
            // If there are no search terms, return all of the data
            if ($.trim(params.term) === '') {
                return data;
            }

            // Do not display the item if there is no 'text' property
            if (typeof data.text === 'undefined') {
                return null;
            }

            var combinedText = (data.text + data.code).toUpperCase();

            // `params.term` should be the term that is used for searching
            // `data.text` is the text that is displayed for the data object
            if (combinedText.indexOf(params.term.toUpperCase()) >= 0) {
                var modifiedData = $.extend({}, data, true);
                //modifiedData.text += ' (matched)';

                // You can return modified objects from here
                // This includes matching the `children` how you want in nested data sets
                return modifiedData;
            }

            // Return `null` if the term should not be displayed
            return null;
        }
    }
}
