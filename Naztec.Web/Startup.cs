// <auto-generated/>

using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Naztec.Web.Startup))]
namespace Naztec.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureAuth(app);

            this.ConfigureHangfire(app);
        }
    }
}
