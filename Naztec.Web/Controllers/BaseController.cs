namespace Naztec.Web.Controllers
{
    using System.Web.Mvc;
    using Naztec.Web.Infrastructure.Contracts;
    using Naztec.Web.Infrastructure.ActionResults;
    using System.Text;
    using System.IO;
    using System;

    public class BaseController : Controller
    {
        private IApplicationStore applicationStore;
        private ISessionStore sessionStore;

        public BaseController(IApplicationStore applicationStore, ISessionStore sessionStore)
        {
            this.applicationStore = applicationStore;
            this.sessionStore = sessionStore;
        }

        public IApplicationStore ApplicationStore
        {
            get
            {
                return this.applicationStore;
            }

            private set
            {
            }
        }

        public ISessionStore SessionStore
        {
            get
            {
                return this.sessionStore;
            }

            private set
            {
            }
        }

        protected JsonNetResult JsonNet(object data, JsonRequestBehavior behavior)
        {
            return new JsonNetResult
            {
                Data = data,
                ContentType = "application/json",
                ContentEncoding = null,
                JsonRequestBehavior = behavior
            };
        }

        protected FileStreamResult Sql(string sqlScript, string fileName)
        {
            var byteArray = Encoding.ASCII.GetBytes(sqlScript);
            var stream = new MemoryStream(byteArray);

            return File(stream, "text/plain", $"{fileName}.sql");
        }

        public bool CreateImageInPath(Guid? imageName, byte[] image, string imagePath)
        {
            try
            {
                string filePath = Server.MapPath(imagePath + Path.GetFileName(imageName.ToString() + ".jpg"));

                System.IO.File.WriteAllBytes(filePath, image);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteImageInPath(Guid? imageName, string imagePath)
        {
            try
            {
                string previousFilePath = Server.MapPath(imagePath + Path.GetFileName(imageName.ToString() + ".jpg"));

                if (System.IO.File.Exists(previousFilePath))
                {
                    System.IO.File.Delete(previousFilePath);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
