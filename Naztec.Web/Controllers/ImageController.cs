﻿namespace Naztec.Web.Controllers
{
    using Domain.Contracts;
    using Domain.Entities;
    using Infrastructure.Attributes;
    using Infrastructure.Contracts;
    using log4net;
    using System;
    using System.IO;
    using System.Web.Mvc;
    using System.Web.UI;
    using ViewModels;

    [NoAuthorize]
    public class ImageController : BaseController
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private IProfileService profileService;

        public ImageController(IApplicationStore applicationStore, ISessionStore sessionStore, IProfileService profileService)
            : base(applicationStore, sessionStore)
        {
            this.profileService = profileService;
        }

        public ImageController(IApplicationStore applicationStore, ISessionStore sessionStore)
           : base(applicationStore, sessionStore)
        {
            
        }

        [OutputCache(Duration = int.MaxValue, Location = OutputCacheLocation.Client, VaryByParam = "*")]
        [Route("Image/Picture/{imageId}")]
        public ActionResult Picture(string imageId)
        {
            BaseVM viewModel = new BaseVM(this.ApplicationStore, this.SessionStore);

            var memberImagePath = Path.Combine(Server.MapPath("~/Images/profile/"), $"{imageId}.jpg");

            Guid validImageGuid;

            bool isValidGuid = Guid.TryParse(imageId, out validImageGuid);

            if (isValidGuid && validImageGuid != Guid.Empty)
            {
                if (System.IO.File.Exists(memberImagePath))
                {
                    return this.File(memberImagePath, "image/jpeg");
                }
                else
                {
                    var imageEntity = this.profileService.GetProfileImageByImageName(validImageGuid);

                    var result = this.CreateProfileImageInPath(validImageGuid, imageEntity.ProfileImage);

                    if (result)
                    {
                        return this.File(memberImagePath, "image/jpeg");
                    }
                }
            }

            var defaultImagePath = Server.MapPath("~/Images/profile/noimage.jpg");

            return this.File(defaultImagePath, "image/jpeg");
        }

        [OutputCache(Duration = int.MaxValue, Location = OutputCacheLocation.Client, VaryByParam = "*")]
        [Route("Image/Company/{imageId}")]
        public ActionResult Company(string imageId)
        {
            BaseVM viewModel = new BaseVM(this.ApplicationStore, this.SessionStore);

            var companyImagePath = Path.Combine(Server.MapPath("~/Images/company/"), $"{imageId}.jpg");

            Guid validImageGuid;

            bool isValidGuid = Guid.TryParse(imageId, out validImageGuid);

            if (isValidGuid && validImageGuid != Guid.Empty)
            {
                //if (System.IO.File.Exists(companyImagePath))
                //{
                //    return this.File(companyImagePath, "image/jpeg");
                //}
                //else
                //{
                //    var companyimageEntity = this.profileService.GetProfileImageByImageName(validImageGuid);

                //    var result = this.CreateCompanyImageInPath(validImageGuid, imageEntity.ProfileImage);

                //    if (result)
                //    {
                //        return this.File(companyImagePath, "image/jpeg");
                //    }
                //}
            }

            var defaultCompanyImagePath = Server.MapPath("~/Images/company/nocompanylogo.jpg");

            return this.File(defaultCompanyImagePath, "image/jpeg");
        }

        public bool CreateProfileImageInPath(Guid? imageName, byte[] profileImage)
        {
            try
            {
                string filePath = Server.MapPath(NaztecConstants.ProfileImage.ServerImagePath + Path.GetFileName(imageName.ToString() + ".jpg"));

                System.IO.File.WriteAllBytes(filePath, profileImage);

                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);

                return false;
            }
        }
    }
}