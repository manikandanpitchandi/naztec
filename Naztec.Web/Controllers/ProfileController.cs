﻿using AutoMapper;
using Naztec.Domain.Contracts;
using Naztec.Domain.Entities;
using Naztec.Web.Infrastructure.Contracts;
using Naztec.Web.Infrastructure.Extensions;
using Naztec.Web.Models;
using Naztec.Web.ViewModels;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Naztec.Web.Controllers
{
    public class ProfileController : BaseController
    {
        private IProfileService profileService;
        private IAuthService authService;
        private IAuthenticationManager authenticationManager;

        public ProfileController(IAuthenticationManager authenticationManager, IApplicationStore applicationStore, ISessionStore sessionStore, IProfileService profileService, IAuthService authService)
            : base(applicationStore, sessionStore)
        {
            this.profileService = profileService;
            this.authenticationManager = authenticationManager;
            this.authService = authService;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Manage");
        }

        public ActionResult Manage()
        {
            ProfileVM viewModel = new ProfileVM(this.ApplicationStore, this.SessionStore);

            viewModel.Profile = Mapper.Map<ProfileDM>(this.profileService.GetProfile(viewModel.UserName));
            viewModel.ChangePassword = new ChangePasswordDM();

            if (viewModel.Profile.ImageId != null)
            {
                string profilePath = NaztecConstants.ProfileImage.JsImagePath + viewModel.Profile.ImageId + ".jpg";

                string searchprofilePath = Server.MapPath(NaztecConstants.ProfileImage.ServerImagePath + viewModel.Profile.ImageId + ".jpg");

                viewModel.Profile.ProfileImageUrl = profilePath;

                if (!System.IO.File.Exists(searchprofilePath))
                {
                    var response = this.CreateImageInPath(viewModel.Profile.ImageId, viewModel.Profile.ProfileImage, NaztecConstants.ProfileImage.ServerImagePath);

                    if (!response)
                    {
                        viewModel.Profile.ProfileImageUrl = NaztecConstants.ProfileImage.JsImagePath + NaztecConstants.ProfileImage.DefaultImage;
                    }
                }
            }
            else
            {
                viewModel.Profile.ProfileImageUrl = NaztecConstants.ProfileImage.JsImagePath + NaztecConstants.ProfileImage.DefaultImage;
            }

            return View(viewModel);
        }

        [HttpPost]
        [Route("Profile/SaveProfile")]
        public ActionResult SaveProfile(ProfileDM profile)
        {
            var viewModel = new BaseVM(this.ApplicationStore, this.SessionStore);

            profile.Email = viewModel.UserName;

            if (profile.ImageId == null)
            {
                profile.ImageId = Guid.Empty;
            }

            if (!ModelState.IsValid)
            {
                return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            }
            var response = this.profileService.SaveProfile(Mapper.Map<UserProfile>(profile));

            if (response.IsOk)
            {
                this.SessionStore.Set<string>(SessionConstants.FullName, profile.FirstName + " " + profile.LastName);
            }
            return this.Json(new { success = response.IsOk, message = response.GetHtmlMessage() });
        }

        [HttpPost]
        public async Task<ActionResult> SaveTheme(string theme)
        {
            var viewModel = new BaseVM(this.ApplicationStore, this.SessionStore);

            var response = await this.authService.WriteToUserClaims(viewModel.SessionSecurityUserId, NaztecConstants.Claims.SkinTheme, theme);

            var authenticationProperties = new AuthenticationProperties { };
            this.authenticationManager.SignIn(authenticationProperties, response.UserClaims);

            return this.Json(new { success = response.IsOk, message = response.GetHtmlMessage() });
        }

        public ActionResult AddProfileImage(string imageString)
        {
            if (imageString == null)
            {
                return this.Json(new { success = false, message = "Adding Image Failed" });
            }

            var viewModel = new ProfileImageVM(this.ApplicationStore, this.SessionStore);

            viewModel.ImageName = Guid.NewGuid();

            viewModel.ImageBinaryData = this.profileService.GetImageinByteArray(imageString);

            if (viewModel.ImageBinaryData != null)
            {
                viewModel.AppUserId = viewModel.SessionSecurityUserId;

                var response = this.profileService.AddImages(Mapper.Map<ProfileImageItem>(viewModel));

                Guid? profilePicture = null;

                if (response.IsOk)
                {
                    var creationResult = this.CreateImageInPath(viewModel.ImageName, viewModel.ImageBinaryData, NaztecConstants.ProfileImage.ServerImagePath);

                    if (creationResult)
                    {
                        this.SessionStore.Set(SessionConstants.AppUserImageId, viewModel.ImageName);

                        var deletionResult = this.DeleteImageInPath(response.Item.PreviousImageName, NaztecConstants.ProfileImage.ServerImagePath);

                        profilePicture = response.Item.ImageName;
                    }
                    else
                    {
                        profilePicture = response.Item.PreviousImageName;
                    }
                }

                return this.Json(new { success = response.IsOk, imageUrl = NaztecConstants.ProfileImage.JsImagePath + Path.GetFileName(profilePicture + ".jpg"), message = response.GetHtmlMessage() });
            }

            return this.Json(new { success = false, message = "No file Added" });
        }
    }
}