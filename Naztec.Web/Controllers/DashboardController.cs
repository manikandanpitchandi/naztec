namespace Naztec.Web.Controllers
{
    using System.Web.Mvc;
    using Naztec.Web.Infrastructure.Contracts;
    using Naztec.Web.Infrastructure.Attributes;
    using Naztec.Web.ViewModels;

    [GlobalAuthorize]
    public class DashboardController : BaseController
    {
        public DashboardController(IApplicationStore applicationStore, ISessionStore sessionStore)
            :base(applicationStore,sessionStore)
        {
            
        }

        public ActionResult Index()
        {
            DashboardVM viewModel = new DashboardVM();

            return View(viewModel);
        }
    }
}
