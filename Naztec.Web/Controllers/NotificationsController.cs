﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Naztec.Web.Infrastructure.Contracts;
using Naztec.Web.ViewModels.Notifications;

namespace Naztec.Web.Controllers
{
    public class NotificationsController : BaseController
    {
        public NotificationsController(IApplicationStore applicationStore, ISessionStore sessionStore) : base(applicationStore, sessionStore)
        {
        }

        public ActionResult Index()
        {
            NotificationsVM viewModel = new NotificationsVM(this.ApplicationStore, this.SessionStore);

            return View(viewModel);
        }
    }
}