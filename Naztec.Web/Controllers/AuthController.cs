namespace Naztec.Web.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using AutoMapper;
    using Naztec.Domain.Contracts;
    using Naztec.Domain.Entities;
    using Naztec.Web.Infrastructure.Attributes;
    using Naztec.Web.Infrastructure.Contracts;
    using Infrastructure.Extensions;
    using Naztec.Web.Models;
    using Microsoft.Owin.Security;
    using ViewModels;
    using System.Web;
    using System.Collections.Generic;
    using Naztec.Domain.Implementations;

    [NoOutputCache]
    public class AuthController : BaseController
    {
        private IAuthService authService;
        private IAuthenticationManager authenticationManager;
        private IEmailManager emailManager;
        private IApplicationConfigurationManager configurationManager;
        private IEmailService emailService;
        private IHangfireWrapper hangfireWrapper;

        public AuthController(IAuthenticationManager authenticationManager, IApplicationStore applicationStore, ISessionStore sessionStore, IAuthService authService, IEmailManager emailManager, IApplicationConfigurationManager configurationManager,
              IEmailService emailService, IHangfireWrapper hangfireWrapper)
            : base(applicationStore, sessionStore)
        {
            this.authService = authService;
            this.authenticationManager = authenticationManager;
            this.emailManager = emailManager;
            this.configurationManager = configurationManager;
            this.emailService = emailService;
            this.hangfireWrapper = hangfireWrapper;
        }

        [HttpGet]
        public ActionResult Login()
        {
            LoginVM viewModel = new LoginVM();

            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginVM model)
        {
            this.Session.Clear();

            var response = await this.authService.AppLogin(Mapper.Map<LoginRequest>(model.Login));


            this.ModelState.AddModelErrors(response);

            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            // Set all available Session variables.
            this.SessionStore.Set<Guid>(SessionConstants.SecurityUserId, response.SecurityUserId);
            this.SessionStore.Set<string>(SessionConstants.FullName, response.FullName);
            this.SessionStore.Set<string>(SessionConstants.UserName, model.Login.UserName);
            this.SessionStore.Set<Guid?>(SessionConstants.AppUserImageId,response.ImageId);
            this.SessionStore.Set<string>(SessionConstants.UserName, model.Login.UserName);
            this.SessionStore.Set<List<Guid>>(SessionConstants.RoleIds, response.Roles);

            // Prepare Auth ticket.

            var authenticationProperties = new AuthenticationProperties { IsPersistent = model.RememberMe };
            this.authenticationManager.SignIn(authenticationProperties, response.UserClaims);

            return this.RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            this.Session.Clear();

            this.authenticationManager.SignOut();
            return this.RedirectToAction("Login");
        }

        [HttpPost]
        [Route("Auth/HeartBeat")]
        public ActionResult HeartBeat()
        {
            return this.Json(new { success = true });
        }

        [Route("Auth/AccessDenied")]
        public ActionResult AccessDenied()
        {
            return this.View();
        }

        [Route("Auth/SessionExpired")]
        public ActionResult SessionExpired()
        {
            return this.View();
        }

        [Route("Auth/ForgotPassword")]
        public ActionResult ForgotPassword()
        {
            var viewModel = new ForgotPasswordVM();

            return this.View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Auth/ForgotPassword")]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordVM viewModel)
        {
            if (viewModel.Email == null)
            {
                return this.View(viewModel);
            }
            var response = await this.authService.ForgotPassword(Mapper.Map<ForgotPassword>(viewModel));

            this.ModelState.AddModelErrors(response);

            if (!this.ModelState.IsValid)
            {
                return this.View(viewModel);
            }

            var callbackUrl = Url.Action("ResetPassword", "Auth", new { UserName = response.UserName, code = HttpUtility.UrlEncode(response.Token) }, protocol: Request.Url.Scheme);

            this.emailManager.SendMail(this.configurationManager.GetValue<string>(NaztecConstants.FromEmail), response.Email, "Reset your Password", this.authService.EmailBodyForForgotPassword(response.UserName, callbackUrl), true);
            var forgetPassword = new ForgetPasswordEmail {
                Email = response.Email,
                CallbackUrl = callbackUrl
            };
            this.hangfireWrapper.Enqueue<EmailService>(x => x.SendForgotPasswordEmail(forgetPassword));
            return this.RedirectToAction("ForgotPasswordSuccess");
        }

        [Route("Auth/ForgotPasswordSuccess")]
        public ActionResult ForgotPasswordSuccess()
        {
            return this.View();
        }

        public ActionResult ResetPassword(string UserName, string code)
        {
            var resetPassword = new ResetPasswordVM(this.ApplicationStore, this.SessionStore);

            resetPassword.UserName = UserName;

            resetPassword.Code = code;

            return this.View(resetPassword);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordVM viewModel)
        {
            if (!ModelState.IsValid)
            {
                return this.View(viewModel);
            }

            viewModel.Code = Uri.UnescapeDataString(viewModel.Code);

            var response = await this.authService.ResetPassword(Mapper.Map<ResetPassword>(viewModel));

            this.ModelState.AddModelErrors(response);

            if (!this.ModelState.IsValid)
            {
                return this.View(viewModel);
            }

            return this.RedirectToAction("ResetPasswordSuccess");
        }

        [Route("Auth/ResetPasswordSucess")]
        public ActionResult ResetPasswordSuccess()
        {
            return this.View();
        }

        [Route("Auth/ChangePassword")]
        public ActionResult ChangePassword()
        {
            var viewModel = new ChangePasswordVM(this.ApplicationStore, this.SessionStore);

            return this.View(viewModel);

        }
        [HttpPost]
        [Route("Auth/ChangePassword")]
        public async Task<ActionResult> ChangePassword(ChangePasswordDM viewModel)
        {
            var changePassword = new BaseVM(this.ApplicationStore, this.SessionStore);

            viewModel.EmailId = changePassword.UserName;

            //if (!this.ModelState.IsValid)
            //{
            //    return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            //}

            var response = await this.authService.ChangePassword(Mapper.Map<ChangePassword>(viewModel));

            return this.Json(new { success = response.IsOk, message = response.GetHtmlMessage() });
        }
    }
}
