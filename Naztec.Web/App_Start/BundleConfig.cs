using System.Web;
using System.Web.Optimization;

namespace Naztec.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Content Bundles

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap/bootstrap.min.css",
                      "~/Content/bootstrap-datetimepicker/bootstrap-datetimepicker.css",
                      "~/Content/font-awesome/font-awesome.css",
                      "~/Content/ionicons/ionicons.min.css",
                      "~/Content/toastr/toastr.css",
                      "~/Content/AdminLTE/AdminLTE.css",
                       "~/Content/select2/select2.css",
                      "~/Content/shared/structure.css",
                      "~/Content/shared/controls.css",
                      "~/Content/AdminLTE/skins/_all-skins.min.css"));

            bundles.Add(new StyleBundle("~/Content/iCheck").Include(
                    "~/Content/iCheck/square/blue.css"));

            bundles.Add(new StyleBundle("~/Content/jquery.jsTree/css")
             .Include("~/Content/jquery.jsTree/style.css"));

            bundles.Add(new StyleBundle("~/Content/croppie/croppiecss")
                 .Include("~/Content/croppie/croppie.css"));

            // Script Bundles                     

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                            "~/Scripts/lib/jQuery/jquery.js",
                            "~/Scripts/lib/jLinq/jLinq-2.2.1.js",
                            "~/Scripts/lib/moment/moment.min.js",
                            "~/Scripts/lib/bootstrap/bootstrap.min.js",
                            "~/Scripts/lib/bootstrap-datetime/bootstrap-datetimepicker.js",
                            "~/Scripts/lib/fastclick/fastclick.js",
                            "~/Scripts/lib/jQuery-slimscroll/jquery.slimscroll.min.js",
                            "~/Scripts/lib/respond/respond.js",
                            "~/Scripts/lib/vue/vue.js",
                            "~/Scripts/lib/vuelidate/vuelidate.min.js",
                            "~/Scripts/lib/vuelidate/validators.min.js",
                            "~/Scripts/lib/fuelux/fuelux.js",
                            "~/Scripts/lib/toastr/toastr.js",
                            "~/Scripts/lib/uuid/uuid.js",
                            "~/Scripts/lib/lodash/lodash.js",
                            "~/Scripts/lib/adminlte/adminlte.min.js",
                             "~/Scripts/lib/select2/select2.js",
                            //"~/Scripts/lib/adminlte/demo.js",
                            "~/Scripts/naztec.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                          "~/Scripts/lib/jQuery/jquery.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                          "~/Scripts/lib/bootstrap/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/iCheck").Include(
                        "~/Scripts/lib/iCheck/iCheck.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jsTree").Include(
                         "~/Scripts/lib/jstree/jstree.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/lib/jQuery-Validate/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/charts").Include(
                        "~/Scripts/lib/jQuery-sparkline/jquery.sparkline.min.js",
                        "~/Scripts/lib/chart/chart.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/croppie")
                  .Include("~/Scripts/lib/croppie/croppie.js")
                  .Include("~/Scripts/lib/croppie/exif.js"));

           

        }
    }
}
