namespace Naztec.Web
{
    using System.Configuration;
    using Owin;
    using Hangfire;
    using System.Web;
    using Naztec.Web.Common;

    public partial class Startup
    {
        public void ConfigureHangfire(IAppBuilder app)
        {
            var hangfireConnectionString = ConfigurationManager.ConnectionStrings["HangfireConnection"].ConnectionString;

            GlobalConfiguration.Configuration.UseSqlServerStorage(hangfireConnectionString);

            DashboardOptions options = new DashboardOptions();
            options.Authorization = new[] { new HangFireAuthorizationFilter() };
            options.AppPath = VirtualPathUtility.ToAbsolute("/Dashboard/Index");
            app.UseHangfireDashboard("/Dashboard/jobs", options);
        }
    }
}
