﻿using AutoMapper;
using Naztec.Core.Contracts;
using Naztec.Domain.Entities;
using Naztec.Web.Areas;

namespace Naztec.Web.Mappings
{
    public class InventoryMappings : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<InventoryDM, InventoryDTO>();
            configuration.CreateMap<InventoryDTO, InventoryDM>();
        }
    }
}