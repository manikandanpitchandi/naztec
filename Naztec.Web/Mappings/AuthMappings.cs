namespace Naztec.Web.Mappings
{
    using AutoMapper;
    using Core.Contracts;
    using Naztec.Domain.Entities;
    using Naztec.Web.Models;
    using ViewModels;

    public class AuthMappings : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<LoginDM, LoginRequest>();

            configuration.CreateMap<ForgotPasswordVM, ForgotPassword>();
            configuration.CreateMap<ResetPasswordVM, ResetPassword>();

           
        }
    }
}
