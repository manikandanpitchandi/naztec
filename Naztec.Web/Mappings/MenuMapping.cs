﻿namespace Naztec.Web.Mappings
{
    using Areas.Admin.DataModels.Menu;
    using AutoMapper;
    using Naztec.Domain.Entities;
    using Naztec.Core.Contracts;

    public class MenuMapping : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<MenuDM, MenuDTO>();
            configuration.CreateMap<MenuDTO, MenuDM>();
        }
    }
}