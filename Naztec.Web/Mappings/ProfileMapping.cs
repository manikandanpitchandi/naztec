﻿namespace Naztec.Web.Mappings
{
    using AutoMapper;
    using Naztec.Core.Contracts;
    using Naztec.Domain.Entities;
    using Models;
    using ViewModels;

    public class ProfileMapping : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<ProfileVM, UserProfile>();
            configuration.CreateMap<UserProfile, ProfileDM>();
            configuration.CreateMap<ProfileDM, UserProfile>();

            configuration.CreateMap<ChangePasswordVM, ChangePassword>();
            configuration.CreateMap<ChangePassword, ChangePasswordDM>();
            configuration.CreateMap<ChangePasswordDM, ChangePassword>();

            configuration.CreateMap<ProfileImageVM, ProfileImageItem>();
            configuration.CreateMap<ProfileImageItem, ProfileImageVM>();

        }
    }
}