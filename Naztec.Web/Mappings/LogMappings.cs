﻿namespace Naztec.Web.Mappings
{
    using AutoMapper;
    using Naztec.Core.Contracts;
    using Naztec.Domain.Entities;
    using Naztec.Web.Areas;

    public class LogMappings : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<LogDM, LogDTO>();
            configuration.CreateMap<LogDTO, LogDM>();

            
        }
    }
}