﻿namespace Naztec.Web.Mappings
{
    using Areas.Admin;
    using Areas.Admin.DataModels.Security;
    using Areas.Admin.DataModels.User;
    using AutoMapper;
    using Core.Entities;
    using Domain.Entities;
    using Domain.Entities.Admin.Security;
    using Naztec.Core.Contracts;
    using Models;

    public class SecurityManagementMappings : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<SecurityRightDM, SecurityRightDTO>();
            configuration.CreateMap<SecurityRightDTO, SecurityRightDM>();

            configuration.CreateMap<UserDM, UserDTO>();
            configuration.CreateMap<UserDTO, UserDM>();

            configuration.CreateMap<SecurityRightDisplayDM, SecurityRightDisplayDTO>();
            configuration.CreateMap<SecurityRightDisplayDTO, SecurityRightDisplayDM>();

            configuration.CreateMap<StateDM, StateDTO>();
            configuration.CreateMap<StateDTO, StateDM>();

            configuration.CreateMap<SecurityRoleDM, SecurityRoleDTO>();
            configuration.CreateMap<SecurityRoleDTO, SecurityRoleDM>();

            configuration.CreateMap<SecurityMenuDisplayDM, SecurityMenuDisplayDTO>();
            configuration.CreateMap<SecurityMenuDisplayDTO, SecurityMenuDisplayDM>();

            configuration.CreateMap<SecurityRoleMenuDM, SecurityRoleMenuDTO>();
            configuration.CreateMap<SecurityRoleMenuDTO, SecurityRoleMenuDM>();

            configuration.CreateMap<SecurityRoleDM, SecurityRoleDTO>();
            configuration.CreateMap<SecurityRoleDTO, SecurityRoleDM>();

            configuration.CreateMap<SecurityRoleRightsDTO, SecurityRoleRightsDM>();
            configuration.CreateMap<SecurityRoleRightsDM, SecurityRoleRightsDTO>();

            configuration.CreateMap<SecurityRoleUserDTO, SecurityRoleUserDM>();
            configuration.CreateMap<SecurityRoleUserDM, SecurityRoleUserDTO>();

            configuration.CreateMap<NaztecStringDM, NaztecStringItem>();
            configuration.CreateMap<NaztecStringItem, NaztecStringDM>();
        }
    }
}