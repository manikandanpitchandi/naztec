﻿namespace Naztec.Web.Mappings
{
    using Areas.Admin.DataModels.List;
    using AutoMapper;
    using Domain.Entities;
    using Naztec.Core.Contracts;

    public class ListMappings : IDefineMappings
    {
        public void CreateMappings(IConfiguration configuration)
        {
            configuration.CreateMap<ListMasterDM, ListMasterDTO>();
            configuration.CreateMap<ListMasterDTO, ListMasterDM>();

            configuration.CreateMap<ListDetailDM, ListDetailDTO>();
            configuration.CreateMap<ListDetailDTO, ListDetailDM>();
        }
    }
}
