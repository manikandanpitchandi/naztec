﻿namespace Naztec.Web.Areas.Admin.ViewModels.List
{
    using System;
    using System.Collections.Generic;
    using DataModels.List;
    using Naztec.Web.Infrastructure.Contracts;
    using Naztec.Web.ViewModels;
    using Infrastructure.Implementations;

    public class ListManageVM : BaseVM
    {
        public ListManageVM(IApplicationStore applicationStore, ISessionStore sessionStore) : base(new ApplicationStore(), new SessionStore())
        {
            this.ListMasters = new List<ListMasterDM>();            
        }

        public List<ListMasterDM> ListMasters { get; set; }
    }
}