﻿namespace Naztec.Web.Areas.Admin.ViewModels.Security
{
    using System.Collections.Generic;
    using Core.Entities;
    using Naztec.Web.Areas.Admin.DataModels.Security;
    using Naztec.Web.Infrastructure.Contracts;
    using Naztec.Web.Infrastructure.Implementations;
    using Web.ViewModels;

    public class SecurityRoleManageVM : BaseVM
    {
        public SecurityRoleManageVM(IApplicationStore applicationStore, ISessionStore sessionStore) :
            base(new ApplicationStore(), new SessionStore())
        {
            this.Roles = new List<SecurityRoleDM>();
        }

        public List<SecurityRoleDM> Roles { get; set; }

        public List<SecurityRightDisplayDM> RightsForTreeDisplay { get; set; }

        public List<NaztecGuidList> RoleUsers { get; set; }

        public List<NaztecGuidList> UnAssignedUsers { get; set; }

        public List<SecurityMenuDisplayDM> MenuForTreeDisplay { get; set; }
    }
}