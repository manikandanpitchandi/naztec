﻿namespace Naztec.Web.Areas.Admin.ViewModels.Security
{
    using System.Collections.Generic;
    using DataModels.Security;
    using Infrastructure.Contracts;
    using Infrastructure.Implementations;
    using Naztec.Web.ViewModels;

    public class SecurityRightManageVM : BaseVM
    {
        public SecurityRightManageVM(IApplicationStore applicationStore, ISessionStore sessionStore) : base(new ApplicationStore(), new SessionStore())
        {
            this.Rights = new List<SecurityRightDM>();
        }

        public List<SecurityRightDM> Rights { get; set; }
    }
}