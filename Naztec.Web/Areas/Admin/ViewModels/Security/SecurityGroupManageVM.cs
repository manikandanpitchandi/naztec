﻿namespace ECBase.Web.Areas.Admin.ViewModels.Security
{
    using System.Collections.Generic;
    using ECBase.Core.Entities;
    using ECBase.Web.Areas.Admin.DataModels.Security;
    using ECBase.Web.ViewModels;
    using Infrastructure.Contracts;
    using Infrastructure.Implementations;

    public class SecurityGroupManageVM : BaseVM
    {
        public SecurityGroupManageVM(IApplicationStore applicationStore, ISessionStore sessionStore) : base(new ApplicationStore(), new SessionStore())
        {
            this.Groups = new List<SecurityGroupDM>();
        }

        public List<SecurityGroupDM> Groups { get; set; }

        public List<SecurityRightDisplayDM> RightsForTreeDisplay { get; set; }

        public List<ECGuidList> GroupUsers { get; set; }

        public List<ECGuidList> UnAssignedUsers { get; set; }
    }
}