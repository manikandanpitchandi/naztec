﻿namespace Naztec.Web.Areas.Admin.ViewModels.Menu
{
    using System.Collections.Generic;
    using Naztec.Web.Areas.Admin.DataModels.Menu;
    using Naztec.Web.Infrastructure.Contracts;
    using Naztec.Web.ViewModels;

    public class MenuManageVM : BaseVM
    {
        public MenuManageVM(IApplicationStore applicationStore, ISessionStore sessionStore)
            : base(applicationStore, sessionStore)
        {
            this.Menu = new List<MenuDM>();
        }

        public List<MenuDM> Menu { get; set; }
    }
}