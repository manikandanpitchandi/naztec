﻿namespace Naztec.Web.Areas.Admin.ViewModels.User
{
    using DataModels.User;
    using Naztec.Web.Areas.Admin.DataModels;
    using Naztec.Web.Infrastructure.Contracts;
    using Infrastructure.Implementations;
    using Models;
    using System.Collections.Generic;
    using Web.ViewModels;

    public class UserManageVM : BaseVM
    {
        public UserManageVM(IApplicationStore applicationStore, ISessionStore sessionStore) 
            : base(applicationStore, sessionStore)
        {
            this.Users = new List<UserDM>();
        }

        public UserManageVM() : base(new ApplicationStore(), new SessionStore())
        {

        }

        public List<UserDM> Users { get; set; }

        public List<NaztecStringDM> TimeZoneList { get; set; }
    }
}