﻿using Naztec.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Naztec.Web.Infrastructure.Contracts;

namespace Naztec.Web.Areas.Admin.ViewModels.Log
{
    public class LogManageVM : BaseVM
    {
        public LogManageVM(IApplicationStore applicationStore, ISessionStore sessionStore) : base(applicationStore, sessionStore)
        {
            this.Logs = new List<LogDM>();
        }

        public List<LogDM> Logs { get; set; }
    }
}