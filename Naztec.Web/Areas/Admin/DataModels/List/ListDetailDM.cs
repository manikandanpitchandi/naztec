﻿namespace Naztec.Web.Areas.Admin.DataModels.List
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using Data.Entities;

    public class ListDetailDM
    {
        public ListDetailDM()
        {            
        }

        public Guid Id { get; set; }
        
        public Guid MasterId { get; set; }        

        [Required(ErrorMessage = "Code is required")]
        [MaxLength(20, ErrorMessage = "Maximum of 20 characters allowed")]
        public string Code { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [MaxLength(256, ErrorMessage = "Maximum of 256 characters allowed")]
        public string Description { get; set; }

        public ListMasterDM ListMaster { get; set; }
    }
}
