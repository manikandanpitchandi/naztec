﻿namespace Naztec.Web.Areas.Admin.DataModels.List
{
    using System;

    public class ListMasterDM
    {
        public Guid Id { get; set; }

        public string MasterName { get; set; }
    }
}