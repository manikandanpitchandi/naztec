﻿namespace Naztec.Web.Areas.Admin.DataModels.User
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Naztec.Web.Models;
    using System.Collections.Generic;

    public class UserDM : BaseDM
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [MaxLength(100, ErrorMessage = "Maximum of 100 characters allowed")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [MaxLength(100, ErrorMessage = "Maximum of 100 characters allowed")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Phone Number is required")]
        [MaxLength(10, ErrorMessage = "Maximum of 10 characters allowed")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [MaxLength(256, ErrorMessage = "Maximum of 256 characters allowed")]
        public string Email { get; set; }

        public List<string> Roles { get; set; }

        public Guid? ImageId { get; set; }

        public string Timezone { get; set; }
    }
}