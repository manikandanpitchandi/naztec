﻿namespace Naztec.Web.Areas.Admin.DataModels.Menu
{
    using System;
    using Naztec.Web.Models;
    using System.ComponentModel.DataAnnotations;

    public class MenuDM : BaseDM
    {
        public Guid Id { get; set; }

        public Guid? ParentId { get; set; }

        [MaxLength(50)]
        public string Text { get; set; }

        [MaxLength(50)]
        public string Slug { get; set; }

        public string CssClass { get; set; }        

        public int DisplayOrder { get; set; }

        public bool IsActive { get; set; }

        public bool HasChildren { get; set; }
    }
}