﻿namespace Naztec.Web.Areas
{
    using System;

    public class LogDM
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string Level { get; set; }

        public string Logger { get; set; }

        public string Message { get; set; }

        public string Exception { get; set; }

        public string StringDateTime { get { return Date.ToString("dd/MM/yy hh:mm"); } }
    }
}