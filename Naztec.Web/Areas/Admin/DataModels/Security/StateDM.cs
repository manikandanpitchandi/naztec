﻿namespace Naztec.Web.Areas.Admin.DataModels.Security
{
    using Newtonsoft.Json;

    public class StateDM
    {
        [JsonProperty(PropertyName = "selected")]
        public bool Selected { get; set; }
    }
}