﻿namespace Naztec.Web.Areas.Admin.DataModels.Security
{
    using System;
    using Naztec.Web.Models;

    public class SecurityRoleDM : BaseDM
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}