﻿namespace Naztec.Web.Areas.Admin.DataModels.Security
{
    using System;

    public class SecurityRightDM
    {
        public Guid Id { get; set; }

        public bool ApplicationSecurity { get; set; }

        public bool IsHidden { get; set; }

        public Guid? ParentRightId { get; set; }

        public int DisplayOrder { get; set; }

        public string SecurityRightDefinition { get; set; }
    }
}