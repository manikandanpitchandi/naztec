﻿namespace ECBase.Web.Areas.Admin.DataModels.Security
{
    using System;
    using System.Collections.Generic;
    using ECBase.Core.Entities;

    public class SecurityGroupRightsDM
    {
        public Guid SecurityGroupId { get; set; }

        public List<ECGuidList> SecurityRightsList { get; set; }
    }
}