﻿namespace Naztec.Web.Areas.Admin.DataModels.Security
{
    using System;
    using System.Collections.Generic;
    using Core.Entities;

    public class SecurityRoleMenuDM
    {
        public Guid SecurityRoleId { get; set; }

        public List<NaztecGuidList> MenuList { get; set; }
    }
}