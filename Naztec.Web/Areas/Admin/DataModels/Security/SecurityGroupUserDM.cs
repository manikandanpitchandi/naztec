﻿namespace ECBase.Web.Areas.Admin.DataModels.Security
{
    using System;
    using ECBase.Web.Models;

    public class SecurityGroupUserDM : BaseDM
    {
        public Guid Id { get; set; }

        public Guid SecurityGroupId { get; set; }

        public Guid SecurityUserId { get; set; }
    }
}