﻿namespace Naztec.Web.Areas.Admin.DataModels.Security
{
    using System;
    using Naztec.Web.Models;

    public class SecurityRoleUserDM : BaseDM
    {
        public Guid Id { get; set; }

        public Guid RoleId { get; set; }

        public Guid UserId { get; set; }
    }
}