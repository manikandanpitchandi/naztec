﻿namespace Naztec.Web.Areas.Admin.DataModels.Security
{
    using System;
    using System.Collections.Generic;
    using Naztec.Core.Entities;

    public class SecurityRoleRightsDM
    {
        public Guid SecurityRoleId { get; set; }

        public List<NaztecGuidList> SecurityRightsList { get; set; }
    }
}