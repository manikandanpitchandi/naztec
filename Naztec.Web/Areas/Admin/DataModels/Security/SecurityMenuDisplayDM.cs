﻿namespace Naztec.Web.Areas.Admin.DataModels.Security
{
    using System;
    using Newtonsoft.Json;

    public class SecurityMenuDisplayDM
    {
        public SecurityMenuDisplayDM()
        {
            this.State = new StateDM();
        }

        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }

        [JsonIgnore]
        public Guid? ParentId { get; set; }

        [JsonProperty(PropertyName = "parent")]
        public string Parent
        {
            get { return this.ParentId != null ? Convert.ToString(this.ParentId) : "#"; }
        }

        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }

        [JsonProperty(PropertyName = "state")]
        public StateDM State { get; set; }
    }
}