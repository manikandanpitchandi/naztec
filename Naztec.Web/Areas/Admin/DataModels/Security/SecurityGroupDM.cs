﻿namespace ECBase.Web.Areas.Admin.DataModels.Security
{
    using System;
    using Models;

    public class SecurityGroupDM : BaseDM
    {
        public Guid Id { get; set; }
        public string SecurityGroupDescription { get; set; }
        public string SecurityGroupName { get; set; }
    }
}