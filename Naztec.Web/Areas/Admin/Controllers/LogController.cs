﻿using Naztec.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Naztec.Web.Infrastructure.Contracts;
using Naztec.Web.Areas.Admin.ViewModels.Log;
using Naztec.Domain.Contracts;
using AutoMapper;

namespace Naztec.Web.Areas.Admin.Controllers
{
    public class LogController : BaseController
    {
        private ILogService logService;
        private IInventoryService invService;

      
        public LogController(IApplicationStore applicationStore, ISessionStore sessionStore, ILogService logService, IInventoryService inventoryService) : base(applicationStore, sessionStore)
        {
            this.logService = logService;
            this.invService = inventoryService;
        }

        [Route("Log/Index")]
        public ActionResult Index()
        {
            return RedirectToAction("Manage");
        }

        [Route("Log/Manage")]
        public ActionResult Manage()
        {
            LogManageVM viewModel = new LogManageVM(this.ApplicationStore, this.SessionStore);

            var data = this.invService.GetInventory();


            Mapper.Map(this.logService.GetLog(), viewModel.Logs);

            return this.View(viewModel);
        }
    }
}