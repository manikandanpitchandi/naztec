﻿namespace Naztec.Web.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using AutoMapper;
    using DataModels.User;
    using Domain.Entities.Admin.Security;
    using Naztec.Domain.Contracts;
    using Naztec.Domain.Entities;
    using Naztec.Web.Areas.Admin.DataModels.Security;
    using Naztec.Web.Areas.Admin.ViewModels.Security;
    using Naztec.Web.Controllers;
    using Naztec.Web.Infrastructure.Contracts;
    using Naztec.Web.Infrastructure.Extensions;
    using ViewModels.User;
    using Core.Helpers;

    public class SecurityController : BaseController
    {
        private ISecurityManagementService securityService;

        public SecurityController(IApplicationStore applicationStore, ISessionStore sessionStore, ISecurityManagementService securityService)
            : base(applicationStore, sessionStore)
        {
            this.securityService = securityService;
        }

        #region Users

        public ActionResult Users()
        {
            UserManageVM viewModel = new UserManageVM(this.ApplicationStore, this.SessionStore);

            viewModel.TimeZoneList = Mapper.Map(TimeZoneHelper.GetTimeZoneList(), viewModel.TimeZoneList);

            Mapper.Map(this.securityService.GetUsers(), viewModel.Users);

            return this.View(viewModel);
        }

        public async Task<ActionResult> CreateUser(UserDM model)
        {
            if (!ModelState.IsValid)
            {
                return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            }

            var response = await this.securityService.CreateUser(Mapper.Map<UserDTO>(model));

            return this.Json(new { success = response.IsOk, user = response.Item, message = response.GetHtmlMessage() });
        }

        public ActionResult EditUser(UserDM model)
        {
            var response = this.securityService.EditUser(Mapper.Map<UserDTO>(model));

            return this.Json(new { success = response.IsOk, user = response.Item, message = response.GetHtmlMessage() });
        } 
        
        #endregion
        
        #region Roles

        [HttpGet]
        public ActionResult Roles()
        {
            SecurityRoleManageVM viewModel = new SecurityRoleManageVM(this.ApplicationStore, this.SessionStore);

            Mapper.Map(this.securityService.GetSecurityRoles(), viewModel.Roles);

            return this.View(viewModel);
        }

        [HttpPost]
        [Route("Security/SaveSecurityRole")]
        public ActionResult SaveSecurityRole(SecurityRoleDM model)
        {
            if (!ModelState.IsValid)
            {
                return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            }

            var response = this.securityService.SaveSecurityRole(Mapper.Map<SecurityRoleDTO>(model));

            return this.Json(new { success = response.IsOk, role = response.Item, message = response.GetHtmlMessage() });
        }

        [HttpGet]
        public JsonResult GetSecurityRightsByRoleId(Guid roleId)
        {
            var viewModel = new SecurityRoleManageVM(this.ApplicationStore, this.SessionStore);

            viewModel.RightsForTreeDisplay = Mapper.Map<List<SecurityRightDisplayDM>>(this.securityService.GetSecurityRights(roleId).ToList());

            viewModel.RoleUsers = this.securityService.GetSecurityRoleUsers(roleId).ToList();

            viewModel.UnAssignedUsers = this.securityService.GetUnAssignedUsers(roleId).ToList();

            viewModel.MenuForTreeDisplay = Mapper.Map<List<SecurityMenuDisplayDM>>(this.securityService.GetSecurityRoleMenu(roleId).ToList());

            return this.JsonNet(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSecurityMenuByRoleId(Guid roleId)
        {
            var viewModel = new SecurityRoleManageVM(this.ApplicationStore, this.SessionStore);

            return this.JsonNet(viewModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("Security/SaveSecurityRoleMenu")]
        public ActionResult SaveSecurityRoleMenu(SecurityRoleMenuDM model)
        {
            if (!ModelState.IsValid)
            {
                return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            }

            var response = this.securityService.SaveSecurityRoleMenus(Mapper.Map<SecurityRoleMenuDTO>(model));

            return this.Json(new { success = response.IsOk, message = response.GetHtmlMessage() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("Security/SaveSecurityRoleRights")]
        public ActionResult SaveSecurityRoleRights(SecurityRoleRightsDM model)
        {
            if (!ModelState.IsValid)
            {
                return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            }

            var response = this.securityService.SaveSecurityRoleRights(Mapper.Map<SecurityRoleRightsDTO>(model));

            return this.Json(new { success = response.IsOk, message = response.GetHtmlMessage() }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("Security/AddUserToRole")]
        public ActionResult AddUserToRole(SecurityRoleUserDM model)
        {
            if (!ModelState.IsValid)
            {
                return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            }

            var response = this.securityService.AddUserToRole(Mapper.Map<SecurityRoleUserDTO>(model));

            return this.Json(new { success = response.IsOk, addedUser = response.Item, message = response.GetHtmlMessage() });
        }

        [HttpPost]
        [Route("Security/RemoveUserFromRole")]
        public ActionResult RemoveUserFromRole(SecurityRoleUserDM model)
        {
            var response = this.securityService.RemoveUserFromRole(Mapper.Map<SecurityRoleUserDTO>(model));

            return this.Json(new { success = response.IsOk, removedUser = response.Item, message = response.GetHtmlMessage() });
        }

        #endregion

        #region Rights

        [HttpGet]
        public ActionResult Rights()
        {
            SecurityRightManageVM viewModel = new SecurityRightManageVM(this.ApplicationStore, this.SessionStore);

            Mapper.Map(this.securityService.GetSecurityRights(), viewModel.Rights);

            return View(viewModel);
        }

        [HttpPost]
        [Route("Security/SaveSecurityRights")]
        public ActionResult SaveSecurityRights(SecurityRightDM model)
        {
            if (!ModelState.IsValid)
            {
                return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            }

            var response = this.securityService.SaveSecurityRights(Mapper.Map<SecurityRightDTO>(model));

            return this.Json(new { success = response.IsOk, right = response.Item, message = response.GetHtmlMessage() });
        }

        public ActionResult DeleteRight(Guid id)
        {
            var response = this.securityService.DeleteRight(id);

            return this.Json(new { success = response.IsOk, message = response.GetHtmlMessage() });
        }

        public ActionResult MoveRight(Guid rightId, bool isMoveUp)
        {
            var response = this.securityService.MoveRight(rightId, isMoveUp);

            var rights = this.securityService.GetSecurityRights();

            return this.Json(new { success = response.IsOk, rights = rights, message = response.GetHtmlMessage() });
        }

        #endregion
    }
}