﻿namespace Naztec.Web.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using AutoMapper;
    using Naztec.Domain.Contracts;
    using Naztec.Domain.Entities;
    using Naztec.Web.Areas.Admin.DataModels.List;
    using Naztec.Web.Areas.Admin.ViewModels.List;
    using Naztec.Web.Controllers;
    using Naztec.Web.Infrastructure.Contracts;
    using Naztec.Web.Infrastructure.Extensions;

    public class ListController : BaseController
    {
        private IListService listService;

        public ListController(IApplicationStore applicationStore, ISessionStore sessionStore, IListService listService)
            : base(applicationStore, sessionStore)
        {
            this.listService = listService;
        }

        [Route("List/Index")]
        public ActionResult Index()
        {
            return RedirectToAction("Manage");
        }

        [Route("List/Manage")]
        public ActionResult Manage()
        {
            ListManageVM viewModel = new ListManageVM(this.ApplicationStore, this.SessionStore);

            Mapper.Map(this.listService.GetListMasters(), viewModel.ListMasters);

            return this.View(viewModel);
        }

        [HttpGet]
        [Route("List/GetListMasters")]
        public ActionResult GetListMasters()
        {
            var viewModel = new ListManageVM(this.ApplicationStore, this.SessionStore);

            return this.Json(new { success = true, lists = Mapper.Map(this.listService.GetListMasters(), viewModel.ListMasters) });
        }

        [HttpGet]
        [Route("List/GetListDetailsByMasterId")]
        public ActionResult GetListDetailsByMasterId(Guid masterId)
        {
            var result = Mapper.Map<List<ListDetailDM>>(this.listService.GetListDetailsByMasterId(masterId));

            return this.Json(new { success = true, items = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Route("List/SaveListMaster")]
        public ActionResult SaveListMaster(ListMasterDM model)
        {
            if (!ModelState.IsValid)
            {
                return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            }

            var response = this.listService.SaveListMaster(Mapper.Map<ListMasterDTO>(model));

            return this.Json(new { success = response.IsOk, listMaster = response.Item, message = response.GetHtmlMessage() });
        }

        [HttpPost]
        [Route("List/SaveListDetail")]
        public ActionResult SaveListDetail(ListDetailDM model)
        {
            if (!ModelState.IsValid)
            {
                return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            }

            var response = this.listService.SaveListDetail(Mapper.Map<ListDetailDTO>(model));

            return this.Json(new { success = response.IsOk, listDetail = response.Item, message = response.GetHtmlMessage() });
        }

        [HttpPost]
        [Route("List/DeleteListMasterById")]
        public ActionResult DeleteListMasterById(Guid id)
        {
            var response = this.listService.DeleteListMasterById(id);

            return this.Json(new { success = response.IsOk, message = response.GetHtmlMessage() });
        }

        [HttpPost]
        [Route("List/DeleteListDetailById")]
        public ActionResult DeleteListDetailById(Guid id)
        {
            var response = this.listService.DeleteListDetailById(id);

            return this.Json(new { success = response.IsOk, id = response.Id, message = response.GetHtmlMessage() });
        }
    }
}