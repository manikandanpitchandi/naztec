﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Naztec.Web.Infrastructure.Contracts;
using Naztec.Web.Infrastructure.Attributes;
using Naztec.Core.Utilities;
using Naztec.Web.Controllers;
using Naztec.Domain.Contracts;

namespace Naztec.Web.Areas.Admin.Controllers
{
    [NoOutputCache]
    public class ScriptController : BaseController
    {
        private IScriptService scriptService;

        public ScriptController(IApplicationStore applicationStore, ISessionStore sessionStore, IScriptService scriptService)
            : base(applicationStore, sessionStore)
        {
            this.scriptService = scriptService;
        }

        [HttpGet]
        public ActionResult GenerateSQL(int moduleId)
        {
            Guard.ForNonZero(moduleId, "Module Id");

            var sqlScript = this.scriptService.GenerateSQL(moduleId);

            return this.Json(new { success = true, script = sqlScript }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DownloadSQL(int moduleId)
        {
            Guard.ForNonZero(moduleId, "Module Id");

            var sqlScript = this.scriptService.GenerateSQL(moduleId);

            return this.Sql(sqlScript, GetModuleName(moduleId));
        }

        [HttpGet]
        public ActionResult GenerateCSharp(int moduleId)
        {
            Guard.ForNonZero(moduleId, "Module Id");

            var cSharpScript = this.scriptService.GenerateCSharp(moduleId);

            return this.Json(new { success = true, script = cSharpScript }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DownloadCSharp(int moduleId)
        {
            Guard.ForNonZero(moduleId, "Module Id");

            var sqlScript = "Select * from AppMenu";

            return this.Sql(sqlScript, GetModuleName(moduleId));
        }

        [HttpGet]
        public ActionResult GenerateJS(int moduleId)
        {
            Guard.ForNonZero(moduleId, "Module Id");

            var jsScript = this.scriptService.GenerateJavascript(moduleId);

            return this.Json(new { success = true, script = jsScript }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DownloadJS(int moduleId)
        {
            Guard.ForNonZero(moduleId, "Module Id");

            var jsScript = "Select * from AppMenu";

            return this.Sql(jsScript, GetModuleName(moduleId));
        }

        private string GetModuleName(int moduleId)
        {
            string moduleName = string.Empty;

            switch (moduleId)
            {
                case 1:
                    moduleName = "List Manager";
                    break;
                case 2:
                    moduleName = "Menu Manager";
                    break;
                case 3:
                    moduleName = "User Manager";
                    break;
                case 4:
                    moduleName = "Rights Manager";
                    break;
                case 5:
                    moduleName = "Role Manager";
                    break;
            }

            return moduleName;
        }
    }
}