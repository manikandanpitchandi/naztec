﻿namespace Naztec.Web.Areas.Admin.Controllers
{
    using System;
    using System.Web.Mvc;
    using AutoMapper;
    using Naztec.Domain.Contracts;
    using Naztec.Domain.Entities;
    using Naztec.Web.Areas.Admin.DataModels.Menu;
    using Naztec.Web.Areas.Admin.ViewModels.Menu;
    using Naztec.Web.Controllers;
    using Naztec.Web.Infrastructure.Contracts;
    using Infrastructure.Extensions;

    public class MenuController : BaseController
    {
        private IMenuService menuService;

        public MenuController(IApplicationStore applicationStore, ISessionStore sessionStore, IMenuService menuService)
            : base(applicationStore, sessionStore)
        {
            this.menuService = menuService;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Manage");
        }

        public ActionResult Manage()
        {
            MenuManageVM viewModel = new MenuManageVM(this.ApplicationStore, this.SessionStore);

            Mapper.Map(this.menuService.GetMenu(), viewModel.Menu);

            return View(viewModel);
        }

        public ActionResult SaveMenu(MenuDM model)
        {
            if (!ModelState.IsValid)
            {
                return this.Json(new { success = false, message = ModelState.ToResponse().GetHtmlMessage() });
            }

            var response = this.menuService.SaveMenu(Mapper.Map<MenuDTO>(model));

            return this.Json(new { success = response.IsOk, menu = response.Item, message = response.GetHtmlMessage() });
        }

        public ActionResult DeleteMenu(Guid id)
        {
            var response = this.menuService.DeleteMenu(id);

            return this.Json(new { success = response.IsOk, message = response.GetHtmlMessage() });
        }

        public ActionResult MoveMenu(Guid menuId, bool isMoveUp)
        {
            var response = this.menuService.MoveMenu(menuId, isMoveUp);

            var menu = this.menuService.GetMenu();

            return this.Json(new { success = response.IsOk, menu = menu, message = response.GetHtmlMessage() });
        }
    }
}