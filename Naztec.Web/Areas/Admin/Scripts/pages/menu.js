﻿function menuManager(settings) {
    naztec.menu = naztec.menu || {};
    naztec.menu = {
        variables: {
            menu: settings.menu
        },
    };
    naztec.bus = new Vue();

    Vue.use(window.vuelidate.default);
    const localValidators = { required, maxLength, minLength } = window.validators;

    Vue.component('menu-wrapper-component',
       Vue.extend({
           template: '#menu-wrapper-template',
           props: ['menu', 'children', 'depth', 'selectedMenuId', 'index', 'siblingCount'],
           data: function () {
               return {
                   isUnderEdit: false,
               }
           },
           validations: {
               menu: {
                   Text: {
                       required,
                       minLength: minLength(4),
                       maxLength: maxLength(50)
                   }
               },
           },
           computed: {
               getLeftMarginDepth: function () {
                   return (depth * 10) + 'px';
               },
               amISelected: function () {
                   return (this.menu.Id == this.selectedMenuId);
               },
               sortedChildren: function () {
                   return _.sortBy(this.children, 'DisplayOrder');
               }
           },
           methods: {
               selectMenu: function () {
                   naztec.bus.$emit('menu-selected', this.menu.Id);
               },
               duplicateMenu: function () {
                   this.createMenu({
                       Id: uuid.v4(),
                       ParentId: this.menu.ParentId,
                       Text: this.menu.Text + ' - Copy',
                       Slug: '',
                       CssClass: '',
                       DisplayOrder: this.siblingCount,
                       IsActive: true
                   });
               },
               createMenuDown: function () {
                   this.createMenu({
                       Id: uuid.v4(),
                       ParentId: this.menu.Id,
                       Text: this.menu.Text + ' - ' + (this.children.length + 1),
                       Slug: '',
                       CssClass: '',
                       DisplayOrder: this.children.length,
                       IsActive: true
                   });
               },
               createMenu: function (menuObj) {
                   $.ajax({
                       method: 'POST',
                       url: settings.rootDir + "Menu/SaveMenu",
                       data: { model: menuObj },
                       success: function (data) {
                           if (data.success === true) {
                               toastr.success(data.message);
                               naztec.bus.$emit('menu-added', data.menu);
                           }
                           else {
                               toastr.error(data.message);
                           }
                       }
                   });
               },
               saveMenu: function () {
                   var that = this;

                   $.ajax({
                       method: 'POST',
                       url: settings.rootDir + "Menu/SaveMenu",
                       data: { model: this.menu },
                       success: function (data) {
                           if (data.success === true) {
                               toastr.success(data.message);
                               that.isUnderEdit = false;
                           }
                           else {
                               toastr.error(data.message);
                           }
                       }
                   });
               },
               deleteMenu: function () {
                   var that = this;

                   $.ajax({
                       method: 'POST',
                       url: settings.rootDir + "Menu/DeleteMenu",
                       data: { id: this.menu.Id },
                       success: function (data) {
                           if (data.success === true) {
                               toastr.success(data.message);
                               naztec.bus.$emit('menu-deleted', that.menu.Id);
                           }
                           else {
                               toastr.error(data.message);
                           }
                       }
                   });
               },

               moveUp: function () {
                   var that = this;

                   $.ajax({
                       method: 'POST',
                       url: settings.rootDir + "Menu/MoveMenu",
                       data: { menuId: that.selectedMenuId, isMoveUp: true },
                       success: function (data) {
                           if (data.success === true) {
                               toastr.success(data.message);
                               naztec.bus.$emit('menu-refresh', data.menu);
                           }
                           else {
                               toastr.error(data.message);
                           }
                       }
                   })

               },

               moveDown: function () {
                   var that = this;

                   $.ajax({
                       method: 'POST',
                       url: settings.rootDir + "Menu/MoveMenu",
                       data: { menuId: that.selectedMenuId, isMoveUp: false },
                       success: function (data) {
                           if (data.success === true) {
                               naztec.bus.$emit('menu-refresh', data.menu);
                               toastr.success(data.message);
                           }
                           else {
                               toastr.error(data.message);
                           }
                       }
                   })
               }
           }
       }));

    var menuManagerApp = new Vue({
        el: '#menuManagerApp',
        data: {
            menu: naztec.menu.variables.menu,
            depth: 0,
            selectedMenuId: ''
        },
        computed: {
            sortedMenu: function () {
                var that = this;

                var tree = [];
                var mappedArr = {};
                var arrElem;
                var mappedElem;

                // First map the nodes of the array to an object -> create a hash table.
                for (var i = 0, len = that.menu.length; i < len; i++) {
                    arrElem = that.menu[i];
                    mappedArr[arrElem.Id] = arrElem;
                    mappedArr[arrElem.Id]['children'] = [];
                }

                for (var id in mappedArr) {
                    if (mappedArr.hasOwnProperty(id)) {
                        mappedElem = mappedArr[id];
                        // If the element is not at the root level, add it to its parent array of children.
                        if (mappedElem.ParentId) {
                            mappedArr[mappedElem['ParentId']]['children'].push(mappedElem);
                        }
                            // If the element is at the root level, add it to first level elements array.
                        else {
                            tree.push(mappedElem);
                        }
                    }
                }

                return _.sortBy(tree, "DisplayOrder");
            }
        },
        methods: {
            createNewRootMenu: function () {
                this.createMenu({
                    Id: uuid.v4(),
                    ParentId: '',
                    Text: 'Root Menu - ' + (this.sortedMenu.length + 1),
                    Slug: '',
                    CssClass: '',
                    DisplayOrder: this.sortedMenu.length,
                    IsActive: true
                });
            },
            createMenu: function (menuObj) {
                var that = this;

                $.ajax({
                    method: 'POST',
                    url: settings.rootDir + "Menu/SaveMenu",
                    data: { model: menuObj },
                    success: function (data) {
                        if (data.success === true) {
                            toastr.success(data.message);
                            that.menu.push(data.menu);
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                });
            },


        },
        created: function () {
            var that = this;
            naztec.bus.$on('menu-selected', function (id) {
                if (that.selectedMenuId == id)
                    that.selectedMenuId = '';
                else
                    that.selectedMenuId = id;
            }.bind(this));
            naztec.bus.$on('menu-added', function (addedMenu) {
                that.menu.push(addedMenu);
            }.bind(this));
            naztec.bus.$on('menu-refresh', function (sortedmenu) {
                that.menu = sortedmenu;
            }.bind(this));
            naztec.bus.$on('menu-deleted', function (id) {
                that.selectedMenuId = '';
                naztec.utilities.common.findAndRemove(this.menu, 'Id', id);
            }.bind(this));
        }
    });
}