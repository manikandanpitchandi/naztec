﻿function securityGroups(settings) {
    ecbase.securityGroups = ecbase.securityGroups || {};

    ecbase.securityGroups = {
        controls: {
            groupRightsTreeView: $('#securityGroupRightsTreeView'),
        },
        variables: {
            groups: settings.groups
        },
    }

    ecbase.bus = new Vue();

    var assignedRights = null;

    Vue.component('group-right-component',
        Vue.extend({
            template: '#group-right-template',
            props: ['group', 'rights'],
            data: function () {
                return {
                    filteredUnAssignedUsers: [],
                    filteredGroupUsers: [],
                    showComponent: false,
                    groupUnderEdit: false,
                    groupBeforeEdit: '',
                    unAssignedUsers: [],
                    groupUsers: [],
                    searchUnAssignedUser: '',
                    searchGroupUser: ''

                }
            },
            computed: {
                sortedunAssignedUsers: function () {
                    return _.sortBy(this.filteredUnAssignedUsers, 'Value');
                },
                sortedGroupUsers: function () {
                    return _.sortBy(this.filteredGroupUsers, 'Value');
                }

            },
            methods: {
                show: function (data) {
                    var that = this;

                    that.unAssignedUsers = data.UnAssignedUsers;
                    that.groupUsers = data.GroupUsers;
                    that.filteredUnAssignedUsers = data.UnAssignedUsers;
                    that.filteredGroupUsers = data.GroupUsers;

                    var rightsForTreeDisplay = data.RightsForTreeDisplay;

                    that.groupUnderEdit = false;

                    $('#securityGroupRightsTreeView').jstree('destroy');

                    this.showComponent = true;

                    Vue.nextTick(function () {

                        $('#securityGroupRightsTreeView').jstree({
                            "core": {
                                "themes": {
                                    "variant": "default",
                                    "dots": false,
                                    "icons": false,
                                    "stripes": true
                                },
                                "data": rightsForTreeDisplay
                            },
                            "checkbox": {
                                "keep_selected_style": false,
                                "three_state": true
                            },
                            "search": {
                                "case_insensitive": true,
                                "show_only_matches": true
                            },
                            "plugins": ["checkbox", "search"]

                        });

                        $('#securityGroupRightsTreeView').on("ready.jstree", function (e, rightsForTreeDisplay) {

                            $('#securityGroupRightsTreeView').jstree('open_all');

                            var currentAppRights = $('#securityGroupRightsTreeView').jstree(true).get_json('#');

                            assignedRights = that.getSelectedRightsJSONModel(currentAppRights);

                        });
                    });
                },
                getSelectedRightsJSONModel: function (data) {
                    var ids = [];

                    var isValid = false;

                    var hasValidChild = false;

                    var item = $('#securityGroupRightsTreeView');

                    var validateChildren = function (children) {

                        var isChildrenValid = false;

                        $.each(children, function (i, item) {
                            if (!item.children || item.children.length === 0) {
                                isChildrenValid = isChildrenValid || item.state.selected;

                                if (item.state.selected == true) {

                                    ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                                }
                            }
                            else {
                                var hasValidChild = validateChildren(item.children);

                                isChildrenValid = isChildrenValid || hasValidChild;

                                if (hasValidChild === true) {

                                    ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                                }
                            }

                        });

                        return isChildrenValid;
                    }

                    $.each(data, function (i, item) {
                        if (!item.children || item.children.length === 0) {

                            isValid = item.state.selected;

                            if (item.state.selected == true) {

                                ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                            }
                        }
                        else {
                            var rootHasChildren = validateChildren(item.children);

                            isValid = isValid || rootHasChildren;

                            if (rootHasChildren === true) {

                                ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                            }
                        }
                    });

                    return ids;
                },
                saveGroupRights: function () {
                    var that = this;

                    var currentAppRights = $('#securityGroupRightsTreeView').jstree(true).get_json('#');
                    var checkedRights = that.getSelectedRightsJSONModel(currentAppRights);

                    var isDirty = ecbase.utilities.common.jsonCompare(assignedRights, checkedRights, true);

                    if (isDirty) {

                        var rights = { securityRightsList: checkedRights, securityGroupId: that.group.Id };

                        $.ajax({
                            method: 'POST',
                            url: settings.rootDir + "Security/SaveSecurityGroupRights",
                            data: ecbase.utilities.common.postify({ model: rights }),
                            success: function (data) {
                                if (data.success === true) {

                                    var currentAppRights = $('#securityGroupRightsTreeView').jstree(true).get_json('#');

                                    assignedRights = that.getSelectedRightsJSONModel(currentAppRights);

                                    toastr.success(data.message);
                                }
                                else {

                                    toastr.error(data.message);

                                }
                            }
                        });

                    }
                    else {
                        toastr.info("No changes available to save");
                    }

                },
                hide: function () {
                    this.showComponent = false;
                },
                editGroup: function () {
                    var that = this;
                    that.groupUnderEdit = true;
                    that.groupBeforeEdit = _.cloneDeep(that.group);
                },
                updateGroup: function (group) {
                    var that = this;
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "security/SaveSecurityGroup",
                        data: { model: group },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                that.groupUnderEdit = false;
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                },
                cancelUpdatingGroup: function () {
                    var that = this;
                    that.group.SecurityGroupName = that.groupBeforeEdit.SecurityGroupName;
                    that.group.SecurityGroupDescription = that.groupBeforeEdit.SecurityGroupDescription;
                    that.groupUnderEdit = false;
                },
                moveToGroup: function (user) {
                    var that = this;
                    var model = { Id: uuid.v4(), SecurityGroupId: that.group.Id, SecurityUserId: user.Id }
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "Security/AddUserToGroup",
                        data: { model: model },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                var resultModel = { Id: model.Id, Value: user.Value };
                                that.groupUsers.push(resultModel);
                                ecbase.utilities.common.findAndRemove(that.filteredUnAssignedUsers, 'Id', user.Id);
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })

                },
                removeFromGroup: function (groupUser) {
                    var that = this;
                    var model = { Id: groupUser.Id, SecurityGroupId: that.group.Id }
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "Security/RemoveUserFromGroup",
                        data: { model: model },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                var resultModel = { Id: data.removedUser.Id, Value: groupUser.Value };
                                ecbase.utilities.common.findAndRemove(that.filteredGroupUsers, 'Id', groupUser.Id);
                                that.unAssignedUsers.push(resultModel);
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                },
            },
            watch: {
                searchUnAssignedUser: function (val) {
                    var formattedVal = val.toLowerCase().trim();

                    if (formattedVal === '') {
                        this.filteredUnAssignedUsers = this.unAssignedUsers;
                    }
                    else {
                        this.filteredUnAssignedUsers = _.filter(this.unAssignedUsers, function (ntm) { return ntm.Value.toLowerCase().indexOf(formattedVal) != -1 });
                    }
                },
                searchGroupUser: function (val) {
                    var formattedVal = val.toLowerCase().trim();

                    if (formattedVal === '') {
                        this.filteredGroupUsers = this.groupUsers;
                    }
                    else {
                        this.filteredGroupUsers = _.filter(this.groupUsers, function (ntm) { return ntm.Value.toLowerCase().indexOf(formattedVal) != -1 });
                    }
                }
            }
        }));

    var securityGroupApp = new Vue({
        el: "#securityGroupApp",
        data: {
            groups: ecbase.securityGroups.variables.groups,
            filteredGroups: ecbase.securityGroups.variables.groups,
            selectedGroup: {},
            selectedGroupId: '',
            searchCriteria: '',
            groupRights: '',
            newGroup: {
                Id: uuid.v4(),
                SecurityGroupName: '',
                SecurityGroupDescription: ''
            }
        },
        computed: {
            groupCount: function () {
                return (this.groups) ? this.groups.length : 0;
            },
            sortedGroups: function () {
                return _.sortBy(this.filteredGroups, 'SecurityGroupName');
            }
        },
        methods: {
            initialize: function () {
                var that = this;
                that.newGroup.Id = uuid.v4(),
                that.newGroup.SecurityGroupName = '',
                that.newGroup.SecurityGroupDescription = ''

            },
            showGroupModal: function () {
                $('#newGroupModal').modal();
            },
            createGroup: function () {
                var that = this;

                $.ajax({
                    method: "POST",
                    url: settings.rootDir + "Security/SaveSecurityGroup",
                    data: { model: that.newGroup },
                    success: function (data) {
                        if (data.success === true) {
                            toastr.success(data.message);
                            $('#newGroupModal').modal('hide');
                            that.initialize();
                            that.groups.push(data.group);
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                })
            },

            selectGroup: function (group) {

                var that = this;
                that.selectedGroupId = group.Id;
                that.selectedGroup = group;

                $.ajax({
                    method: "GET",
                    url: settings.rootDir + "security/GetSecurityRightsByGroupId",
                    data: {
                        groupId: this.selectedGroupId
                    },
                    success: function (data) {
                        that.$refs.groupcomponent.show(data);
                    }
                });
            },

            amISelected: function (group) {
                return (group.Id == this.selectedGroupId);
            }

        },
        watch: {
            searchCriteria: function (val) {
                var formattedVal = val.toLowerCase().trim();

                if (formattedVal === '') {
                    this.filteredGroups = this.groups;
                }
                else {
                    this.filteredGroups = _.filter(this.groups, function (ntm) { return ntm.SecurityGroupName.toLowerCase().indexOf(formattedVal) != -1 });
                }
            }
        }
    })

}