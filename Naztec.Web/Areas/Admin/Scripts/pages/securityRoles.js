﻿function securityRoles(settings) {
    naztec.securityRoles = naztec.securityRoles || {};

    naztec.securityRoles = {
        controls: {
        },
        variables: {
            roles: settings.roles
        },
    }

    naztec.bus = new Vue();

    Vue.use(window.vuelidate.default);
    const localValidators = { required, maxLength, minLength, sameAs, numeric } = window.validators;

    var assignedRights = null;

    Vue.component('role-right-component',
        Vue.extend({
            template: '#role-right-template',
            props: ['role', 'rights'],
            data: function () {
                return {
                    filteredUnAssignedUsers: [],
                    filteredRoleUsers: [],
                    showComponent: false,
                    roleUnderEdit: false,
                    roleBeforeEdit: '',
                    unAssignedUsers: [],
                    roleUsers: [],
                    searchUnAssignedUser: '',
                    searchRoleUser: '',
                    role: {
                        Name: '',
                    },
                }
            },
            validations: {
                role: {
                    Name: {
                        required
                    }
                },
            },
            computed: {
                sortedunAssignedUsers: function () {
                    return _.sortBy(this.filteredUnAssignedUsers, 'Value');
                },
                sortedRoleUsers: function () {
                    return _.sortBy(this.filteredRoleUsers, 'Value');
                }
            },
            methods: {
                show: function (data) {
                    var that = this;

                    that.unAssignedUsers = data.UnAssignedUsers;
                    that.roleUsers = data.RoleUsers;
                    that.filteredUnAssignedUsers = data.UnAssignedUsers;
                    that.filteredRoleUsers = data.RoleUsers;
                    that.renderRightsTree(data.RightsForTreeDisplay);
                    that.renderMenuTree(data.MenuForTreeDisplay);
                    that.roleUnderEdit = false;
                    this.showComponent = true;
                },
                renderRightsTree: function (data) {

                    var rightsForTreeDisplay = data;

                    $('#securityRoleRightsTreeView').jstree('destroy');

                    Vue.nextTick(function () {

                        $('#securityRoleRightsTreeView').jstree({
                            "core": {
                                "themes": {
                                    "variant": "default",
                                    "dots": false,
                                    "icons": false,
                                    "stripes": true
                                },
                                "data": rightsForTreeDisplay
                            },
                            "checkbox": {
                                "keep_selected_style": false,
                                "three_state": true
                            },
                            "search": {
                                "case_insensitive": true,
                                "show_only_matches": true
                            },
                            "plugins": ["checkbox", "search"]

                        });

                        $('#securityRoleRightsTreeView').on("ready.jstree", function (e, rightsForTreeDisplay) {

                            $('#securityRoleRightsTreeView').jstree('open_all');

                            var currentAppRights = $('#securityRoleRightsTreeView').jstree(true).get_json('#');

                            assignedRights = that.getSelectedRightsJSONModel(currentAppRights);

                        });
                    });
                },
                renderMenuTree: function (data) {

                    var menuForTreeDisplay = data;

                    $('#securityRoleMenuTreeView').jstree('destroy');

                    Vue.nextTick(function () {

                        $('#securityRoleMenuTreeView').jstree({
                            "core": {
                                "themes": {
                                    "variant": "default",
                                    "dots": false,
                                    "icons": false,
                                    "stripes": true
                                },
                                "data": menuForTreeDisplay
                            },
                            "checkbox": {
                                "keep_selected_style": false,
                                "three_state": true
                            },
                            "search": {
                                "case_insensitive": true,
                                "show_only_matches": true
                            },
                            "plugins": ["checkbox", "search"]

                        });

                        $('#menuForTreedisplay').on("ready.jstree", function (e, menuForTreeDisplay) {

                            $('#menuForTreedisplay').jstree('open_all');

                            var currentAppMenu = $('#menuForTreedisplay').jstree(true).get_json('#');

                            assignedMenu = that.getSelectedMenuJSONModel(currentAppMenu);

                        });
                    });
                },
                getSelectedRightsJSONModel: function (data) {
                    var ids = [];

                    var isValid = false;

                    var hasValidChild = false;

                    var item = $('#securityRoleRightsTreeView');

                    var validateChildren = function (children) {

                        var isChildrenValid = false;

                        $.each(children, function (i, item) {
                            if (!item.children || item.children.length === 0) {
                                isChildrenValid = isChildrenValid || item.state.selected;

                                if (item.state.selected == true) {

                                    ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                                }
                            }
                            else {
                                var hasValidChild = validateChildren(item.children);

                                isChildrenValid = isChildrenValid || hasValidChild;

                                if (hasValidChild === true) {

                                    ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                                }
                            }

                        });

                        return isChildrenValid;
                    }

                    $.each(data, function (i, item) {
                        if (!item.children || item.children.length === 0) {

                            isValid = item.state.selected;

                            if (item.state.selected == true) {

                                ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                            }
                        }
                        else {
                            var rootHasChildren = validateChildren(item.children);

                            isValid = isValid || rootHasChildren;

                            if (rootHasChildren === true) {

                                ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                            }
                        }
                    });

                    return ids;
                },
                getSelectedMenuJSONModel: function (data) {
                    var ids = [];

                    var isValid = false;

                    var hasValidChild = false;

                    var item = $('#securityRoleMenuTreeView');

                    var validateChildren = function (children) {

                        var isChildrenValid = false;

                        $.each(children, function (i, item) {
                            if (!item.children || item.children.length === 0) {
                                isChildrenValid = isChildrenValid || item.state.selected;

                                if (item.state.selected == true) {

                                    ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                                }
                            }
                            else {
                                var hasValidChild = validateChildren(item.children);

                                isChildrenValid = isChildrenValid || hasValidChild;

                                if (hasValidChild === true) {

                                    ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                                }
                            }

                        });

                        return isChildrenValid;
                    }

                    $.each(data, function (i, item) {
                        if (!item.children || item.children.length === 0) {

                            isValid = item.state.selected;

                            if (item.state.selected == true) {

                                ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                            }
                        }
                        else {
                            var rootHasChildren = validateChildren(item.children);

                            isValid = isValid || rootHasChildren;

                            if (rootHasChildren === true) {

                                ids.push({ Id: item.id, Value: item.text, IsSelected: item.state.IsSelected || true });

                            }
                        }
                    });

                    return ids;
                },
                saveRoleRights: function () {
                    var that = this;

                    var currentAppRights = $('#securityRoleRightsTreeView').jstree(true).get_json('#');
                    var checkedRights = that.getSelectedRightsJSONModel(currentAppRights);

                    var isDirty = naztec.utilities.common.jsonCompare(assignedRights, checkedRights, true);

                    if (isDirty) {

                        var rights = { securityRightsList: checkedRights, securityRoleId: that.role.Id };

                        $.ajax({
                            method: 'POST',
                            url: settings.rootDir + "Security/SaveSecurityRoleRights",
                            data: naztec.utilities.common.postify({ model: rights }),
                            success: function (data) {
                                if (data.success === true) {

                                    var currentAppRights = $('#securityRoleRightsTreeView').jstree(true).get_json('#');

                                    assignedRights = that.getSelectedRightsJSONModel(currentAppRights);

                                    toastr.success(data.message);
                                }
                                else {

                                    toastr.error(data.message);

                                }
                            }
                        });

                    }
                    else {
                        toastr.info("No changes available to save");
                    }

                },
                saveRoleMenu: function () {
                    var that = this;

                    var currentAppMenu = $('#securityRoleMenuTreeView').jstree(true).get_json('#');
                    var checkedMenu = that.getSelectedMenuJSONModel(currentAppMenu);

                    var isDirty = naztec.utilities.common.jsonCompare(currentAppMenu, checkedMenu, true);

                    if (isDirty) {

                        var menu = { MenuList: checkedMenu, securityRoleId: that.role.Id };

                        $.ajax({
                            method: 'POST',
                            url: settings.rootDir + "Security/SaveSecurityRoleMenu",
                            data: naztec.utilities.common.postify({ model: menu }),
                            success: function (data) {
                                if (data.success === true) {

                                    var currentmenu = $('#securityRoleMenuTreeView').jstree(true).get_json('#');

                                    assignedMenu = that.getSelectedMenuJSONModel(currentmenu);

                                    toastr.success(data.message);
                                }
                                else {

                                    toastr.error(data.message);

                                }
                            }
                        });

                    }
                    else {
                        toastr.info("No changes available to save");
                    }

                },
                hide: function () {
                    this.showComponent = false;
                },
                editRole: function () {
                    var that = this;
                    that.roleUnderEdit = true;
                    that.roleBeforeEdit = _.cloneDeep(that.role);
                },
                updateRole: function (role) {
                    var that = this;
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "security/SaveSecurityRole",
                        data: { model: role },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                that.roleUnderEdit = false;
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                },
                cancelUpdatingRole: function () {
                    var that = this;
                    that.role.Name = that.roleBeforeEdit.Name;
                    that.roleUnderEdit = false;
                },
                moveToRole: function (user) {
                    var that = this;
                    var model = { Id: uuid.v4(), RoleId: that.role.Id, UserId: user.Id }
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "Security/AddUserToRole",
                        data: { model: model },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                var resultModel = { Id: user.Id, Value: user.Value };
                                that.roleUsers.push(resultModel);
                                naztec.utilities.common.findAndRemove(that.filteredUnAssignedUsers, 'Id', user.Id);
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })

                },
                removeFromRole: function (roleUser) {
                    var that = this;
                    var model = { UserId: roleUser.Id, RoleId: that.role.Id }
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "Security/RemoveUserFromRole",
                        data: { model: model },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                var resultModel = { Id: data.removedUser.Id, Value: roleUser.Value };
                                naztec.utilities.common.findAndRemove(that.filteredRoleUsers, 'Id', roleUser.Id);
                                that.unAssignedUsers.push(resultModel);
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                },
            },
            watch: {
                searchUnAssignedUser: function (val) {
                    var formattedVal = val.toLowerCase().trim();

                    if (formattedVal === '') {
                        this.filteredUnAssignedUsers = this.unAssignedUsers;
                    }
                    else {
                        this.filteredUnAssignedUsers = _.filter(this.unAssignedUsers, function (ntm) { return ntm.Value.toLowerCase().indexOf(formattedVal) != -1 });
                    }
                },
                searchRoleUser: function (val) {
                    var formattedVal = val.toLowerCase().trim();

                    if (formattedVal === '') {
                        this.filteredRoleUsers = this.roleUsers;
                    }
                    else {
                        this.filteredRoleUsers = _.filter(this.roleUsers, function (ntm) { return ntm.Value.toLowerCase().indexOf(formattedVal) != -1 });
                    }
                }
            }
        }));

    var securityRoleApp = new Vue({
        el: "#securityRoleApp",
        data: {
            roles: naztec.securityRoles.variables.roles,
            filteredRoles: naztec.securityRoles.variables.roles,
            selectedRole: {},
            selectedRoleId: '',
            searchCriteria: '',
            roleRights: '',
            newRole: {
                Id: uuid.v4(),
                Name: ''
            }
        },
        validations: {
            newRole: {
                Name: {
                    required
                }
            },
        },
        computed: {
            roleCount: function () {
                return (this.roles) ? this.roles.length : 0;
            },
            sortedRoles: function () {
                return _.sortBy(this.filteredRoles, 'Name');
            }
        },
        methods: {
            initialize: function () {
                var that = this;
                that.newRole.Id = uuid.v4(),
                that.newRole.Name = ''
            },
            showRoleModal: function () {
                $('#newRoleModal').modal();
            },
            createRole: function () {
                this.$v.newRole.$touch();
                if (!this.$v.newRole.$invalid) {
                    var that = this;
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "Security/SaveSecurityRole",
                        data: { model: that.newRole },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                $('#newRoleModal').modal('hide');
                                that.initialize();
                                that.roles.push(data.role);
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                }
            },
            cancelCreateRole: function () {
                $('#newRoleModal').modal('hide');
                this.$v.newRole.$reset();
            },
            selectRole: function (role) {
                var that = this;
                that.selectedRoleId = role.Id;
                that.selectedRole = role;
                $.ajax({
                    method: "GET",
                    url: settings.rootDir + "security/GetSecurityRightsByRoleId",
                    data: {
                        roleId: this.selectedRoleId
                    },
                    success: function (data) {
                        that.$refs.rolecomponent.show(data);
                    }
                });
            },
            amISelected: function (role) {
                return (role.Id == this.selectedRoleId);
            }
        },
        watch: {
            searchCriteria: function (val) {
                var formattedVal = val.toLowerCase().trim();

                if (formattedVal === '') {
                    this.filteredRoles = this.roles;
                }
                else {
                    this.filteredRoles = _.filter(this.roles, function (ntm) { return ntm.Name.toLowerCase().indexOf(formattedVal) != -1 });
                }
            }
        }
    })

}