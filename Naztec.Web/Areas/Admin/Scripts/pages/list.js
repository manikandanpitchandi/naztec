﻿function listManager(settings) {
    naztec.lists = naztec.lists || {};

    naztec.lists = {
        controls: {},
        variables: {
            lists: settings.lists
        },
        methods: {}
    }

    naztec.bus = new Vue();

    Vue.component('list-component',
        Vue.extend({
            template: '#list-template',
            props: ['list', 'listItems'],
            data: function () {
                return {
                    showListEditorComponent: false,
                    listMasterUnderEdit: false,
                    listDetailUnderEdit: '',
                    searchCriteria: '',
                    itemUnderCreation: false,
                    filteredListItems: [],
                    masterBeforeEdit: '',
                    detailBeforeEdit: '',
                    newListDetail: { Code: '', Description: '' },
                    listDetailIdForDelete: '',
                    listMasterIdForDelete: ''
                }
            },
            computed: {

            },
            methods: {
                show: function () {
                    var that = this;
                    this.showListEditorComponent = true;
                    Vue.nextTick(function () {
                        that.filteredListItems = that.listItems;
                    })
                },
                hide: function () {
                    this.showListEditorComponent = false;
                },
                showListIds: function(){
                    $('#displayIdsModal').modal();
                },
                copyId: function (listId) {                    
                    var copyText = document.getElementById('' + listId + '');
                    copyText.select();
                    try {
                        var successful = document.execCommand('copy');
                        toastr.success("Id copied to Clipboard");
                    } catch (err) {
                        console.log('Oops, unable to copy');
                    }
                },
                updateListMaster: function (id, masterName) {

                    if (!masterName.trim()) {
                        toastr.error("List Name is required");
                        return false;
                    }

                    var that = this;
                    var model = { Id: id, MasterName: masterName };

                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "List/SaveListMaster",
                        data: model,
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                that.listMasterUnderEdit = false;
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                },
                deleteListMaster: function () {
                    var that = this;

                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "List/DeleteListMasterById",
                        data: { id: that.listMasterIdForDelete },
                        success: function (data) {
                            if (data.success === true) {
                                naztec.bus.$emit('master-removed', that.listMasterIdForDelete);
                                $('#deleteListMasterModal').modal('hide');
                                that.showListEditorComponent = false;
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                },
                showDeleteListMaster: function () {
                    this.listMasterIdForDelete = this.list.Id;
                    $('#deleteListMasterModal').modal();
                },
                showDeleteListDetail: function (item) {
                    this.listDetailIdForDelete = item.Id;
                    $('#deleteListDetailsModal').modal();
                },
                editListMaster: function () {
                    var that = this;
                    that.listMasterUnderEdit = true;
                    that.masterBeforeEdit = _.cloneDeep(that.list);
                },
                cancelEditMaster: function () {
                    var that = this;
                    that.listMasterUnderEdit = false;
                    that.list.MasterName = that.masterBeforeEdit.MasterName;
                },
                itemUnderEdit: function (item) {
                    return this.listDetailUnderEdit == item.Id;
                },
                editListDetail: function (item) {
                    var that = this;
                    that.listDetailUnderEdit = item.Id;
                    that.listDetailBeforeEdit = _.cloneDeep(item);
                },
                cancelEditListDetail: function (item) {
                    var that = this;
                    that.listDetailUnderEdit = false;
                    item.Code = that.listDetailBeforeEdit.Code;
                    item.Description = that.listDetailBeforeEdit.Description;
                },
                showDeleteListDetail: function (item) {
                    this.listDetailIdForDelete = item.Id;
                    $('#deleteListDetailsModal').modal();
                },
                deleteListDetail: function () {
                    var that = this;
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "List/DeleteListDetailById",
                        data: { id: that.listDetailIdForDelete },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                $('#deleteListDetailsModal').modal('hide');
                                naztec.utilities.common.findAndRemove(that.filteredListItems, 'Id', data.id);
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                },
                updateListDetail: function (item) {
                    var that = this;

                    if (!item.Code.trim()) {
                        toastr.error("Code is required");
                        return false;
                    }

                    if (!item.Description.trim()) {
                        toastr.error("Description is required");
                        return false;
                    }


                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "List/SaveListDetail",
                        data: { model: item },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                that.listDetailUnderEdit = false;
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                },
                cancelNewlistDetails: function () {
                    var that = this;
                    that.itemUnderCreation = false;
                    that.newListDetail.Code = '',
                    that.newListDetail.Description = '',
                    that.newListDetail.Id = ''
                },

                saveListDetail: function () {
                    var that = this;

                    that.newListDetail.Id = uuid.v4();

                    if (!that.newListDetail.Code.trim()) {
                        toastr.error("Code is required");
                        return false;
                    }

                    if (!that.newListDetail.Description.trim()) {
                        toastr.error("Description is required");
                        return false;
                    }

                    that.newListDetail.MasterId = that.list.Id;
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "List/SaveListDetail",
                        data: { model: that.newListDetail },
                        success: function (data) {
                            if (data.success === true) {
                                toastr.success(data.message);
                                that.itemUnderCreation = false;
                                that.filteredListItems.push(data.listDetail);
                                that.newListDetail.Code = '',
                                that.newListDetail.Description = '',
                                that.newListDetail.Id = ''
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                }
            },
            created: function () {
            },
            watch: {
                searchCriteria: function (val) {
                    var formattedVal = val.toLowerCase().trim();

                    if (formattedVal === '') {
                        this.filteredListItems = this.listItems;
                    }
                    else {
                        this.filteredListItems = _.filter(this.listItems, function (ntm) { return ntm.Description.toLowerCase().indexOf(formattedVal) != -1 });
                    }
                }
            }
        }));

    var listManagerApp = new Vue({
        el: '#listManagerApp',
        data: {
            lists: settings.lists,
            selectedListId: '',
            selectedList: {},
            selectedListItems: [],
            searchCriteria: '',
            filteredLists: settings.lists,
            newMasterName: ''
        },
        computed: {
            listCount: function () {
                return (this.lists) ? this.lists.length : 0;
            },
            sortedLists: function () {
                return _.sortBy(this.filteredLists, 'MasterName');
            }

        },
        methods: {
            showNewListModal: function () {
                this.newMasterName = "";
                $('#newListModal').modal();
            },
            createList: function () {
                var that = this;
                var model = { MasterName: this.newMasterName };

                if (!this.newMasterName.trim()) {
                    toastr.error("List Name is required");
                    return false;
                }

                $.ajax({
                    method: "POST",
                    url: settings.rootDir + "List/SaveListMaster",
                    data: model,
                    success: function (data) {
                        if (data.success === true) {
                            toastr.success(data.message);
                            $('#newListModal').modal('hide');
                            that.lists.push(data.listMaster);
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                })
            },
            selectList: function (list) {
                this.selectedListId = list.Id;
                this.selectedList = list;
                var that = this;
                $.ajax({
                    method: "GET",
                    url: settings.rootDir + "List/GetListDetailsByMasterId",
                    data: {
                        masterId: this.selectedListId
                    },
                    success: function (data) {
                        if (data.success === true) {
                            that.selectedListItems = data.items;
                            that.$refs.listcomponent.show();
                        }
                        else {

                        }
                    }
                });
            },
            amISelected: function (list) {
                return (list.Id == this.selectedListId);
            }
        },
        created: function () {
            naztec.bus.$on('master-removed', function (masterId) {
                naztec.utilities.common.findAndRemove(this.lists, 'Id', masterId);
                toastr.success('List was removed.');
            }.bind(this));
        },
        watch: {
            searchCriteria: function (val) {
                var formattedVal = val.toLowerCase().trim();

                if (formattedVal === '') {
                    this.filteredLists = this.lists;
                }
                else {
                    this.filteredLists = _.filter(this.lists, function (ntm) { return ntm.MasterName.toLowerCase().indexOf(formattedVal) != -1 });
                }
            }
        }
    });
}