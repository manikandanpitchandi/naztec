﻿function roleManager(settings) {
    ecbase.roles = ecbase.roles || {};
    ecbase.roles = {
        controls: {},
        variables: {
            roles: settings.roles
        },
        methods: {}
    }
    ecbase.bus = new Vue();

    Vue.use(window.vuelidate.default);
    const localValidators = { required, maxLength, minLength, sameAs,  numeric } = window.validators;

    var roleManagerApp = new Vue({
        el: '#roleManagerApp',
        data: {
            isRoleUnderEdit: false,
            roles: ecbase.roles.variables.roles,
            selectedRoleId: '',
            selectedRole: [],
            searchCriteria: '',
            filteredroles: ecbase.roles.variables.roles,
            showComponent: false,
            role: {
                Name: '',
            },
            roleBeforeEdit: '',
            roleIdDelete:'',
        },
        validations: {
            role: {
                Name: {
                    required
                }
            },
        },
        computed: {
            roleCount: function () {
                return (this.roles) ? this.roles.length : 0;
            },
            sortedRoles: function () {
                return _.sortBy(this.roles, 'Name');
            },
            amISelected: function (role) {
                return role.Id == this.selectedRoleId;
            },
            showRole: function () {
                return this.selectedRoleId != '';
            }
        },
        methods: {
            editRole: function () {
                var that = this;
                that.isRoleUnderEdit = true;
                that.roleBeforeEdit = _.cloneDeep(that.role);
            },
            cancelRoleEdit: function () {
                var that = this;
                that.isRoleUnderEdit = false;
                that.role = that.roleBeforeEdit;
            },
            editRoleInfo: function () {
                var that = this;
                var model = { Id: that.role.Id, Name: that.role.Name };

                $.ajax({
                    method: "POST",
                    url: settings.rootDir + "Security/SaveRole",
                    data: model,
                    success: function (data) {
                        if (data.success === true) {
                            toastr.success(data.message);
                            that.isRoleUnderEdit = false;
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                });
            },
            showNewRoleModal: function () {
                $('#newRoleModal').modal();
            },
            createRole: function () {
                var that = this;
                var model = { Id: '', Name: this.role.Name };

                $.ajax({
                    method: "POST",
                    url: settings.rootDir + "Security/Saverole",
                    data: model,
                    success: function (data) {
                        if (data.success === true) {
                            toastr.success(data.message);
                            $('#newRoleModal').modal('hide');
                            that.roles.push(data.securityRole);
                            that.role.Name = '';
                            this.$v.role.$reset();
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                })
            },
            selectedRoleName: function (role) {
                this.selectedRoleId = role.Id;
                this.role = role;
                var that = this;
                $.ajax({
                    method: "GET",
                    url: settings.rootDir + "Security/GetRoleById",
                    data: {
                        roleId: this.selectedRoleId
                    },
                    success: function (data) {
                        if (data.success === true) {
                            that.role = data.items;
                            that.isRoleUnderEdit = false;
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                });
            },
            amISelected: function (role) {
                return (role.Id == this.selectedRoleId);
            },
            showDeleteRole: function () {
                this.roleIdDelete = this.role.Id;
                $('#deleteRoleModal').modal();
            },
            cancelCreateRole: function () {
                $('#newRoleModal').modal('hide');
                var that = this;
                that.role.Id = '';
                that.role.Name = '';
                this.$v.role.$reset();
            },
        },
        created: function () {
        },
        watch: {
            searchCriteria: function (val) {
                var formattedVal = val.toLowerCase().trim();

                if (formattedVal === '') {
                    this.filteredroles = this.roles;
                }
                else {
                    this.filteredroles = _.filter(this.roles, function (ntm) { return ntm.Name.toLowerCase().indexOf(formattedVal) != -1 });
                }
            }
        }
    });
}