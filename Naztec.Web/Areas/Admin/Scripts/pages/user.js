﻿function userManager(settings) {
    naztec.users = naztec.users || {};
    naztec.users = {
        controls: {},
        variables: {
            users: settings.users,
            timezones: settings.timezones,
        },
        methods: {}
    }
    naztec.bus = new Vue();

    Vue.use(window.vuelidate.default);
    const localValidators = { required, maxLength, minLength, numeric, email } = window.validators;

    var userManagerApp = new Vue({
        el: '#userManagerApp',
        data: {            
            listView: true,
            users: naztec.users.variables.users,
            timezones: naztec.users.variables.timezones,
            selectedUserId: '',
            selectedUser: {
                Id: '',
                FirstName: '',
                LastName: '',
                PhoneNumber: '',
                Email: '',
                ImageId: null,
                Timezone: ''
            },
            isUnderEdit: false,
            searchCriteria: '',
            filteredUsers: naztec.users.variables.users,
            newUser: {
                Id: uuid.v4(),
                FirstName: '',
                LastName: '',
                PhoneNumber: '',
                Email: '',
                Timezone:'',
            },
            userBeforeEdit: ''
        },
        validations: {
            newUser: {
                FirstName: {
                    required,
                },
                LastName: {
                    required,
                },
                PhoneNumber: {
                    required,
                    numeric,
                    maxLength: maxLength(10)
                },
                Email: {
                    required,
                    email
                },
                Timezone: {
                    required,
                }
            },
            selectedUser: {
                FirstName: {
                    required,
                },
                LastName: {
                    required,
                },
                PhoneNumber: {
                    required,
                    numeric,
                    maxLength: maxLength(10)
                },
                Timezone: {
                    required,
                }
            }
        },
        computed: {
            sortedUsers: function () {
                return _.sortBy(this.filteredUsers, 'FirstName');
            },
            showUser: function () {
                return this.selectedUserId != '';
            }
        },
        methods: {
            setCurrentDisplay: function(key){
                this.currentUiView = key;
            },
            imageUrl: function (user) {
                if ((user.ImageId != null) && (user.ImageId != naztec.globals.emptyGuid)) {
                    return '/Image/Picture?imageid=' + user.ImageId;
                }
                else {
                    return naztec.utilities.ui.generateProfilePicture(user.FirstName + ' ' + user.LastName, 90);
                }
            },
            showNewUserModal: function () {
                this.selectedUserId = '';
                $('#newUserModal').modal();
            },
            createNewUser: function () {
                this.$v.newUser.$touch();

                var that = this;
                if (!this.$v.newUser.$invalid) {
                    $.ajax({
                        method: "POST",
                        url: settings.rootDir + "Security/CreateUser",
                        data: that.newUser,
                        success: function (data) {
                            if (data.success === true) {
                                that.users.push(data.user);

                                toastr.success(data.message);

                                that.cancelCreateNewUser();
                            }
                            else {
                                toastr.error(data.message);
                            }
                        }
                    })
                }
            },
            cancelCreateNewUser: function () {
                var that = this;

                $('#newUserModal').modal('hide');

                that.newUser.Id = uuid.v4();
                that.newUser.Email = '';
                that.newUser.FirstName = '';
                that.newUser.LastName = '';
                that.newUser.PhoneNumber = '';
                that.newUser.Timezone = '';

                this.$v.newUser.$reset();
            },

            selectUser: function (user) {
                var that = this;

                if (that.selectedUserId != user.Id) {
                    that.selectedUser = _.cloneDeep(user);
                    that.selectedUserId = user.Id;
                }
                else {
                    that.selectedUser = {};
                    that.selectedUserId = '';
                }
            },
            amISelected: function (user) {
                return (user.Id == this.selectedUserId);
            },

            editUser: function () {
                var that = this;
                that.isUnderEdit = true;
                that.userBeforeEdit = _.cloneDeep(that.selectedUser);
            },
            updateUser: function () {
                var that = this;

                $.ajax({
                    method: "POST",
                    url: settings.rootDir + "Security/EditUser",
                    data: that.selectedUser,
                    success: function (data) {
                        if (data.success === true) {
                            toastr.success(data.message);
                            that.isUnderEdit = false;
                            naztec.bus.$emit('user-updated', data.user);
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                });
            },
            cancelUpdateUser: function () {
                var that = this;
                that.isUnderEdit = false;
                that.selectedUser = that.userBeforeEdit;
            },
        },
        created: function () {
            var that = this;
            naztec.bus.$on('user-updated', function (user) {
                var updatedUser = _.find(that.users, { 'Id': user.Id });
                updatedUser.FirstName = user.FirstName;
                updatedUser.LastName = user.LastName;
                updatedUser.PhoneNumber = user.PhoneNumber;
                updatedUser.Timezone = user.Timezone;
            }.bind(this));
        },
        watch: {
            searchCriteria: function (val) {
                var formattedVal = val.toLowerCase().trim();

                if (formattedVal === '') {
                    this.filteredUsers = this.users;
                }
                else {
                    this.filteredUsers = _.filter(this.users, function (ntm) { return (ntm.FirstName + ' ' + ntm.LastName).toLowerCase().indexOf(formattedVal) != -1 });
                }
            }
        }
    });
}