﻿function logManager(settings) {
    naztec.logManager = naztec.logManager || {};

    naztec.logManager = {
        controls: {
            logtable: $('#logtable')
        },
        variables: {
            logs: settings.logs
        },
    }

    naztec.logManager.controls.logtable.dataTable({
        "data": naztec.logManager.variables.logs,
        "columns": [
             { "title": "Date", "data": "StringDateTime" },
             { "title": "Logger", "data": "Logger" },
            { "title": "Message", "data": "Message" },
        ],
        'paging': true,
        'lengthChange': false,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': true
    });
}