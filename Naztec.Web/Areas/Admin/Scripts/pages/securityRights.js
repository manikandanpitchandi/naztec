﻿function securityRights(settings) {
    naztec.securityRights = naztec.securityRights || {};

    naztec.securityRights = {
        controls: {},
        variables: {
            rights: settings.rights
        },
    };

    naztec.bus = new Vue();

    Vue.use(window.vuelidate.default);
    const localValidators = { required, maxLength, minLength } = window.validators;

    Vue.component('rights-wrapper-component',
   Vue.extend({
       template: '#rights-wrapper-template',
       props: ['right', 'children', 'depth', 'selectedRightId', 'index', 'siblingCount'],
       data: function () {
           return {
               isUnderEdit: false,
           }
       },
       validations: {
           right: {
               SecurityRightDefinition: {
                   required,
                   minLength: minLength(4),
                   maxLength: maxLength(50)
               }
           },
       },
       computed: {
           getLeftMarginDepth: function () {
               return (depth * 10) + 'px';
           },
           amISelected: function () {
               return (this.right.Id == this.selectedRightId);
           },
           sortedChildren: function () {
               return _.sortBy(this.children, 'DisplayOrder');
           }
       },
       methods: {
           selectRight: function () {
               naztec.bus.$emit('right-selected', this.right.Id);
           },
           createRightDown: function () {
               this.createRight({
                   Id: uuid.v4(),
                   ParentRightId: this.right.Id,
                   SecurityRightDefinition: this.right.SecurityRightDefinition + ' - ' + (this.children.length + 1),
                   DisplayOrder: this.children.length,
                   ApplicationSecurity: false,
                   IsHidden: true
               });
           },
           createRight: function (rightObj) {
               var that = this;

               $.ajax({
                   method: 'POST',
                   url: settings.rootDir + "Security/SaveSecurityRights",
                   data: { model: rightObj },
                   success: function (data) {
                       if (data.success === true) {
                           toastr.success(data.message);
                           naztec.bus.$emit('right-added', data.right);
                       }
                       else {
                           toastr.error(data.message);
                       }
                   }
               });
           },
           saveRight: function () {
               var that = this;

               $.ajax({
                   method: 'POST',
                   url: settings.rootDir + "Security/SaveSecurityRights",
                   data: { model: this.right },
                   success: function (data) {
                       if (data.success === true) {
                           toastr.success(data.message);
                           naztec.bus.$emit('right-added', data.right);
                           that.isUnderEdit = false;
                       }
                       else {
                           toastr.error(data.message);
                       }
                   }
               });
           },
           deleteRight: function () {
               var that = this;

               $.ajax({
                   method: 'POST',
                   url: settings.rootDir + "Security/DeleteRight",
                   data: { id: this.right.Id },
                   success: function (data) {
                       if (data.success === true) {
                           toastr.success(data.message);
                           naztec.bus.$emit('right-deleted', that.right.Id);
                       }
                       else {
                           toastr.error(data.message);
                       }
                   }
               });
           },
           moveUp: function () {
               var that = this;

               $.ajax({
                   method: 'POST',
                   url: settings.rootDir + "Security/MoveRight",
                   data: { rightId: that.selectedRightId, isMoveUp: true },
                   success: function (data) {
                       if (data.success === true) {
                           toastr.success(data.message);
                           naztec.bus.$emit('right-refresh', data.rights);
                       }
                       else {
                           toastr.error(data.message);
                       }
                   }
               })

           },
           moveDown: function () {
               var that = this;

               $.ajax({
                   method: 'POST',
                   url: settings.rootDir + "Security/MoveRight",
                   data: { rightId: that.selectedRightId, isMoveUp: false },
                   success: function (data) {
                       if (data.success === true) {
                           naztec.bus.$emit('right-refresh', data.rights);
                           toastr.success(data.message);
                       }
                       else {
                           toastr.error(data.message);
                       }
                   }
               })
           }
       }
   }));

    var securityRightApp = new Vue({
        el: '#securityRightApp',
        data: {
            rights: naztec.securityRights.variables.rights,
            depth: 0,
            selectedRightId: ''
        },
        computed: {
            sortedRights: function () {
                var that = this;

                var tree = [];
                var mappedArr = {};
                var arrElem;
                var mappedElem;

                // First map the nodes of the array to an object -> create a hash table.
                for (var i = 0, len = that.rights.length; i < len; i++) {
                    arrElem = that.rights[i];
                    mappedArr[arrElem.Id] = arrElem;
                    mappedArr[arrElem.Id]['children'] = [];
                }

                for (var id in mappedArr) {
                    if (mappedArr.hasOwnProperty(id)) {
                        mappedElem = mappedArr[id];
                        // If the element is not at the root level, add it to its parent array of children.
                        if (mappedElem.ParentRightId) {
                            mappedArr[mappedElem['ParentRightId']]['children'].push(mappedElem);
                        }
                            // If the element is at the root level, add it to first level elements array.
                        else {
                            tree.push(mappedElem);
                        }
                    }
                }

                return _.sortBy(tree, "DisplayOrder");
            }
        },
        methods: {
            createNewRight: function () {
                this.createRight({
                    Id: uuid.v4(),
                    ParentRightId: '',
                    SecurityRightDefinition: 'Right - ' + (this.sortedRights.length + 1),
                    ApplicationSecurity: false,
                    DisplayOrder: this.sortedRights.length,
                    IsHidden: true
                });
            },

            createRight: function (rightObj) {
                var that = this;

                $.ajax({
                    method: 'POST',
                    url: settings.rootDir + "Security/SaveSecurityRights",
                    data: { model: rightObj },
                    success: function (data) {
                        if (data.success === true) {
                            toastr.success(data.message);
                            that.rights.push(data.right);
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                });
            },

        },
        created: function () {
            var that = this;
            naztec.bus.$on('right-selected', function (id) {
                if (that.selectedRightId == id)
                    that.selectedRightId = '';
                else
                    that.selectedRightId = id;
            }.bind(this));

            naztec.bus.$on('right-added', function (addedRight) {
                that.rights.push(addedRight);
            }.bind(this));
            naztec.bus.$on('right-refresh', function (sortedRight) {
                that.rights = sortedRight;
            }.bind(this));
            naztec.bus.$on('right-deleted', function (id) {
                that.selectedRightId = '';
                naztec.utilities.common.findAndRemove(this.rights, 'Id', id);
            }.bind(this));
        },
        watch: {
        }
    });

}