﻿function scriptGenerator(settings) {

    var scriptGeneratorApp = new Vue({
        el: '#scriptGenerator',
        data: {
            moduleName: settings.moduleName,
            moduleId: settings.moduleId,
            modes: settings.modes,
            sqlData: '...',
            csharpData: '...',
            jsData: '...',
        },
        methods: {
            show: function () {
                $('#scriptModal').modal();
            },
            generateSQL: function () {
                var that = this;

                $.ajax({
                    method: "GET",
                    url: settings.rootDir + "Script/GenerateSql",
                    data: { moduleId: this.moduleId },
                    success: function (data) {
                        if (data.success === true) {                            
                            that.sqlData = data.script;
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                });
            },
            copySQL: function(){
                var copyText = document.getElementById("sqlTextArea");
                copyText.select();
                try {
                    var successful = document.execCommand('copy');
                    toastr.success("Script copied to Clipboard");
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
            },
            generateCSharp: function () {               
                var that = this;

                $.ajax({
                    method: "GET",
                    url: settings.rootDir + "Script/GenerateCSharp",
                    data: { moduleId: this.moduleId },
                    success: function (data) {
                        if (data.success === true) {
                            that.csharpData = data.script;
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                });
            },
            copyCSharp: function(){
                var copyText = document.getElementById("csharpTextArea");
                copyText.select();
                try {
                    var successful = document.execCommand('copy');
                    toastr.success("Script copied to Clipboard");
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
            },
            generateJS: function () {
                var that = this;

                $.ajax({
                    method: "GET",
                    url: settings.rootDir + "Script/GenerateJS",
                    data: { moduleId: this.moduleId },
                    success: function (data) {
                        if (data.success === true) {
                            that.jsData = data.script;
                        }
                        else {
                            toastr.error(data.message);
                        }
                    }
                });
            },
            copyJS: function () {
                var copyText = document.getElementById("jsTextArea");
                copyText.select();
                try {
                    var successful = document.execCommand('copy');
                    toastr.success("Script copied to Clipboard");
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
            }
        }
    });
};