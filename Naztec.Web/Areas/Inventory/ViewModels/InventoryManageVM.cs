﻿using Naztec.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Naztec.Web.Infrastructure.Contracts;

namespace Naztec.Web.Areas.Inventory.ViewModels
{
    public class InventoryManageVM : BaseVM
    {
        public InventoryManageVM(IApplicationStore applicationStore, ISessionStore sessionStore) : base(applicationStore, sessionStore)
        {
            this.Data = new List<InventoryDM>();
        }

        public List<InventoryDM> Data { get; set; }
    }
}