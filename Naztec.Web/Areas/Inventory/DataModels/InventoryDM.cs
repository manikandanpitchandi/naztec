﻿using System;

namespace Naztec.Web.Areas
{
    public class InventoryDM
    {
        public Guid InventoryId { get; set; }

        public string PartNumber { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }
    }
}