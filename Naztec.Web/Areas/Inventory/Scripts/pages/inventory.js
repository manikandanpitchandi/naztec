﻿function inventoryManager(settings) {
    naztec.inventoryManager = naztec.inventoryManager || {};

    naztec.inventoryManager = {
        controls: {
            logtable: $('#inventorytable')
        },
        variables: {
            data: settings.data
        },
    }

    naztec.inventoryManager.controls.logtable.dataTable({
        "data": naztec.inventoryManager.variables.data,
        "columns": [
            { "title": "Name", "data": "Name" },
            { "title": "PartNumber", "data": "PartNumber" },
            { "title": "Description", "data": "Description" },
        ],
        'paging': true,
        'lengthChange': false,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': true
    });
}