﻿using System.Web.Mvc;
using AutoMapper;
using Naztec.Domain.Contracts;
using Naztec.Web.Areas.Inventory.ViewModels;
using Naztec.Web.Controllers;
using Naztec.Web.Infrastructure.Contracts;

namespace Naztec.Web.Areas.Inventory.Controllers
{
    public class InventoryController : BaseController
    {
        private IInventoryService inventoryService;
        public InventoryController(IApplicationStore applicationStore, ISessionStore sessionStore, IInventoryService inventoryService) : base(applicationStore, sessionStore)
        {
            this.inventoryService = inventoryService;
        }

        // GET: Inventory/Inventory
        [Route("Inventory/Manage")]
        public ActionResult Manage()
        {
            InventoryManageVM viewModel = new InventoryManageVM(this.ApplicationStore, this.SessionStore);
                        
            Mapper.Map(this.inventoryService.GetInventory(), viewModel.Data);

            return this.View(viewModel);
        }
    }
}