namespace Naztec.Web.Infrastructure.Contracts
{
    public interface ISessionStore
    {
        void Set<T>(string key, T value);

        T Get<T>(string key);

        bool HaveKey(string key);
    }
}
