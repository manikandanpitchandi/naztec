namespace Naztec.Web.Infrastructure.Contracts
{
    public interface IApplicationStore
    {
        void Set<T>(string key, T value);

        T Get<T>(string key);
    }
}
