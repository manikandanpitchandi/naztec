namespace Naztec.Web.Infrastructure.Extensions
{
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Caching;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using Naztec.Core.Entities;
    using Newtonsoft.Json;

    public static class MVCExtensions
    {
        public static MvcHtmlString ToJSON(this object value)
        {
            return new MvcHtmlString(JsonConvert.SerializeObject(value, new JsonSerializerSettings { StringEscapeHandling = StringEscapeHandling.EscapeHtml }).Replace(@"\", @"\\"));
        }

        public static NaztecResponse ToResponse(this ModelStateDictionary modelState)
        {
            var response = new NaztecResponse();

            foreach (var error in modelState.Values.SelectMany(x => x.Errors))
            {
                response.Error(error.ErrorMessage);
            }

            return response;
        }

        public static void AddModelErrors(this ModelStateDictionary modelState, NaztecResponseBase response)
        {
            foreach (var errorMessage in response.ErrorMessages)
            {
                modelState.AddModelError(errorMessage.Name, errorMessage.Message);
            }
        }

        public static string GetPageURL(this HttpContextBase httpContext)
        {
            var url = string.Empty;

            if (httpContext != null && httpContext.Request != null && !string.IsNullOrWhiteSpace(httpContext.Request.Path))
            {
                url = httpContext.Request.Path;
                if (httpContext.Request.ApplicationPath != "/")
                {
                    url = url.Replace(httpContext.Request.ApplicationPath, string.Empty);
                }
            }

            return url;
        }

        public static string GetReferalPageURL(this HttpContextBase httpContext)
        {
            var url = string.Empty;

            if (httpContext.Request != null && httpContext.Request.UrlReferrer != null && !string.IsNullOrWhiteSpace(httpContext.Request.UrlReferrer.AbsolutePath))
            {
                url = httpContext.Request.UrlReferrer.AbsolutePath;
                if (httpContext.Request.ApplicationPath != "/")
                {
                    url = url.Replace(httpContext.Request.ApplicationPath, string.Empty);
                }
            }

            return url;
        }

        public static string GetPageURL(this HttpContext httpContext)
        {
            var url = string.Empty;

            if (httpContext != null && httpContext.Request != null && !string.IsNullOrWhiteSpace(httpContext.Request.Path))
            {
                url = httpContext.Request.Path;
                if (httpContext.Request.ApplicationPath != "/")
                {
                    url = url.Replace(httpContext.Request.ApplicationPath, string.Empty);
                }
            }

            return url;
        }

        public static string GetReferalPageURL(this HttpContext httpContext)
        {
            var url = string.Empty;

            if (httpContext.Request != null && httpContext.Request.UrlReferrer != null && !string.IsNullOrWhiteSpace(httpContext.Request.UrlReferrer.AbsolutePath))
            {
                url = httpContext.Request.UrlReferrer.AbsolutePath;
                if (httpContext.Request.ApplicationPath != "/")
                {
                    url = url.Replace(httpContext.Request.ApplicationPath, string.Empty);
                }
            }

            return url;
        }

        public static string StampVersion(this string htmlString)
        {
            var devFileRelativePath = htmlString;

            if (HttpRuntime.Cache[devFileRelativePath] == null)
            {
                var minFileRelativePath = devFileRelativePath.Insert(devFileRelativePath.LastIndexOf('.'), ".min");

                var devFileAbsolutePath = HttpContext.Current.Server.MapPath(devFileRelativePath);

                var minFileAbsolutePath = HttpContext.Current.Server.MapPath(minFileRelativePath);

                var minFileExist = File.Exists(minFileAbsolutePath);

                var desiredAbsolutePath = !HttpContext.Current.IsDebuggingEnabled && minFileExist ? minFileAbsolutePath : devFileAbsolutePath;

                var desiredRelativePath = !HttpContext.Current.IsDebuggingEnabled && minFileExist ? minFileRelativePath : devFileRelativePath;

                var lastWriteTime = File.GetLastWriteTime(desiredAbsolutePath);

                var result = string.Format("{0}?v={1}", desiredRelativePath, lastWriteTime.Ticks);

                HttpRuntime.Cache.Insert(devFileRelativePath, result, new CacheDependency(desiredAbsolutePath));
            }

            return HttpRuntime.Cache[devFileRelativePath] as string;
        }

        public static void RenderPartialIf(this HtmlHelper htmlHelper, string partialViewName)
        {
            htmlHelper.RenderPartialIf(partialViewName, false);
        }

        public static void RenderPartialIf(this HtmlHelper htmlHelper, string partialViewName, bool condition)
        {
            if (!condition)
            {
                return;
            }

            htmlHelper.RenderPartial(partialViewName);
        }

        public static void RenderPartialIf(this HtmlHelper htmlHelper, string partialViewName, object model, bool condition)
        {
            if (!condition)
            {
                return;
            }

            htmlHelper.RenderPartial(partialViewName, model);
        }

        public static string PartialViewToString(this Controller controller)
        {
            return controller.PartialViewToString(null, null);
        }

        public static string RenderPartialViewToString(this Controller controller, string viewName)
        {
            return controller.PartialViewToString(viewName, null);
        }

        public static string RenderPartialViewToString(this Controller controller, object model)
        {
            return controller.PartialViewToString(null, model);
        }

        public static string PartialViewToString(this Controller controller, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = controller.ControllerContext.RouteData.GetRequiredString("action");
            }

            controller.ViewData.Model = model;

            using (StringWriter stringWriter = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, stringWriter);
                viewResult.View.Render(viewContext, stringWriter);
                return stringWriter.GetStringBuilder().ToString();
            }
        }
    }
}
