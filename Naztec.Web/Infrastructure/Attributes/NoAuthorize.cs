﻿namespace Naztec.Web.Infrastructure.Attributes
{
    using System;

    public class NoAuthorize : Attribute
    {
    }
}