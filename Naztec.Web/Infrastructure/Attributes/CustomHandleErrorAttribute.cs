﻿namespace Naztec.Web.Infrastructure.Attributes
{
    using System.Web.Mvc;
    using System.Web.Routing;

    //using log4net;    

    public class CustomHandleErrorAttribute : HandleErrorAttribute
    {
        //private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;

            //Log.Warn("OnException - " + exception);

            RouteValueDictionary redirectTargetDictionary = new RouteValueDictionary();
            redirectTargetDictionary.Add("action", "Unknown");
            redirectTargetDictionary.Add("controller", "Error");
            context.Result = new RedirectToRouteResult(redirectTargetDictionary);

            context.ExceptionHandled = true;

            base.OnException(context);
        }
    }
}
