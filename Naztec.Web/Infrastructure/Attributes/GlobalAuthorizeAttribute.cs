namespace Naztec.Web.Infrastructure.Attributes
{
    using Contracts;
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    public class GlobalAuthorizeAttribute : AuthorizeAttribute
    {
        private bool isSessionExpired;

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);

            this.Order = 0;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Session[SessionConstants.SecurityUserId] == null)
            {
                this.isSessionExpired = true;

                return false;
            }

            var user = httpContext.User;

            if (user == null || user.Identity == null || !user.Identity.IsAuthenticated)
            {
                this.isSessionExpired = true;
                return false;
            }

            var sessionStore = DependencyResolver.Current.GetService<ISessionStore>();

            if (sessionStore == null)
            {
                return false;
            }

            return true;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                if (this.isSessionExpired)
                {
                    filterContext.Result = new JsonResult { Data = new { success = false, isSessionExpired = true, message = "SessionExpired" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
                else
                {
                    filterContext.Result = new JsonResult { Data = new { success = false, isAccessDenied = true, message = "Access Denied" }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
                }
            }
            else
            {
                if (this.isSessionExpired)
                {
                    filterContext.Result = new RedirectResult("~/Auth/SessionExpired");
                }
                else
                {
                    filterContext.Result = new RedirectResult("~/Auth/AccessDenied");
                }
            }
        }
    }
}
