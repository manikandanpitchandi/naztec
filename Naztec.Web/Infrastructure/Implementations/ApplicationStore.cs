namespace Naztec.Web.Infrastructure.Implementations
{
    using System.Web;
    using Naztec.Web.Infrastructure.Contracts;

    public class ApplicationStore : IApplicationStore
    {
        public void Set<T>(string key, T value)
        {
            HttpContext.Current.Application[key] = value;
        }

        public T Get<T>(string key)
        {
            var keyValue = HttpContext.Current.Application[key];

            if (keyValue != null)
            {
                return (T)keyValue;
            }

            return default(T);
        }
    }
}
