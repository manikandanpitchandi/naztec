namespace Naztec.Web.Infrastructure.Implementations
{
    using System.Web;
    using Naztec.Web.Infrastructure.Contracts;

    public class SessionStore : ISessionStore
    {
        public void Set<T>(string key, T value)
        {
            HttpContext.Current.Session[key] = value;
        }

        public T Get<T>(string key)
        {
            var keyValue = HttpContext.Current.Session[key];

            if (keyValue != null)
            {
                return (T)keyValue;
            }

            return default(T);
        }

        public bool HaveKey(string key)
        {
            return HttpContext.Current.Session[key] != null;
        }
    }
}
