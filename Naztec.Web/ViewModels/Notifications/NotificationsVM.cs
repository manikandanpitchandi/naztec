﻿namespace Naztec.Web.ViewModels.Notifications
{
    using Naztec.Web.Infrastructure.Contracts;

    public class NotificationsVM : BaseVM
    {
        public NotificationsVM(IApplicationStore applicationStore, ISessionStore sessionStore) : base(applicationStore, sessionStore)
        {
        }
    }
}