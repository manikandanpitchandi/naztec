﻿namespace Naztec.Web.ViewModels
{
    using Naztec.Web.Infrastructure.Contracts;
    using Naztec.Web.Infrastructure.Implementations;

    public class AppMenuVM : BaseVM
    {
        public AppMenuVM(IApplicationStore applicationStore, ISessionStore sessionStore) : base(new ApplicationStore(), new SessionStore())
        {
            this.Class = string.Empty;
        }

        public int Id { get; set; }

        public string Caption { get; set; }

        public string Description { get; set; }

        public int? ParentMenuId { get; set; }

        public string MenuType { get; set; }

        public int? PageId { get; set; }

        public int? MenuOrder { get; set; }

        public bool? IsActive { get; set; }

        public string Class { get; set; }

        public string PagePath { get; set; }

        public bool IsSelected { get; set; }

        public bool IsActiveParent { get; set; }
    }
}