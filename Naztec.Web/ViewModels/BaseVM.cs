namespace Naztec.Web.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using Domain.Contracts;
    using Naztec.Web.Infrastructure.Contracts;
    using Naztec.Web.Models;
    using Microsoft.AspNet.Identity;
    using System.Security.Claims;
    using System.Linq;
    using Infrastructure.Extensions;
    using Domain.Entities;

    public class BaseVM
    {
        private IApplicationStore applicationStore;
        private ISessionStore sessionStore;

        public BaseVM(IApplicationStore applicationStore, ISessionStore sessionStore)
        {
            this.applicationStore = applicationStore;
            this.sessionStore = sessionStore;
        }

        public Guid SessionSecurityUserId
        {
            get
            {
                return this.sessionStore.Get<Guid>(SessionConstants.SecurityUserId);
            }
        }

        public string SessionFullName
        {
            get
            {
                return this.sessionStore.Get<string>(SessionConstants.FullName);
            }
        }

        public string UserName
        {
            get
            {
                return this.sessionStore.Get<string>(SessionConstants.UserName);
            }
        }

        public Guid SessionRoleId
        {
            get
            {
                return this.sessionStore.Get<Guid>(SessionConstants.RoleId);
            }

            private set
            {
            }
        }

        public List<Guid> SessionRoleIds
        {
            get
            {
                return this.sessionStore.Get<List<Guid>>(SessionConstants.RoleIds);
            }

            private set
            {
            }
        }

        public List<AppMenuDTO> GetMenu()
        {
            var userMenu = new List<AppMenuDTO>();

            var service = DependencyResolver.Current.GetService<ISecurityManagementService>();

            if (service == null)
            {
                return userMenu;
            }

            if (!this.sessionStore.HaveKey(SessionConstants.AppMenu))
            {
                this.sessionStore.Set(SessionConstants.AppMenu, service.GetMenu(this.SessionRoleIds).ToList());
            }

            var menuItems = this.sessionStore.Get<List<AppMenuDTO>>(SessionConstants.AppMenu).ToList();

            menuItems.ForEach(x =>
            {
                x.IsSelected = false;
                x.IsActiveParent = false;
            });

            var currentMenu = menuItems.FirstOrDefault(x => string.Compare(x.Slug, HttpContext.Current.GetPageURL(), true) == 0);

            if (currentMenu != null)
            {
                currentMenu.IsSelected = true;                

                while (currentMenu != null && currentMenu.ParentMenuId.HasValue)
                {
                    currentMenu = menuItems.FirstOrDefault(x => x.Id == currentMenu.ParentMenuId);
                    if(currentMenu != null)
                    {
                        currentMenu.IsActiveParent = true;
                    }
                }

                //do {
                //    parentMenu = menuItems.FirstOrDefault(x => x.Id == currentMenu.ParentMenuId);

                //    if (parentMenu != null)
                //    {
                //        parentMenu.IsActiveParent = true;
                //    }
                //} while (currentMenu.ParentMenuId != null);





                //var parentMenu = menuItems.FirstOrDefault(x => x.Id == currentMenu.ParentMenuId);

                //if (parentMenu != null)
                //{
                //    parentMenu.IsActiveParent = true;
                //}
            }

            return menuItems;
        }

        public IEnumerable<AppMenuDTO> GetUserMenuByParentId(Guid? parentId)
        {
            var menu = this.GetMenu();

            return menu.Where(x => x.ParentMenuId == parentId).OrderBy(x => x.MenuOrder) ?? Enumerable.Empty<AppMenuDTO>();
        }

        public string GetTheme()
        {
            var claimIdentity = HttpContext.Current?.User?.Identity as ClaimsIdentity;
            var skinTheme = claimIdentity?.FindFirst(NaztecConstants.Claims.SkinTheme);

            if(skinTheme == null)
            {
                return "skin-blue";
            }
            else
            {
                return skinTheme.Value;
            }
        }

        public string GetRoles()
        {
            var claimIdentity = HttpContext.Current?.User?.Identity as ClaimsIdentity;
            var claims = claimIdentity.Claims;
            var roleClaimType = claimIdentity.RoleClaimType;
            var roles = claims.Where(c => c.Type == ClaimTypes.Role).Select(c => c.Value).ToList();

            return String.Join(", ", roles);
        }

        public Guid AppUserImageId
        {
            get
            {
                return this.sessionStore.Get<Guid>(SessionConstants.AppUserImageId);
            }
        }
    }
}
