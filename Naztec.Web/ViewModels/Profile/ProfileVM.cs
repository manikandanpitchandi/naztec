﻿namespace Naztec.Web.ViewModels
{
    using Naztec.Web.Infrastructure.Contracts;
    using Infrastructure.Implementations;
    using Models;
    using System;
    using System.ComponentModel.DataAnnotations;

    public class ProfileVM : BaseVM
    {
        public ProfileVM(IApplicationStore applicationStore, ISessionStore sessionStore) 
            : base(new ApplicationStore(), new SessionStore())
        {
        }      

        public ProfileDM Profile { get; set; }

        public ChangePasswordDM ChangePassword { get; set; }
    }
}