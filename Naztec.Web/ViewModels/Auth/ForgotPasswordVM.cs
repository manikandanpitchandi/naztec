﻿namespace Naztec.Web.ViewModels
{
    using Infrastructure.Implementations;
    using System.ComponentModel.DataAnnotations;

    public class ForgotPasswordVM : BaseVM
    {
        public ForgotPasswordVM()
         : base(new ApplicationStore(), new SessionStore())
        {

        }

        [Required]
        public string Email { get; set; }
    }
}