﻿using Naztec.Web.Infrastructure.Contracts;
using Naztec.Web.Infrastructure.Implementations;
using System.ComponentModel.DataAnnotations;

namespace Naztec.Web.ViewModels
{
    public class ResetPasswordVM : BaseVM
    {
        public ResetPasswordVM()
            : base(new ApplicationStore(), new SessionStore())
        {
        }

        public ResetPasswordVM(IApplicationStore applicationStore, ISessionStore sessionStore)
            : base(applicationStore, sessionStore)
        {
        }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
}