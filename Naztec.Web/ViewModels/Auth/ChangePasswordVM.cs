﻿namespace Naztec.Web.ViewModels
{
    using Naztec.Web.Infrastructure.Contracts;
    using Naztec.Web.Infrastructure.Implementations;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class ChangePasswordVM : BaseVM
    {
        public ChangePasswordVM()
            : base(new ApplicationStore(), new SessionStore())
        {
        }

        public ChangePasswordVM(IApplicationStore applicationStore, ISessionStore sessionStore)
            : base(applicationStore, sessionStore)
        {
        }

       
        public string EmailId { get; set; }

        [Required(ErrorMessage = "The old password field is required.")]
        [DataType(DataType.Password)]
        [DisplayName("Old Password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "The new password filed is required.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "The confirm password field is required.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}