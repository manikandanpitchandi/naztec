namespace Naztec.Web.ViewModels
{
    using Naztec.Web.Infrastructure.Implementations;
    using Naztec.Web.ViewModels;
    using Models;
    using System.ComponentModel;

    public class LoginVM : BaseVM
    {
        public LoginVM() : base(new ApplicationStore(), new SessionStore())
        {

        }
        public LoginDM Login { get; set; }

        [DisplayName("Remember Me")]
        public bool RememberMe { get; set; }
    }
}
