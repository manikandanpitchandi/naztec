namespace Naztec.Web.ViewModels
{
    using Naztec.Web.Infrastructure.Implementations;

    public class DashboardVM : BaseVM
    {
        public DashboardVM() : base(new ApplicationStore(), new SessionStore())
        {

        }
    }
}
