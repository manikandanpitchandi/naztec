﻿using Naztec.Web.Infrastructure.Contracts;
using Naztec.Web.Infrastructure.Implementations;
using Naztec.Web.ViewModels;
using System;

namespace Naztec.Web.Models
{
    public class ProfileImageVM : BaseVM
    {
        public ProfileImageVM()
            : base(new ApplicationStore(), new SessionStore())
        {
        }

        public ProfileImageVM(IApplicationStore applicationStore, ISessionStore sessionStore)
            : base(applicationStore, sessionStore)
        {
        }

        public Guid AppUserId { get; set; }

        public Guid ImageName { get; set; }

        public byte[] ImageBinaryData { get; set; }
    }
}