﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Naztec.Web.Models
{
    public class ProfileDM : BaseDM
    {
        public ProfileDM()
        {

        }

        public Guid Id { get; set; }

        public string Email { get; set; }

        [Required(ErrorMessage = "FirstName is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "LastName is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "PhoneNumber is required")]
        public string PhoneNumber { get; set; }

        public Guid? ImageId { get; set; }

        public byte[] ProfileImage { get; set; }

        public string ProfileImageUrl
        {
            get; set;
        }
    }
}