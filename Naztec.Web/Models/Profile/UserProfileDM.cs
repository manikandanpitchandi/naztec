﻿using Naztec.Web.ViewModels;

namespace Naztec.Web.Models
{
    public class UserProfileVM
    {
        public ProfileDM ProfileDM { get; set; }
        public ChangePasswordDM ChangePasswordVM { get; set; }
    }
}