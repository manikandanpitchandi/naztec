﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Naztec.Web.Models
{
    public class ChangePasswordDM : BaseDM
    {
        public ChangePasswordDM()
        { 
        }

        public string EmailId { get; set; }

        [Required(ErrorMessage = "The old password field is required.")]
        [DataType(DataType.Password)]
        [DisplayName("Old Password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "The new password field is required.")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "The confirm password field is required.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}