﻿namespace Naztec.Web.Models
{
    using Newtonsoft.Json;

    public class NaztecStringDM
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("text")]
        public string Value { get; set; }

        [JsonProperty("selected")]
        public bool IsSelected { get; set; }
    }
}