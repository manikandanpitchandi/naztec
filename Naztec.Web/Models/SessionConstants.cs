namespace Naztec.Web.Models
{
    using System;

    public static class SessionConstants
    {
        public static String SecurityUserId { get { return "SECURITYUSERID"; } }
        public static String FullName { get { return "FULLNAME"; } }
        public static String UserName { get { return "USERNAME"; } }
        public static String AppMenu { get { return "APPMENU"; } }
        public static String RoleId { get { return "ROLEID"; } }
        public static string AppUserImageId
        {
            get { return "APPUSERIMAGEID"; }
        }
        public static string AppUserId
        {
            get { return "APPUSERID"; }
        }
        public static String RoleIds { get { return "ROLEIDS"; } }
    }
}
