namespace Naztec.Data.Implementations
{
    using System;

    using Naztec.Data.Contracts;

    public abstract class BaseMultiTenantUnitOfWorkFactory<TUnitOfWork, TKey> : IMultiTenantUnitOfWorkFactory<TKey> where TUnitOfWork : IUnitOfWork
    {
        private readonly IConnectionStringProvider<TKey> connectionStringProvider;

        protected BaseMultiTenantUnitOfWorkFactory(IConnectionStringProvider<TKey> connectionStringProvider)
        {
            this.connectionStringProvider = connectionStringProvider;
        }

        public IUnitOfWork GetUnitOfWork(TKey connectionStringKey)
        {
            return (IUnitOfWork)Activator.CreateInstance(typeof(TUnitOfWork), this.connectionStringProvider.GetConnectionString(connectionStringKey));
        }
    }
}
