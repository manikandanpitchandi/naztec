namespace Naztec.Data.Implementations
{
    using System;

    using Naztec.Data.Contracts;

    public abstract class BaseSingleTenantUnitOfWorkFactory<TUnitOfWork> : ISingleTenantUnitOfWorkFactory where TUnitOfWork : IUnitOfWork
    {
        private string connectionString;

        public BaseSingleTenantUnitOfWorkFactory(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public IUnitOfWork GetUnitOfWork()
        {
            return (IUnitOfWork)Activator.CreateInstance(typeof(TUnitOfWork), this.connectionString);
        }
    }
}
