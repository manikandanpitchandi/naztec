namespace Naztec.Data.Implementations
{
    using Naztec.Data.Contracts;

    public class AdminUnitOfWork : BaseUnitOfWork<AdminContext>, IAdminUnitOfWork
    {
        public AdminUnitOfWork(string connectionString)
            : base(connectionString)
        {
        }
    }
}
