namespace Naztec.Data.Implementations.UnitOfWorks
{
    using Naztec.Data.Contracts;

    public class AdminUnitOfWorkFactory : BaseUnitOfWorkFactory<AdminUnitOfWork>, IAdminUnitOfWorkFactory
    {
        public AdminUnitOfWorkFactory(string connectionString)
            : base(connectionString)
        {
        }
    }
}
