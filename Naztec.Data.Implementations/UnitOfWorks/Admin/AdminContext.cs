namespace Naztec.Data.Implementations
{
    using System.Data.Entity;
    using Naztec.Data.Entities;
    
    public partial class AdminContext : BaseContext<AdminContext>
    {
        public AdminContext(string connectionString)
            : base(connectionString)
        {
        }
        
        public DbSet<AppMenu> AppMenus { get; set; }
        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<ListDetail> ListDetails { get; set; }
        public DbSet<ListMaster> ListMasters { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<SchemaVersion> SchemaVersions { get; set; }
        public DbSet<SecurityRight> SecurityRights { get; set; }
        public DbSet<SecurityRoleMenu> SecurityRoleMenus { get; set; }
        public DbSet<SecurityRoleRight> SecurityRoleRights { get; set; }
        public DbSet<SecurityRole> SecurityRoles { get; set; }
        public DbSet<SecurityUserClaim> SecurityUserClaims { get; set; }
        public DbSet<SecurityUserLogin> SecurityUserLogins { get; set; }
        public DbSet<SecurityUserRole> SecurityUserRoles { get; set; }
        public DbSet<SecurityUser> SecurityUsers { get; set; }
    
    }   
}