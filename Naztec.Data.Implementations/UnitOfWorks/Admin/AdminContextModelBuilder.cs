namespace Naztec.Data.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using Data.Entities;

    public partial class AdminContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<SecurityRole>().HasKey(x => x.Id);
            modelBuilder.Entity<SecurityUserRole>().HasKey(x => new { x.UserId, x.RoleId });
            modelBuilder.Entity<SecurityUserRole>().HasRequired(x => x.SecurityRole).WithMany(X=>X.Users).HasForeignKey(x => x.RoleId);
            modelBuilder.Entity<SecurityUserRole>().HasRequired(x => x.SecurityUser).WithMany(x => x.Roles).HasForeignKey(x => x.UserId);
            modelBuilder.Entity<SecurityUserClaim>().HasKey(x => x.Id);
            modelBuilder.Entity<SecurityUserLogin>().HasKey(x => new { x.UserId, x.LoginProvider, x.ProviderKey });
            
            modelBuilder.Entity<AppUser>().HasRequired(x => x.SecurityUser).WithOptional(x => x.AppUser);
        }
    }
}
