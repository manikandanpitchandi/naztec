namespace Naztec.Data.Implementations.UnitOfWorks
{
    using System;
    using System.Data.Entity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Naztec.Data.Entities;

    public class SecurityIdentityContext : IdentityDbContext<SecurityUser, SecurityRole, Guid, SecurityUserLogin, SecurityUserRole, SecurityUserClaim>
    {
        static SecurityIdentityContext()
        {
            Database.SetInitializer<SecurityIdentityContext>(null);            
        }

        public SecurityIdentityContext(string connectionString)
            : base(connectionString)
        {
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
        }

        public static SecurityIdentityContext Create(string connectionString)
        {
            return new SecurityIdentityContext(connectionString);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<SecurityUser>().ToTable("SecurityUsers");
            modelBuilder.Entity<SecurityRole>().ToTable("SecurityRoles");
            modelBuilder.Entity<SecurityUserRole>().ToTable("SecurityUserRoles");
            modelBuilder.Entity<SecurityUserLogin>().ToTable("SecurityUserLogins");            
            modelBuilder.Entity<SecurityUserClaim>().ToTable("SecurityUserClaims");
            modelBuilder.Entity<SecurityUser>().HasOptional(x => x.AppUser).WithRequired(x => x.SecurityUser);
            modelBuilder.Entity<SecurityUserRole>().HasRequired(x => x.SecurityUser).WithMany(x => x.Roles).HasForeignKey(x => x.UserId);
            modelBuilder.Entity<SecurityUserRole>().HasRequired(x => x.SecurityRole).WithMany(X => X.Users).HasForeignKey(x => x.RoleId);
        }
    }   
}
