namespace Naztec.Data.Implementations.UnitOfWorks
{
    using System;
    using System.Data.Entity;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Naztec.Data.Entities;

    public class SecurityRoleRepository : RoleStore<SecurityRole, Guid, SecurityUserRole>, IQueryableRoleStore<SecurityRole, Guid>, IRoleStore<SecurityRole, Guid>, IDisposable
    {
        public SecurityRoleRepository()
            : base(new IdentityDbContext())
        {
            this.DisposeContext = true;
        }

        public SecurityRoleRepository(DbContext context)
            : base(context)
        {
        }
    }
}
