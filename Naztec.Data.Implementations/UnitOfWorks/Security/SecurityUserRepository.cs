namespace Naztec.Data.Implementations.UnitOfWorks
{
    using System;
    using System.Data.Entity;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Naztec.Data.Entities;

    public class SecurityUserRepository : UserStore<SecurityUser, SecurityRole, Guid, SecurityUserLogin, SecurityUserRole, SecurityUserClaim>, IUserStore<SecurityUser, Guid>, IDisposable
    {
        public SecurityUserRepository()
            : this(new IdentityDbContext())
        {
            this.DisposeContext = true;
        }

        public SecurityUserRepository(DbContext context)
            : base(context)
        {
        }
    }
}
