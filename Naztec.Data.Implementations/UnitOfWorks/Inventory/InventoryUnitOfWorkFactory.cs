﻿namespace Naztec.Data.Implementations.UnitOfWorks
{
    using Naztec.Data.Contracts;

    public class InventoryUnitOfWorkFactory : BaseUnitOfWorkFactory<InventoryUnitOfWork>, IInventoryUnitOfWorkFactory
    {
        public InventoryUnitOfWorkFactory(string connectionString)
            : base(connectionString)
        {
        }
    }
}
