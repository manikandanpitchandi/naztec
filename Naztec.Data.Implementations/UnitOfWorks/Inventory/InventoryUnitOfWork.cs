﻿namespace Naztec.Data.Implementations
{
    using Naztec.Data.Contracts;

    public class InventoryUnitOfWork : BaseUnitOfWork<InventoryContext>, IInventoryUnitOfWork
    {
        public InventoryUnitOfWork(string connectionString)
            : base(connectionString)
        {
        }
    }
}