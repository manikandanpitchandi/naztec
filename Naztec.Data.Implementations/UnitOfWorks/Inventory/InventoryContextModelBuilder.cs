﻿namespace Naztec.Data.Implementations
{
    using System.Data.Entity;
    using Data.Entities;

    public partial class InventoryContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Inventory>().Property(e => e.Name).IsUnicode(false);

            modelBuilder.Entity<Inventory>().Property(e => e.PartNumber).IsUnicode(false);

            modelBuilder.Entity<Inventory>().Property(e => e.Description).IsUnicode(false);

            modelBuilder.Entity<Inventory>().HasMany(e => e.InventoryCounts).WithRequired(e => e.Inventory).WillCascadeOnDelete(false);
        }
    }
}
