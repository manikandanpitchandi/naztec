﻿namespace Naztec.Data.Implementations
{
    using System.Data.Entity;
    using Naztec.Data.Entities;

    public partial class InventoryContext : BaseContext<InventoryContext>
    {
        public InventoryContext(string connectionString)
            : base(connectionString)
        {
        }

        public virtual DbSet<Inventory> Inventories { get; set; }
        public virtual DbSet<InventoryCount> InventoryCounts { get; set; }
    }
}
