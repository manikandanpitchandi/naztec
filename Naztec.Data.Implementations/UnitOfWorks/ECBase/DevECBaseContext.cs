namespace Data.Repositories
{
    using System.Data.Entity;
    using Data.Entities;
    
    public class DevECBaseContext : DbContext
    {
        public DevECBaseContext(string connectionString)
            : base(connectionString)
        {
        }
        
        public DbSet<ListCodeDetail> ListCodeDetails { get; set; }
        public DbSet<ListCodeMaster> ListCodeMasters { get; set; }
        public DbSet<SchemaVersion> SchemaVersions { get; set; }
        public DbSet<SecurityRole> SecurityRoles { get; set; }
        public DbSet<SecurityUserClaim> SecurityUserClaims { get; set; }
        public DbSet<SecurityUserLogin> SecurityUserLogins { get; set; }
        public DbSet<SecurityUserRole> SecurityUserRoles { get; set; }
        public DbSet<SecurityUser> SecurityUsers { get; set; }
    }   
}