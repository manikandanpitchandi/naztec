namespace ECBase.Common.Entities.Responses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public enum NaztecResponseType
    {
        Success = 0,
        Warning = 1,
        Error = 2
    }

    public abstract class NaztecResponseBase
    {
        private List<string> successMessages;
        private List<string> warningMessages;
        private List<NaztecErrorMessage> errorMessages;

        protected NaztecResponseBase()
        {
            this.errorMessages = new List<NaztecErrorMessage>();
            this.successMessages = new List<string>();
            this.warningMessages = new List<string>();
        }

        public IReadOnlyList<NaztecErrorMessage> ErrorMessages
        {
            get
            {
                return this.errorMessages;
            }

            private set
            {
            }
        }

        public IReadOnlyList<string> SuccessMessages
        {
            get
            {
                return this.successMessages;
            }
        }

        public IReadOnlyList<string> WarningMessages
        {
            get
            {
                return this.warningMessages;
            }
        }

        public bool IsOk
        {
            get { return this.ErrorMessages.Count == 0; }
            private set { }
        }

        public NaztecResponseType ResponseType
        {
            get
            {
                if (this.IsOk)
                {
                    if (this.successMessages.Any())
                    {
                        return NaztecResponseType.Success;
                    }
                    else if (this.warningMessages.Any())
                    {
                        return NaztecResponseType.Warning;
                    }
                }

                return NaztecResponseType.Error;
            }

            private set
            {
            }
        }

        public string GetHtmlMessage()
        {
            switch (this.ResponseType)
            {
                case NaztecResponseType.Success:
                    return this.GetHtmlMessage(this.successMessages);

                case NaztecResponseType.Warning:
                    return this.GetHtmlMessage(this.warningMessages);

                case NaztecResponseType.Error:

                    return this.GetHtmlMessage(this.errorMessages.Select(x => x.Message));

                default:
                    return string.Empty;
            }
        }

        public string GetTextMessage()
        {
            switch (this.ResponseType)
            {
                case NaztecResponseType.Success:
                    return this.GetTextMessage(this.successMessages);

                case NaztecResponseType.Warning:
                    return this.GetTextMessage(this.warningMessages);

                case NaztecResponseType.Error:
                    return this.GetTextMessage(this.errorMessages.Select(x => x.Message));

                default:
                    return string.Empty;
            }
        }

        public void Error(string message)
        {
            this.errorMessages.Add(new NaztecErrorMessage { Name = string.Empty, Message = message });
        }

        public void Error(string name, string message)
        {
            this.errorMessages.Add(new NaztecErrorMessage { Name = name, Message = message });
        }

        public void Success(string message)
        {
            this.successMessages.Add(message);
        }

        public void Warning(string message)
        {
            this.warningMessages.Add(message);
        }

        public void FromResponse(NaztecResponseBase response)
        {
            switch (response.ResponseType)
            {
                case NaztecResponseType.Success:
                    foreach (var message in response.SuccessMessages)
                    {
                        this.Success(message);
                    }

                    break;
                case NaztecResponseType.Warning:
                    foreach (var message in response.WarningMessages)
                    {
                        this.Warning(message);
                    }

                    break;
                case NaztecResponseType.Error:
                    foreach (var errorMessage in response.ErrorMessages)
                    {
                        this.errorMessages.Add(new NaztecErrorMessage { Name = errorMessage.Name, Message = errorMessage.Message });
                    }

                    break;
            }
        }

        public NaztecResponseBase TryExecute(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                this.Error(ex.Message);
            }

            return this;
        }

        public NaztecResponseBase TryExecute(Action<NaztecResponseBase> action)
        {
            try
            {
                action(this);
            }
            catch (Exception ex)
            {
                this.Error(ex.Message);
            }

            return this;
        }

        private string GetHtmlMessage(IEnumerable<string> messages)
        {
            var htmlMessage = new StringBuilder();

            htmlMessage.Append("<ul>");

            foreach (var item in messages)
            {
                htmlMessage.AppendFormat("<li>{0}</li>", item);
            }

            htmlMessage.Append("</ul>");

            return htmlMessage.ToString();
        }

        private string GetTextMessage(IEnumerable<string> messages)
        {
            var textMessage = new StringBuilder();

            foreach (var item in messages)
            {
                textMessage.AppendFormat("{0}", item).AppendLine();
            }

            return textMessage.ToString();
        }
    }
}
