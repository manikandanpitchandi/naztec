namespace ECBase.Common.Entities.Responses
{
    using System;

    public class NaztecCUDResponse<TResult> : NaztecCUDBaseResponse<Guid, TResult> where TResult : class, new()
    {
    }
}
