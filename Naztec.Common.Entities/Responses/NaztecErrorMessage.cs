namespace ECBase.Common.Entities.Responses
{
    public class NaztecErrorMessage
    {
        public string Name { get; set; }

        public string Message { get; set; }
    }
}
