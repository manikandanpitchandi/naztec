namespace ECBase.Common.Entities.Responses
{
    public class NaztecCUDBaseResponse<TKey, TResult> : NaztecResponseBase where TResult : class, new()
    {
        public NaztecCUDBaseResponse()
        {
            this.Result = new TResult();
        }

        public TKey Key { get; set; }

        public TResult Result { get; set; }
    }
}
