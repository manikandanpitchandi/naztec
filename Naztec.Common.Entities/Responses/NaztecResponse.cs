namespace ECBase.Common.Entities.Responses
{
    public class NaztecResponse : NaztecResponseBase
    {
        public static NaztecResponse CreateError(string message)
        {
            var response = new NaztecResponse();

            response.Error(message);

            return response;
        }

        public static NaztecResponse CreateError(string name, string message)
        {
            var response = new NaztecResponse();

            response.Error(name, message);

            return response;
        }

        public static NaztecResponse CreateSuccess(string message)
        {
            var response = new NaztecResponse();

            response.Success(message);

            return response;
        }

        public static NaztecResponse CreateWarning(string message)
        {
            var response = new NaztecResponse();

            response.Warning(message);

            return response;
        }

        public static T CreateError<T>(string message) where T : NaztecResponseBase, new()
        {
            T response = new T();

            response.Error(message);

            return response;
        }

        public static T CreateError<T>(string name, string message) where T : NaztecResponseBase, new()
        {
            T response = new T();

            response.Error(name, message);

            return response;
        }

        public static T CreateSuccess<T>(string message) where T : NaztecResponseBase, new()
        {
            T response = new T();

            response.Success(message);

            return response;
        }

        public static T CreateWarning<T>(string message) where T : NaztecResponseBase, new()
        {
            T response = new T();

            response.Warning(message);

            return response;
        }
    }
}
