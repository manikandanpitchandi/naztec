namespace Naztec.Data.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InventoryCount")]
    public partial class InventoryCount
    {
        public int Id { get; set; }

        public Guid InventoryId { get; set; }

        public int Count { get; set; }

        public virtual Inventory Inventory { get; set; }
    }
}
