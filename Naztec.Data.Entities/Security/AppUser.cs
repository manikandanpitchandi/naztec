﻿namespace Naztec.Data.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class AppUser
    {
        [ForeignKey("Id")]
        public virtual SecurityUser SecurityUser { get; set; }
    }
}
