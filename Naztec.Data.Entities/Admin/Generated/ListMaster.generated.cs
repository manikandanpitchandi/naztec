//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Naztec.Data.Entities
{
    using System;
    using System.Collections.Generic;

    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;   
    
    
    [Serializable]
    [Table("ListMaster", Schema = "dbo")]
    public partial class ListMaster 
    {        
        public ListMaster()
        {
            this.ListDetails = new List<ListDetail>();
        }


        [Key, Column(Order = 0)]
        public Guid Id { get; set; }
        public bool ShowInListManager { get; set; }
        [MaxLength(256)]
        public string MasterName { get; set; }

        [InverseProperty("ListMaster")]
        public virtual ICollection<ListDetail> ListDetails { get; set; }

 
        public void SetValues(ListMaster item)
        {
            this.Id = item.Id;
            this.MasterName = item.MasterName;
            this.ShowInListManager = item.ShowInListManager;

        }
    }
}