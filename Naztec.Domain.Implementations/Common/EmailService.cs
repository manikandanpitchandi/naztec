﻿namespace Naztec.Domain.Implementations
{
    using System;
    using System.IO;
    using System.Reflection;
    using Naztec.Core.Entities;
    using Naztec.Domain.Contracts;
    using Naztec.Domain.Entities;
    using RazorEngine.Templating;

    public class EmailService : IEmailService
    {
        private const string EmailResourceNameFormat = "Naztec.Domain.Implementations.EmailTemplates.{0}.cshtml";
        private readonly IEmailManager emailManager;
        private IApplicationConfigurationManager configurationManager;

        public EmailService(IEmailManager emailManager, IApplicationConfigurationManager configurationManager)
        {
            this.emailManager = emailManager;
            this.configurationManager = configurationManager;
        }

        public NaztecResponse SendForgotPasswordEmail(ForgetPasswordEmail forgetPasswordEmail)
        {
            return this.emailManager.SendMail(configurationManager.GetValue<string>(NaztecConstants.FromEmail),
                                                forgetPasswordEmail.Email,
                                                "Reset your Password",
                                                this.GetEmailBody<ForgetPasswordEmail>(string.Format(EmailResourceNameFormat, "ForgetPassword"), forgetPasswordEmail), true);
        }

        private string GetEmailBody<T>(string templateName, T model)
        {
            var template = string.Empty;
            var assembly = Assembly.GetExecutingAssembly();

            using (Stream stream = assembly.GetManifestResourceStream(templateName))
            using (StreamReader reader = new StreamReader(stream))
            {
                template = reader.ReadToEnd();
            }

            var templateService = new TemplateService();

            return templateService.Parse(template, model, null, null);
        }

        public NaztecResponse AutomationAppStartEmail()
        {
            var response = new NaztecResponse();
            //this.emailManager.SendMail("ITsupport@ecgroup.in", "campussupport@ecgroup.in", "Campus Automation App Status", "Hi," + System.Environment.NewLine + " Campus Automation App has started at " + DateTime.Now + " by " + System.Environment.MachineName, true);
            return response;
        }

        public NaztecResponse AutomationAppEndEmail()
        {
            var response = new NaztecResponse();
            //this.emailManager.SendMail("ITsupport@ecgroup.in", "campussupport@ecgroup.in", "Campus Automation App Status", "Hi, " + System.Environment.NewLine + " Campus Automation App has stopped at " + DateTime.Now + " by " + System.Environment.MachineName, true);
            return response;
        }
    }
}
