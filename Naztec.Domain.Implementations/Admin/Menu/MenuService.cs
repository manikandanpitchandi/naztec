﻿namespace Naztec.Domain.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using Naztec.Core.Entities;
    using Naztec.Data.Entities;
    using Naztec.Domain.Entities;

    public partial class MenuService
    {
        public List<MenuDTO> GetMenu()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return uow.Repository<AppMenu>().Query(x => x.IsActive == true).Select().ProjectTo<MenuDTO>().ToList();
            }
        }

        public NaztecCUDResponse<MenuDTO> SaveMenu(MenuDTO dto)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var incomingEntity = Mapper.Map<AppMenu>(dto);

                var existingEntity = uow.Repository<AppMenu>().Find(dto.Id);

                if (existingEntity == null)
                {
                    incomingEntity.Id = Guid.NewGuid();
                    incomingEntity.IsActive = true;
                    uow.Repository<AppMenu>().Add(incomingEntity);
                }
                else
                {
                    existingEntity.SetValues(incomingEntity);
                    uow.Repository<AppMenu>().Update(existingEntity);
                }

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<MenuDTO>>("Menu was saved successfully.");

                    dto.Id = incomingEntity.Id;
                    response.Item = dto;

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<MenuDTO>>("Saving Menu failed.");

            }
        }

        public NaztecResponse DeleteMenu(Guid id)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingEntity = uow.Repository<AppMenu>().Find(id);

                if (existingEntity == null)
                {
                    return NaztecResponse.CreateError("Menu does not exist.");
                }
                else
                {
                    uow.Repository<AppMenu>().Delete(existingEntity);
                }

                if (uow.SaveChanges().IsOk)
                {
                    return NaztecResponse.CreateSuccess("Menu was deleted successfully.");
                }

                return NaztecResponse.CreateError("Unable to delete the Menu.");
            }
        }

        public NaztecCUDResponse<MenuDTO> MoveMenu(Guid menuId, bool isMoveUp)
        {
            if (isMoveUp)
            {
                return this.MoveUp(menuId);
            }
            else
            {
                return this.MoveDown(menuId);
            }
        }

        private NaztecCUDResponse<MenuDTO> MoveUp(Guid menuId)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingEntity = uow.Repository<AppMenu>().Find(menuId);

                if (existingEntity.ParentId != null)
                {
                    var swappingEntity = uow.Repository<AppMenu>().Query(x => x.ParentId == existingEntity.ParentId && x.DisplayOrder == existingEntity.DisplayOrder - 1).Select().FirstOrDefault();

                    swappingEntity.DisplayOrder = Convert.ToByte(swappingEntity.DisplayOrder + 1);

                    uow.Repository<AppMenu>().Update(swappingEntity);
                }
                else
                {
                    var swappingEntity = uow.Repository<AppMenu>().Query(x => x.ParentId == null && x.DisplayOrder == existingEntity.DisplayOrder - 1).Select().FirstOrDefault();

                    swappingEntity.DisplayOrder = Convert.ToByte(swappingEntity.DisplayOrder + 1);

                    uow.Repository<AppMenu>().Update(swappingEntity);
                }

                existingEntity.DisplayOrder = Convert.ToByte(existingEntity.DisplayOrder - 1);

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<MenuDTO>>("Menu was moved successfully.");

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<MenuDTO>>("Moving Menu failed.");
            }
        }

        private NaztecCUDResponse<MenuDTO> MoveDown(Guid menuId)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingEntity = uow.Repository<AppMenu>().Find(menuId);

                if (existingEntity.ParentId != null)
                {
                    var swappingEntity = uow.Repository<AppMenu>().Query(x => x.ParentId == existingEntity.ParentId && x.DisplayOrder == existingEntity.DisplayOrder + 1).Select().FirstOrDefault();

                    swappingEntity.DisplayOrder = Convert.ToByte(swappingEntity.DisplayOrder - 1);

                    uow.Repository<AppMenu>().Update(swappingEntity);
                }
                else
                {
                    var swappingEntity = uow.Repository<AppMenu>().Query(x => x.ParentId == null && x.DisplayOrder == existingEntity.DisplayOrder + 1).Select().FirstOrDefault();

                    swappingEntity.DisplayOrder = Convert.ToByte(swappingEntity.DisplayOrder - 1);

                    uow.Repository<AppMenu>().Update(swappingEntity);
                }

                existingEntity.DisplayOrder = Convert.ToByte(existingEntity.DisplayOrder + 1);

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<MenuDTO>>("Menu was moved successfully.");

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<MenuDTO>>("Moving Menu failed.");
            }
        }
    }
}
