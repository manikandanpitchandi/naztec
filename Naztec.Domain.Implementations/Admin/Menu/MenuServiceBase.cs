﻿namespace Naztec.Domain.Implementations
{
    using System;
    using Core.Entities;
    using Naztec.Data.Contracts;
    using Naztec.Domain.Contracts;
    using Entities;

    public partial class MenuService : IMenuService
    {
        private IAdminUnitOfWorkFactory uowFactory;

        public MenuService(IAdminUnitOfWorkFactory uowFactory)
        {
            this.uowFactory = uowFactory;
        }
    }
}
