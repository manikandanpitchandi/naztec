﻿namespace Naztec.Domain.Implementations
{
    using Contracts;
    using Naztec.Data.Contracts;

    public partial class LogService : ILogService
    {
        private IAdminUnitOfWorkFactory uowFactory;

        public LogService(IAdminUnitOfWorkFactory uowFactory)
        {
            this.uowFactory = uowFactory;
        }
    }
}
