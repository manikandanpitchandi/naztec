﻿namespace Naztec.Domain.Implementations
{
    using AutoMapper.QueryableExtensions;
    using Naztec.Data.Entities;
    using Naztec.Domain.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public partial class LogService
    {
        public List<LogDTO> GetLog()
        {
            DateTime toDate = DateTime.Now;
            DateTime fromDate = System.DateTime.Today.AddDays(-7);

            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return uow.Repository<Log>()
                               .Query(x => x.Date <= toDate && x.Date >= fromDate)
                               .Select()
                               .ProjectTo<LogDTO>()
                               .ToList();
            }
        }
    }
}