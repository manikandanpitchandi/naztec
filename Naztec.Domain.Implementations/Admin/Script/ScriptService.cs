﻿using Naztec.Data.Contracts;
using Naztec.Data.Entities;
using Naztec.Domain.Contracts;
using Naztec.Domain.Entities;
using System;
using System.Text;
using System.Linq;
using AutoMapper.QueryableExtensions;
using System.Collections.Generic;
using Naztec.Core.Utilities;
using Naztec.Domain.Entities.Admin.Security;

namespace Naztec.Domain.Implementations
{
    public class ScriptService : IScriptService
    {
        private StringBuilder scripts;
        private IAdminUnitOfWorkFactory uowFactory;

        public ScriptService(IAdminUnitOfWorkFactory uowFactory)
        {
            this.uowFactory = uowFactory;
            this.scripts = new StringBuilder();
        }

        public string GenerateSQL(int moduleId)
        {
            if (moduleId == 1)
            {
                this.scripts.AppendLine($"-- /****************  LIST MANAGER **************/{Environment.NewLine}");

                AddListMasterScripts();
                AddListDetailScripts();
            }
            else if (moduleId == 2)
            {
                this.scripts.AppendLine($"-- /****************  MENU MANAGER **************/{Environment.NewLine}");

                AddMenuScripts();
            }
            else if (moduleId == 4)
            {
                this.scripts.AppendLine($"-- /****************  RIGHTS MANAGER **************/{Environment.NewLine}");

                AddRightsScripts();
            }
            else if (moduleId == 5)
            {
                this.scripts.AppendLine($"-- /****************  ROLE MANAGER **************/{Environment.NewLine}");

                AddRoleScripts();

                AddSecurityRoleMenuScripts();

                AddSecurityRoleRightsScripts();

                AddSecurityUserRolesScripts();
            }

            return this.scripts.ToString();
        }

        public string GenerateCSharp(int moduleId)
        {
            if (moduleId == 4)
            {
                AddRightsCSharpEnumeration();
            }

            return this.scripts.ToString();
        }

        public string GenerateJavascript(int moduleId)
        {
            if (moduleId == 4)
            {
                AddRightsJSEnumeration();
            }

            return this.scripts.ToString();
        }

        #region List Manager

        private void AddListMasterScripts()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                string tableName = "ListMaster";

                this.scripts.AppendLine($"-- {tableName} Insert Scripts {Environment.NewLine}{Environment.NewLine}");

                var masters = uow.Repository<ListMaster>()
                                  .Query(x => x.ShowInListManager == true)
                                  .Select()
                                  .ProjectTo<ListMasterDTO>()
                                  .ToList();
                foreach (ListMasterDTO master in masters)
                {
                    GenerateListMasterInserts(master, tableName);
                }
            }
        }

        private void GenerateListMasterInserts(ListMasterDTO master, string tableName)
        {
            this.scripts.AppendLine($"IF NOT EXISTS(SELECT * FROM [dbo].[{tableName}] WHERE Id = '{master.Id}')");
            this.scripts.AppendLine("   BEGIN");
            this.scripts.AppendLine($"      INSERT INTO [dbo].[{tableName}] ([Id], [MasterName], [ShowInListManager])");
            this.scripts.AppendLine($"      VALUES(N'{master.Id}', N'{master.MasterName.CleanApostrophe()}', 1)");
            this.scripts.AppendLine("   END");
            this.scripts.AppendLine($"{Environment.NewLine}");
        }

        private void AddListDetailScripts()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                string tableName = "ListDetail";

                this.scripts.AppendLine($"-- {tableName} Insert Scripts {Environment.NewLine}{Environment.NewLine}");

                var details = uow.Repository<ListDetail>()
                              .Query()
                              .Select()
                              .ProjectTo<ListDetailDTO>()
                              .ToList();
                foreach (ListDetailDTO detail in details)
                {
                    GenerateListDetailInserts(detail, tableName);
                }
            }
        }

        private void GenerateListDetailInserts(ListDetailDTO detail, string tableName)
        {
            this.scripts.AppendLine($"IF NOT EXISTS(SELECT * FROM [dbo].[{tableName}] WHERE Id = '{detail.Id}')");
            this.scripts.AppendLine("   BEGIN");
            this.scripts.AppendLine($"      INSERT INTO [dbo].[{tableName}] ([Id], [MasterId], [Code], [Description])");
            this.scripts.AppendLine($"      VALUES(N'{detail.Id}', N'{detail.MasterId}', N'{detail.Code.CleanApostrophe()}', N'{detail.Description.CleanApostrophe()}')");
            this.scripts.AppendLine("   END");
            this.scripts.AppendLine($"{Environment.NewLine}");
        }

        #endregion

        #region Menu Manager

        private void AddMenuScripts()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                string tableName = "AppMenus";

                this.scripts.AppendLine($"-- {tableName} Insert Scripts {Environment.NewLine}{Environment.NewLine}");

                var menus = uow.Repository<AppMenu>()
                                .Query(x => x.IsActive == true)
                                .Select()
                                .ProjectTo<MenuDTO>().ToList();
                
                    GenerateAppMenuInserts(menus, tableName,null);
            }
        }

        private void GenerateAppMenuInserts(List<MenuDTO> menus, string tableName,Guid? parentId)
        {
            foreach (var item in menus.Where(x => x.ParentId == parentId).OrderBy(x => x.Text).ToList())
            {
                this.scripts.AppendLine($"IF NOT EXISTS(SELECT * FROM [dbo].[{tableName}] WHERE Id = '{item.Id}')");
            this.scripts.AppendLine("   BEGIN");
            this.scripts.AppendLine($"      INSERT INTO [dbo].[{tableName}] ([Id], [ParentId], [Text], [Slug], [CssClass], [DisplayOrder], [IsActive])");
            this.scripts.AppendLine($"      VALUES (N'{item.Id}', {item.ParentId.GetNULLOrGuid()}, N'{item.Text.CleanApostrophe()}',{item.Slug.CleanApostrophe().GetNULLOrString()}, N'{item.CssClass.CleanApostrophe()}', {item.DisplayOrder}, {item.IsActive.ToString().GetBoolValue()})");
            this.scripts.AppendLine("   END");
            this.scripts.AppendLine($"{Environment.NewLine}");
                var hasChildren = menus.Any(x => x.ParentId == item.Id);
                if (hasChildren)
                {
                    GenerateAppMenuInserts(menus, tableName, item.Id);
                }
            }
        }

        #endregion

        #region Rights Manager

        private void AddRightsScripts()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                string tableName = "SecurityRights";

                this.scripts.AppendLine($"-- {tableName} Insert Scripts {Environment.NewLine}{Environment.NewLine}");

                var rights = uow.Repository<SecurityRight>()
                                .Query()
                                .Select()
                                .ProjectTo<SecurityRightDTO>().ToList();               

                GenerateSecurityRightInserts(rights, tableName, null);                
            }
        }

        private void GenerateSecurityRightInserts(List<SecurityRightDTO> rights, string tableName, Guid? parentId)
        {
            foreach (var item in rights.Where(x => x.ParentRightId == parentId).OrderBy(x => x.SecurityRightDefinition).ToList())
            {
                this.scripts.AppendLine($"IF NOT EXISTS(SELECT * FROM [dbo].[{tableName}] WHERE Id = '{item.Id}')");
                this.scripts.AppendLine("   BEGIN");
                this.scripts.AppendLine($"      INSERT INTO [dbo].[{tableName}] ([Id], [SecurityRightDefinition], [IsHidden], [ApplicationSecurity], [DisplayOrder], [ParentRightId])");
                this.scripts.AppendLine($"      VALUES (N'{item.Id}', N'{item.SecurityRightDefinition.CleanApostrophe()}', {item.IsHidden.ToString().GetBoolValue()}, {item.ApplicationSecurity.ToString().GetBoolValue()}, {item.DisplayOrder}, {parentId.GetNULLOrGuid()})");
                this.scripts.AppendLine("   END");
                this.scripts.AppendLine($"{Environment.NewLine}");

                var hasChildren = rights.Any(x => x.ParentRightId == item.Id);
                if (hasChildren)
                {
                    GenerateSecurityRightInserts(rights, tableName, item.Id);
                }
            }   
        }

        private void AddRightsCSharpEnumeration()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                string namespaceString = "Naztec.Domain.Entities";

                var rights = uow.Repository<SecurityRight>().Query().Select(x => x).ToList();

                Func<int, string> space = (length) =>
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < length; i++)
                    {
                        sb.Append(" ");
                    }

                    return sb.ToString();
                };

                var elementClassBuilder = new StringBuilder($"namespace {namespaceString}")
                                                          .AppendLine()
                                                          .AppendLine("{")
                                                          .AppendLine($"{space(4)}using System;")
                                                          .AppendLine()
                                                          .AppendLine($"{space(4)}public class SecurityRights")
                                                          .AppendLine($"{space(4)}{{");

                ProduceClass(true, elementClassBuilder, null, rights, null, 4);

                elementClassBuilder.AppendLine($"{space(4)}}}").AppendLine("}");

                this.scripts.Append(elementClassBuilder.ToString());

            }
        }

        private void AddRightsJSEnumeration()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var rights = uow.Repository<SecurityRight>().Query().Select(x => x).ToList();

                Func<int, string> space = (length) =>
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < length; i++)
                    {
                        sb.Append(" ");
                    }

                    return sb.ToString();
                };

                var elementJSONBuilder = new StringBuilder("naztec.securityRights = naztec.securityRights || {};")
                                                            .AppendLine();

                elementJSONBuilder.Append($"naztec.securityRights = {{").AppendLine();

                ProduceClass(false, null, elementJSONBuilder, rights, null, 4);

                elementJSONBuilder.AppendLine("};").AppendLine();

                this.scripts.Append(elementJSONBuilder.ToString());
            }
        }

        private void ProduceClass(bool generateCSharp, StringBuilder elementClassBuilder, StringBuilder elementJSONBuilder, List<SecurityRight> rights, Guid? parentId, int indentationDepth)
        {
            var propertyTemplate = @"public const string {0} = ""{1}"";";

            Func<int, string> space = (length) =>
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < length; i++)
                {
                    sb.Append(" ");
                }

                return sb.ToString();
            };

            Func<string, string, string, string> replace = (source, searchFor, searchFor2) =>
            {
                return source.Replace(searchFor, string.Empty).Replace(searchFor2, string.Empty);
            };

            foreach (var item in rights.Where(x => x.ParentRightId == parentId).OrderBy(x => x.SecurityRightDefinition).ToList())
            {
                var hasChildren = rights.Any(x => x.ParentRightId == item.Id);
                var classBuilder = new StringBuilder();
                var jsonBuilder = new StringBuilder();

                if (hasChildren)
                {
                    classBuilder.AppendLine($"{space(indentationDepth + 4)}public class {replace(item.SecurityRightDefinition, " ", "'")}").AppendLine($"{space(indentationDepth + 4)}{{");
                    jsonBuilder.Append(space(indentationDepth + 4)).Append($"{replace(item.SecurityRightDefinition, " ", "'")} : {{").AppendLine();

                    classBuilder.Append(space(indentationDepth + 8)).AppendFormat(propertyTemplate, replace(item.SecurityRightDefinition, " ", "'") + "Right", item.Id).AppendLine();
                    jsonBuilder.Append(space(indentationDepth + 8)).AppendFormat($"{replace(item.SecurityRightDefinition, " ", "'")}Right : '{item.Id}',").AppendLine();

                    ProduceClass(generateCSharp, classBuilder, jsonBuilder, rights, item.Id, indentationDepth + 4);

                    classBuilder.AppendLine($"{space(indentationDepth + 4)}}}");
                    jsonBuilder.Append(space(indentationDepth + 4)).Append("},").AppendLine();
                }
                else
                {
                    classBuilder.Append(space(indentationDepth + 4)).AppendFormat(propertyTemplate, replace(item.SecurityRightDefinition, " ", "'"), item.Id).AppendLine();
                    jsonBuilder.Append(space(indentationDepth + 4)).AppendFormat($"{replace(item.SecurityRightDefinition, " ", "'")} : '{item.Id}',").AppendLine();
                }

                if (generateCSharp)
                {
                    elementClassBuilder.Append(classBuilder.ToString());
                }
                else
                {
                    elementJSONBuilder.Append(jsonBuilder.ToString());
                }
            }
        }

        #endregion

        #region Role Manager

        private void AddRoleScripts()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                string tableName = "SecurityRoles";

                this.scripts.AppendLine($"-- {tableName} Insert Scripts {Environment.NewLine}{Environment.NewLine}");

                var roles = uow.Repository<SecurityRole>()
                                .Query()
                                .Select()
                                .ProjectTo<SecurityRoleDTO>().ToList();
                foreach (SecurityRoleDTO role in roles)
                {
                    GenerateSecurityRoleInserts(role, tableName);
                }
            }

        }

        private void GenerateSecurityRoleInserts(SecurityRoleDTO role, string tableName)
        {
            this.scripts.AppendLine($"IF NOT EXISTS(SELECT * FROM [dbo].[{tableName}] WHERE Id = '{role.Id}')");
            this.scripts.AppendLine("   BEGIN");
            this.scripts.AppendLine($"      INSERT INTO [dbo].[{tableName}] ([Id], [Name])");
            this.scripts.AppendLine($"      VALUES (N'{role.Id}', N'{role.Name}')");
            this.scripts.AppendLine("   END");
            this.scripts.AppendLine($"{Environment.NewLine}");
        }

        private void AddSecurityRoleMenuScripts()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                string tableName = "SecurityRoleMenu";

                this.scripts.AppendLine($"-- {tableName} Insert Scripts {Environment.NewLine}{Environment.NewLine}");

                var roleMenus = uow.Repository<SecurityRoleMenu>()
                                .Query()
                                .Select()
                                .ToList();
                foreach (SecurityRoleMenu roleMenu in roleMenus)
                {
                    GenerateSecurityRoleMenuInserts(roleMenu, tableName);
                }
            }

        }

        private void GenerateSecurityRoleMenuInserts(SecurityRoleMenu roleMenu, string tableName)
        {
            this.scripts.AppendLine($"IF NOT EXISTS(SELECT * FROM [dbo].[{tableName}] WHERE Id = '{roleMenu.Id}')");
            this.scripts.AppendLine("   BEGIN");
            this.scripts.AppendLine($"      INSERT INTO [dbo].[{tableName}] ([Id], [RoleId],[MenuId])");
            this.scripts.AppendLine($"      VALUES (N'{roleMenu.Id}', N'{roleMenu.RoleId}', N'{roleMenu.MenuId}')");
            this.scripts.AppendLine("   END");
            this.scripts.AppendLine($"{Environment.NewLine}");
        }

        private void AddSecurityRoleRightsScripts()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                string tableName = "SecurityRoleRights";

                this.scripts.AppendLine($"-- {tableName} Insert Scripts {Environment.NewLine}{Environment.NewLine}");

                var roleRights = uow.Repository<SecurityRoleRight>()
                                .Query()
                                .Select()
                                .ToList();
                foreach (SecurityRoleRight roleRight in roleRights)
                {
                    GenerateSecurityRoleRightsInserts(roleRight, tableName);
                }
            }

        }

        private void GenerateSecurityRoleRightsInserts(SecurityRoleRight roleRight, string tableName)
        {
            this.scripts.AppendLine($"IF NOT EXISTS(SELECT * FROM [dbo].[{tableName}] WHERE Id = '{roleRight.Id}')");
            this.scripts.AppendLine("   BEGIN");
            this.scripts.AppendLine($"      INSERT INTO [dbo].[{tableName}] ([Id], [SecurityRoleId],[SecurityRightId])");
            this.scripts.AppendLine($"      VALUES (N'{roleRight.Id}', N'{roleRight.SecurityRoleId}', N'{roleRight.SecurityRightId}')");
            this.scripts.AppendLine("   END");
            this.scripts.AppendLine($"{Environment.NewLine}");
        }


        private void AddSecurityUserRolesScripts()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                string tableName = "SecurityUserRoles";

                this.scripts.AppendLine($"-- {tableName} Insert Scripts {Environment.NewLine}{Environment.NewLine}");

                var userRoles = uow.Repository<SecurityUserRole>()
                                .Query()
                                .Select()
                                .ToList();
                foreach (SecurityUserRole userRole in userRoles)
                {
                    GenerateSecurityUserRolesInserts(userRole, tableName);
                }
            }

        }

        private void GenerateSecurityUserRolesInserts(SecurityUserRole userRole, string tableName)
        {
            this.scripts.AppendLine($"IF NOT EXISTS(SELECT * FROM [dbo].[{tableName}] WHERE UserId = '{userRole.UserId}' AND RoleId = '{userRole.RoleId}')");
            this.scripts.AppendLine("   BEGIN");
            this.scripts.AppendLine($"      INSERT INTO [dbo].[{tableName}] ([UserId], [RoleId],[ExpiresOn])");
            this.scripts.AppendLine($"      VALUES (N'{userRole.UserId}', N'{userRole.RoleId}', {userRole.ExpiresOn.GetNULLOrDate()})");
            this.scripts.AppendLine("   END");
            this.scripts.AppendLine($"{Environment.NewLine}");
        }

        #endregion        
    }
}
