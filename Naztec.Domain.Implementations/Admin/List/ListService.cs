﻿namespace Naztec.Domain.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using Core.Entities;
    using Data.Contracts;
    using Naztec.Data.Entities;
    using Naztec.Domain.Entities;

    public partial class ListService
    {
        public List<ListMasterDTO> GetListMasters()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return this.GetListMastersInternal(uow).ToList();
            }
        }

        public List<ListDetailDTO> GetListDetailsByMasterId(Guid masterId)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return this.GetListDetailsInternal(uow, masterId).ToList();
            }
        }

        public NaztecCUDResponse<ListMasterDTO> SaveListMaster(ListMasterDTO dto)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var incomingEntity = Mapper.Map<ListMaster>(dto);

                var existingEntity = uow.Repository<ListMaster>().Find(incomingEntity.Id);

                var masterName = uow.Repository<ListMaster>().Query(x => x.MasterName == dto.MasterName).Select().FirstOrDefault();

                if (existingEntity == null)
                {
                    incomingEntity.Id = Guid.NewGuid();
                    incomingEntity.ShowInListManager = true;

                    if (masterName == null)
                    {
                        uow.Repository<ListMaster>().Add(incomingEntity);
                    }
                    else
                    {
                        return NaztecResponse.CreateError<NaztecCUDResponse<ListMasterDTO>>("List Master name already exists.");
                    }
                }
                else
                {
                    incomingEntity.ShowInListManager = true;
                    if (masterName == null)
                    {
                        existingEntity.SetValues(incomingEntity);
                    }
                    else
                    {
                        return NaztecResponse.CreateError<NaztecCUDResponse<ListMasterDTO>>("List Master name already exists.");
                    }
                }

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<ListMasterDTO>>("List Master saved successfully.");

                    dto.Id = incomingEntity.Id;
                    response.Item = dto;

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<ListMasterDTO>>("Saving List Master failed.");
            }
        }

        public NaztecCUDResponse<ListDetailDTO> SaveListDetail(ListDetailDTO dto)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var codePresentAlready = uow.Repository<ListDetail>()
                                            .Query(x => x.Id != dto.Id && string.Compare(x.MasterId.ToString(), dto.MasterId.ToString(), true) == 0 && string.Compare(x.Code, dto.Code, true) == 0)
                                            .Select()
                                            .Any();
                if (codePresentAlready)
                {
                    return NaztecResponse.CreateError<NaztecCUDResponse<ListDetailDTO>>($"Code - '{ dto.Code }' is already present in the system.");
                }

                var incomingEntity = Mapper.Map<ListDetail>(dto);

                var existingEntity = uow.Repository<ListDetail>().Find(incomingEntity.Id);

                if (existingEntity == null)
                {
                    incomingEntity.Id = Guid.NewGuid();
                    uow.Repository<ListDetail>().Add(incomingEntity);
                }
                else
                {
                    existingEntity.SetValues(incomingEntity);
                }

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<ListDetailDTO>>("List Detail was saved successfully.");

                    dto.Id = incomingEntity.Id;
                    response.Item = dto;

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<ListDetailDTO>>("Saving List Detail failed.");
            }
        }

        public NaztecCUDResponse<ListMasterDTO> DeleteListMasterById(Guid id)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingItem = uow.Repository<ListMaster>().Find(id);

                if (existingItem == null)
                {
                    return NaztecResponse.CreateError<NaztecCUDResponse<ListMasterDTO>>("List Master Record does not exist.");
                }

                uow.Repository<ListMaster>().Delete(existingItem);

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<ListMasterDTO>>("List Master was deleted successfully.");

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<ListMasterDTO>>("Deleting List Master failed.");
            }
        }

        public NaztecCUDResponse<ListDetailDTO> DeleteListDetailById(Guid id)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingItem = uow.Repository<ListDetail>().Find(id);

                if (existingItem == null)
                {
                    return NaztecResponse.CreateError<NaztecCUDResponse<ListDetailDTO>>("List Detail does not exist.");
                }

                uow.Repository<ListDetail>().Delete(existingItem);

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<ListDetailDTO>>("List Detail deleted successfully.");

                    response.Id = id;

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<ListDetailDTO>>("Deleting List Detail failed.");
            }
        }

        private ICollection<ListMasterDTO> GetListMastersInternal(IUnitOfWork uow)
        {
            return uow.Repository<ListMaster>()
                               .Query(x => x.ShowInListManager == true)
                               .Select()
                               .ProjectTo<ListMasterDTO>()
                               .ToList();
        }

        private ICollection<ListDetailDTO> GetListDetailsInternal(IUnitOfWork uow, Guid masterId)
        {
            return uow.Repository<ListDetail>()
                      .Query(x => (masterId != Guid.Empty) ? x.MasterId == masterId : x.MasterId == x.MasterId)
                      .OrderBy(x => x.OrderBy(y => y.Code))
                      .Select()
                      .ProjectTo<ListDetailDTO>()
                      .ToList();
        }

    }
}
