﻿namespace Naztec.Domain.Implementations
{
    using Naztec.Data.Contracts;
    using Naztec.Domain.Contracts;

    public partial class ListService : IListService
    {
        private IAdminUnitOfWorkFactory uowFactory;

        public ListService(IAdminUnitOfWorkFactory uowFactory)
        {
            this.uowFactory = uowFactory;
        }
    }
}
