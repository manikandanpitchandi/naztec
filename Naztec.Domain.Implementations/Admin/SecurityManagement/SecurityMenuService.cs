﻿namespace Naztec.Domain.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Naztec.Data.Entities;
    using Naztec.Domain.Entities;

    public partial class SecurityManagementService
    {
        public ICollection<AppMenuDTO> GetMenu(List<Guid> roleIds)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var appmenuIds = uow.Repository<SecurityRoleMenu>().Query(x => roleIds.Contains(x.RoleId)).Select(x => x.MenuId).ToList();

                return uow.Repository<AppMenu>().Query(x => appmenuIds.Contains(x.Id))
                                                .Select(x => new AppMenuDTO
                                                {
                                                    Id = x.Id,
                                                    Text = x.Text,
                                                    Description = x.Text,
                                                    ParentMenuId = x.ParentId,
                                                    MenuOrder = x.DisplayOrder,
                                                    IsActive = x.IsActive,
                                                    Slug = x.Slug,
                                                    Class = x.CssClass
                                                }).ToList();
            }
        }
    }
}
