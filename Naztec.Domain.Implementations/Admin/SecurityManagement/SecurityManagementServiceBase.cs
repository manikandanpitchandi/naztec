﻿namespace Naztec.Domain.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Naztec.Data.Contracts;
    using Naztec.Data.Entities;
    using Naztec.Domain.Contracts;
    using Security;
    using log4net;

    public partial class SecurityManagementService : ISecurityManagementService
    {
        private static readonly ILog Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IAdminUnitOfWorkFactory uowFactory;
        private SecurityUserServices userService;
        private SecurityRoleServices roleService;
        private IApplicationConfigurationManager configurationManager;
        private IEmailManager emailService;

        public SecurityManagementService(IAdminUnitOfWorkFactory uowFactory, 
            SecurityUserServices userService, 
            SecurityRoleServices roleService,
            IApplicationConfigurationManager configurationManager,
            IEmailManager emailService)
        {
            this.uowFactory = uowFactory;
            this.userService = userService;
            this.roleService = roleService;
            this.configurationManager = configurationManager;
            this.emailService = emailService;
        }

        public void GenerateSecurityRightEnumerations(string namespaceString, string cspath, string jspath)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var rights = uow.Repository<SecurityRight>().Query().Select(x => x).ToList();

                Func<int, string> space = (length) =>
                {
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < length; i++) 
                    {
                        sb.Append(" ");
                    }

                    return sb.ToString();
                };

                var elementClassBuilder = new StringBuilder($"namespace {namespaceString}")
                                                          .AppendLine()
                                                          .AppendLine("{")
                                                          .AppendLine($"{space(4)}using System;")
                                                          .AppendLine()
                                                          .AppendLine($"{space(4)}public class SecurityRights")
                                                          .AppendLine($"{space(4)}{{");

                var elementJSONBuilder = new StringBuilder("eCafe.securityRights = eCafe.securityRights || {};")
                                                            .AppendLine();

                elementJSONBuilder.Append($"eCafe.securityRights = {{").AppendLine();

                ProduceClass(elementClassBuilder, elementJSONBuilder, rights, null, 4);

                elementClassBuilder.AppendLine($"{space(4)}}}").AppendLine("}");
                elementJSONBuilder.AppendLine("};").AppendLine();

                File.WriteAllText(cspath, elementClassBuilder.ToString());
                File.WriteAllText(jspath, elementJSONBuilder.ToString());
            }
        }

        private void ProduceClass(StringBuilder elementClassBuilder, StringBuilder elementJSONBuilder, List<SecurityRight> rights, Guid? parentId, int indentationDepth)
        {
            var propertyTemplate = @"public const string {0} = ""{1}"";";

            Func<int, string> space = (length) =>
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < length; i++)
                {
                    sb.Append(" ");
                }

                return sb.ToString();
            };

            Func<string, string, string, string> replace = (source, searchFor, searchFor2) =>
            {
                return source.Replace(searchFor, string.Empty).Replace(searchFor2, string.Empty);
            };

            foreach (var item in rights.Where(x => x.ParentRightId == parentId).OrderBy(x => x.SecurityRightDefinition).ToList())
            {
                var hasChildren = rights.Any(x => x.ParentRightId == item.Id);
                var classBuilder = new StringBuilder();
                var jsonBuilder = new StringBuilder();

                if (hasChildren)
                {
                    classBuilder.AppendLine($"{space(indentationDepth + 4)}public class {replace(item.SecurityRightDefinition, " ", "'")}").AppendLine($"{space(indentationDepth + 4)}{{");
                    jsonBuilder.Append(space(indentationDepth + 4)).Append($"{replace(item.SecurityRightDefinition, " ", "'")} : {{").AppendLine();

                    classBuilder.Append(space(indentationDepth + 8)).AppendFormat(propertyTemplate, replace(item.SecurityRightDefinition, " ", "'") + "Right", item.Id).AppendLine();
                    jsonBuilder.Append(space(indentationDepth + 8)).AppendFormat($"{replace(item.SecurityRightDefinition, " ", "'")}Right : '{item.Id}',").AppendLine();

                    ProduceClass(classBuilder, jsonBuilder, rights, item.Id, indentationDepth + 4);

                    classBuilder.AppendLine($"{space(indentationDepth + 4)}}}");
                    jsonBuilder.Append(space(indentationDepth + 4)).Append("},").AppendLine();
                }
                else
                {
                    classBuilder.Append(space(indentationDepth + 4)).AppendFormat(propertyTemplate, replace(item.SecurityRightDefinition, " ", "'"), item.Id).AppendLine();
                    jsonBuilder.Append(space(indentationDepth + 4)).AppendFormat($"{replace(item.SecurityRightDefinition, " ", "'")} : '{item.Id}',").AppendLine();
                }

                elementClassBuilder.Append(classBuilder.ToString());
                elementJSONBuilder.Append(jsonBuilder.ToString());
            }
        }
    }
}
