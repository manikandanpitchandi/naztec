﻿namespace Naztec.Domain.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using Core.Entities;
    using Data.Contracts;
    using Data.Entities;
    using Naztec.Domain.Contracts;
    using Naztec.Domain.Entities;

    public partial class SecurityManagementService
    {
        public List<SecurityRightDTO> GetSecurityRights()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return uow.Repository<SecurityRight>().Query().Select().ProjectTo<SecurityRightDTO>().ToList();
            }
        }

        public NaztecCUDResponse<SecurityRightDTO> SaveSecurityRights(SecurityRightDTO dto)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var incomingEntity = Mapper.Map<SecurityRight>(dto);

                var existingEntity = uow.Repository<SecurityRight>().Find(dto.Id);

                if (existingEntity == null)
                {
                    incomingEntity.Id = Guid.NewGuid();
                    uow.Repository<SecurityRight>().Add(incomingEntity);
                }
                else
                {
                    existingEntity.SetValues(incomingEntity);
                    uow.Repository<SecurityRight>().Update(existingEntity);
                }

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<SecurityRightDTO>>("SecurityRight was saved successfully.");

                    dto.Id = incomingEntity.Id;
                    response.Item = dto;

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<SecurityRightDTO>>("Saving SecurityRight failed.");
            }
        }

        public NaztecResponse DeleteRight(Guid id)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingEntity = uow.Repository<SecurityRight>().Find(id);

                if (existingEntity == null)
                {
                    return NaztecResponse.CreateError("Right does not exist.");
                }
                else
                {
                    uow.Repository<SecurityRight>().Delete(existingEntity);
                }

                if (uow.SaveChanges().IsOk)
                {
                    return NaztecResponse.CreateSuccess("Right was deleted successfully.");
                }

                return NaztecResponse.CreateError("Unable to delete the Right.");
            }
        }

        public NaztecCUDResponse<SecurityRightDTO> MoveRight(Guid rightId, bool isMoveUp)
        {
            if (isMoveUp)
            {
                return this.MoveUp(rightId);
            }
            else
            {
                return this.MoveDown(rightId);
            }
        }

        public ICollection<SecurityRightDisplayDTO> GetSecurityRights(Guid roleId)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return this.GetSecurityRightsInternal(uow, roleId);
            }
        }

        private NaztecCUDResponse<SecurityRightDTO> MoveUp(Guid rightId)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingEntity = uow.Repository<SecurityRight>().Find(rightId);

                if (existingEntity.ParentRightId != null)
                {
                    var swappingEntity = uow.Repository<SecurityRight>().Query(x => x.ParentRightId == existingEntity.ParentRightId && x.DisplayOrder == existingEntity.DisplayOrder - 1).Select().FirstOrDefault();

                    swappingEntity.DisplayOrder = Convert.ToByte(swappingEntity.DisplayOrder + 1);

                    uow.Repository<SecurityRight>().Update(swappingEntity);
                }
                else
                {
                    var swappingEntity = uow.Repository<SecurityRight>().Query(x => x.ParentRightId == null && x.DisplayOrder == existingEntity.DisplayOrder - 1).Select().FirstOrDefault();

                    swappingEntity.DisplayOrder = Convert.ToByte(swappingEntity.DisplayOrder + 1);

                    uow.Repository<SecurityRight>().Update(swappingEntity);
                }

                existingEntity.DisplayOrder = Convert.ToByte(existingEntity.DisplayOrder - 1);

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<SecurityRightDTO>>("Menu was moved successfully.");

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<SecurityRightDTO>>("Moving Menu failed.");
            }
        }

        private NaztecCUDResponse<SecurityRightDTO> MoveDown(Guid rightId)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingEntity = uow.Repository<SecurityRight>().Find(rightId);

                if (existingEntity.ParentRightId != null)
                {
                    var swappingEntity = uow.Repository<SecurityRight>().Query(x => x.ParentRightId == existingEntity.ParentRightId && x.DisplayOrder == existingEntity.DisplayOrder + 1).Select().FirstOrDefault();

                    swappingEntity.DisplayOrder = Convert.ToByte(swappingEntity.DisplayOrder - 1);

                    uow.Repository<SecurityRight>().Update(swappingEntity);
                }
                else
                {
                    var swappingEntity = uow.Repository<SecurityRight>().Query(x => x.ParentRightId == null && x.DisplayOrder == existingEntity.DisplayOrder + 1).Select().FirstOrDefault();

                    swappingEntity.DisplayOrder = Convert.ToByte(swappingEntity.DisplayOrder - 1);

                    uow.Repository<SecurityRight>().Update(swappingEntity);
                }

                existingEntity.DisplayOrder = Convert.ToByte(existingEntity.DisplayOrder + 1);

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<SecurityRightDTO>>("Menu was moved successfully.");

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<SecurityRightDTO>>("Moving Menu failed.");
            }
        }

        private ICollection<SecurityRightDisplayDTO> GetSecurityRightsInternal(IUnitOfWork uow, Guid roleId)
        {
            var rights = uow.Repository<SecurityRight>().Query().Select(x => new SecurityRightDisplayDTO()
            {
                Description = x.SecurityRightDefinition,
                Id = x.Id,
                ParentId = x.ParentRightId,
                DisplayOrder = x.DisplayOrder,
                State = new StateDTO() { Selected = x.SecurityRoleRights.Any(z => z.SecurityRoleId == roleId) && !x.ChildRights.Any() }
            }).OrderBy(x => x.DisplayOrder).ToList();

            return rights;
        }
    }
}
