﻿namespace Naztec.Domain.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using Naztec.Core.Entities;
    using Naztec.Data.Contracts;
    using Naztec.Data.Entities;
    using Naztec.Domain.Entities;
    using Naztec.Domain.Entities.Admin.Security;

    public partial class SecurityManagementService
    { 
        public NaztecCUDResponse<SecurityRoleDTO> DeleteSecurityRole(Guid id)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingItem = uow.Repository<SecurityRole>().Find(id);

                if (existingItem == null)
                {
                    return NaztecResponse.CreateError<NaztecCUDResponse<SecurityRoleDTO>>("Security Role does not exist.");
                }

                //if (uow.Repository<SecurityGroupUser>().Query(x => x.SecurityGroupId == id).Select().Any())
                //{
                //    return ECResponse.CreateError<ECCUDResponse<SecurityGroupDTO>>("Operation Failed!! There are User(s) assigned to this Group.");
                //}

                uow.Repository<SecurityRole>().Delete(existingItem);

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<SecurityRoleDTO>>("Security Role deleted successfully.");

                    response.Id = id;

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<SecurityRoleDTO>>("Deleting Security Role failed.");
            }
        }

        public NaztecCUDResponse<SecurityRoleDTO> SaveSecurityRole(SecurityRoleDTO dto)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var incomingEntity = Mapper.Map<SecurityRole>(dto);

                var existingEntity = uow.Repository<SecurityRole>().Find(incomingEntity.Id);

                if (existingEntity == null)
                {
                    if (uow.Repository<SecurityRole>().Query(x => x.Id != dto.Id && string.Compare(x.Name.Trim(), dto.Name.Trim(), true) == 0).Select().Any())
                    {
                        return NaztecResponse.CreateError<NaztecCUDResponse<SecurityRoleDTO>>(string.Format("Security Role - {0} already exists.", dto.Name));
                    }

                    incomingEntity.Id = Guid.NewGuid();

                    uow.Repository<SecurityRole>().Add(incomingEntity);
                }
                else
                {
                    existingEntity.Id = dto.Id;
                    existingEntity.Name = dto.Name;

                    uow.Repository<SecurityRole>().Update(existingEntity);
                }

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<SecurityRoleDTO>>("Security Role saved successfully.");

                    response.Item = dto;

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<SecurityRoleDTO>>("Saving Security Role failed.");
            }
        }

        public ICollection<SecurityRoleDTO> GetSecurityRoles()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return uow.Repository<SecurityRole>()
                      .Query()
                      .Select()
                      .ProjectTo<SecurityRoleDTO>()
                      .ToList();
            }
        }

        public ICollection<SecurityMenuDisplayDTO> GetSecurityRoleMenu(Guid roleId)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return uow.Repository<AppMenu>().Query().Select(x => new SecurityMenuDisplayDTO()
                {
                    Text = x.Text,
                    Id = x.Id,
                    ParentId = x.ParentId,
                    DisplayOrder = x.DisplayOrder,
                    State = new StateDTO() { Selected = !x.ChildMenu.Any() && x.SecurityRoleMenus.Any(z => z.RoleId == roleId && z.MenuId == x.Id) }
                }).OrderBy(x => x.DisplayOrder).ToList();
            }
        }

        public NaztecResponse SaveSecurityRoleMenus(SecurityRoleMenuDTO dto)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var itemsToDelete = uow.Repository<SecurityRoleMenu>().Query(x => x.RoleId == dto.SecurityRoleId).Select();

                foreach (var itemToDelete in itemsToDelete)
                {
                    uow.Repository<SecurityRoleMenu>().Delete(itemToDelete);
                }

                foreach (var menu in dto.MenuList)
                {
                    var incomingEntity = new SecurityRoleMenu();

                    incomingEntity.Id = Guid.NewGuid();

                    incomingEntity.RoleId = dto.SecurityRoleId;

                    incomingEntity.MenuId = menu.Id;

                    uow.Repository<SecurityRoleMenu>().Add(incomingEntity);
                }

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess("Role Menu saved successfully.");

                    return response;
                }

                return NaztecResponse.CreateError("Saving Role Menu failed.");
            }
        }

        public NaztecResponse SaveSecurityRoleRights(SecurityRoleRightsDTO dto)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var itemsToDelete = uow.Repository<SecurityRoleRight>().Query(x => x.SecurityRoleId == dto.SecurityRoleId).Select();

                foreach (var itemToDelete in itemsToDelete)
                {
                    uow.Repository<SecurityRoleRight>().Delete(itemToDelete);
                }

                foreach (var securityRoleRight in dto.SecurityRightsList)
                {
                    var incomingEntity = new SecurityRoleRight();

                    incomingEntity.Id = Guid.NewGuid();

                    incomingEntity.SecurityRoleId = dto.SecurityRoleId;

                    incomingEntity.SecurityRightId = securityRoleRight.Id;

                    uow.Repository<SecurityRoleRight>().Add(incomingEntity);
                }

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess("Security Role Rights saved successfully.");

                    return response;
                }

                return NaztecResponse.CreateError("Saving Security Role Rights failed.");
            }
        }

        public ICollection<NaztecGuidList> GetSecurityRoleUsers(Guid roleId)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return this.GetSecurityRoleUsersInternal(uow, roleId);
            }
        }

        public ICollection<NaztecGuidList> GetUnAssignedUsers(Guid roleId)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return this.GetUnAssignedUsersInternal(uow, roleId);
            }
        }

        public NaztecCUDResponse<NaztecGuidList> RemoveUserFromRole(SecurityRoleUserDTO dto)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingItem = uow.Repository<SecurityUserRole>().Query(x => x.UserId == dto.UserId && dto.RoleId == dto.RoleId).Select().FirstOrDefault();

                if (existingItem == null)
                {
                    return NaztecResponse.CreateError<NaztecCUDResponse<NaztecGuidList>>("Security Role User Detail does not exist.");
                }

                uow.Repository<SecurityUserRole>().Delete(existingItem);

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<NaztecGuidList>>("Security Role User deleted successfully.");

                    var item = new NaztecGuidList();

                    item.Id = dto.UserId;

                    response.Item = item;

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<NaztecGuidList>>("Deleting Security Role User failed.");
            }
        }

        public NaztecCUDResponse<NaztecGuidList> AddUserToRole(SecurityRoleUserDTO dto)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var incomingEntity = Mapper.Map<SecurityUserRole>(dto);

                var existingEntity = uow.Repository<SecurityUserRole>().Query(x => x.UserId == dto.UserId && x.RoleId == dto.RoleId).Select().FirstOrDefault();

                if (existingEntity == null)
                {
                    uow.Repository<SecurityUserRole>().Add(incomingEntity);
                }
                else
                {
                    existingEntity.SetValues(incomingEntity);
                }

                if (uow.SaveChanges().IsOk)
                {
                    var response = NaztecResponse.CreateSuccess<NaztecCUDResponse<NaztecGuidList>>("Security Group User saved successfully.");

                    var item = new NaztecGuidList();

                    item.Id = dto.UserId;

                    return response;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<NaztecGuidList>>("Saving Security Group User failed.");
            }
        }

        private ICollection<NaztecGuidList> GetSecurityRoleUsersInternal(IUnitOfWork uow, Guid roleId)
        {
            return uow.Repository<SecurityUserRole>()
                      .Query(x => x.RoleId == roleId)
                      .Select(y => new NaztecGuidList { Id = y.SecurityUser.Id, Value = y.SecurityUser.AppUser.FirstName + " " + y.SecurityUser.AppUser.LastName })
                      .OrderBy(z => z.Value)
                      .ToList();
        }

        private ICollection<NaztecGuidList> GetUnAssignedUsersInternal(IUnitOfWork uow, Guid roleId)
        {
            return uow.Repository<SecurityUser>()
                           .Query(x => !x.Roles.Any(z => z.RoleId == roleId))
                           .Select(y => new NaztecGuidList
                           {
                               Id = y.Id,
                               Value = y.AppUser.FirstName + " " + y.AppUser.LastName
                           }).ToList();
        }
    }
}
