﻿namespace Naztec.Domain.Implementations
{
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using Core.Entities;
    using Core.Helpers;
    using Data.Contracts;
    using Data.Entities;
    using Naztec.Domain.Contracts;
    using Naztec.Domain.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public partial class SecurityManagementService : ISecurityManagementService
    {
        public List<UserDTO> GetUsers()
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return uow.Repository<AppUser>()
                            .Query()
                            .Select(x => new UserDTO()
                            {
                                Id = x.Id,
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                PhoneNumber = x.PhoneNumber,
                                ImageId = x.ImageId,
                                Email = x.SecurityUser.Email,
                                Roles = x.SecurityUser.Roles.Select(y => y.SecurityRole.Name).ToList(),
                                Timezone = x.Timezone                                
                            }).ToList();
            }
        }

        public async Task<NaztecCUDResponse<UserDTO>> CreateUser(UserDTO userDTO)
        {
            var creationResponse = new NaztecCUDResponse<UserDTO>();

            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingEntity = uow.Repository<SecurityUser>().Query(x => x.Email == userDTO.Email).Select().FirstOrDefault();

                if (existingEntity == null)
                {
                    uow.BeginTransaction();

                    #region Security User

                    var securityUser = new SecurityUser();

                    securityUser.Id = userDTO.Id;
                    securityUser.Email = userDTO.Email;
                    securityUser.UserName = userDTO.Email;
                    securityUser.IsPasswordChanged = false;
                    securityUser.EmailConfirmed = true;

                    var password = Helpers.GetRandomPassword();

                    var securityUserCreationResult = await this.userService.CreateAsync(securityUser, password);

                    #endregion

                    if (securityUserCreationResult.Succeeded)
                    {
                        uow.Repository<AppUser>().Add(Mapper.Map<AppUser>(userDTO));

                        if (uow.SaveChanges().IsOk)
                        {
                            uow.Commit();

                            var callBackUrl = await this.GenerateApplicationSiteCallBackUrlForSetPassword(securityUser.Id, securityUser.Email);
                            SendCustomerInvitationEmail(securityUser.Id, callBackUrl);

                            creationResponse.Item = userDTO;
                            creationResponse.Success("User was created successfully.");
                            return creationResponse;
                        }
                        else
                        {
                            uow.Rollback();

                            var createdSecurityUser = await this.userService.FindByIdAsync(securityUser.Id);
                            var securityUserDeletionResult = await this.userService.DeleteAsync(createdSecurityUser);

                            creationResponse.Error("Creating the User failed.");
                        }
                    }
                    else
                    {
                        Logger.Error("Creation of User Failed " + securityUserCreationResult.Errors.ToString());
                        creationResponse.Error("Creating the User failed.");
                    }

                    return creationResponse;
                }

                return NaztecResponse.CreateError<NaztecCUDResponse<UserDTO>>("User", string.Format("An user exists with the same email."));
            }
        }

        public NaztecCUDResponse<UserDTO> EditUser(UserDTO userDTO)
        {
            var response = new NaztecCUDResponse<UserDTO>();

            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var existingItem = uow.Repository<AppUser>().Find(userDTO.Id);

                if (existingItem != null)
                {
                    existingItem.FirstName = userDTO.FirstName;
                    existingItem.LastName = userDTO.LastName;
                    existingItem.PhoneNumber = userDTO.PhoneNumber;
                    existingItem.Timezone = userDTO.Timezone;
                }
                else
                {
                    response.Success(string.Format("No Record Found."));
                }

                if (uow.SaveChanges().IsOk)
                {
                    response.Item = userDTO;
                    response.Success("User information updated successfully.");

                    return response;
                }
                else
                {
                    response.Error("User information updated failed.");
                }
                return response;
            }
        }

        private async Task<string> GenerateApplicationSiteCallBackUrlForSetPassword(Guid securityUserId, string toEmail)
        {
            var token = await this.userService.GeneratePasswordResetTokenAsync(securityUserId);

            Uri baseuri = new Uri(this.configurationManager.GetValue<string>(NaztecConstants.ApplicationSiteUrl));

            Uri callbackUrl = new Uri(baseuri, "/Auth/ResetPassword?userName=" + toEmail + "&code=" + Uri.EscapeDataString(token));

            return callbackUrl.AbsoluteUri;
        }

        private NaztecResponse SendCustomerInvitationEmail(Guid appUserId, string callBackUrl)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var userEmail = this.BuildCustomerWelcomeEmail(uow, appUserId, callBackUrl);

                var mailResponse = this.emailService.SendMail(userEmail);

                if (mailResponse.IsOk)
                {
                    var newAppUser = uow.Repository<AppUser>().Find(appUserId);

                    uow.Repository<AppUser>().Update(newAppUser);

                    if (uow.SaveChanges().IsOk)
                    {
                        return NaztecResponse.CreateSuccess("Customer Welcome Email was sent Successfully!");
                    }
                }

                return NaztecResponse.CreateSuccess("Customer Welcome Email was not sent");
            }
        }

        private NaztecEmailItem BuildCustomerWelcomeEmail(IUnitOfWork uow, Guid appUserId, string callBackUrl)
        {
            NaztecEmailItem mail = new NaztecEmailItem();

            mail.IsHtml = true;
            mail.Sender = this.configurationManager.GetValue<string>(NaztecConstants.FromEMail);

            var appUsers = uow.Repository<AppUser>()
                                        .Query(s => s.Id == appUserId)
                                        .Select(x => new
                                        {
                                            Id = x.Id,
                                            Email = x.SecurityUser.Email,
                                            SecurityUserId = x.Id,
                                            FirstName = x.FirstName,
                                            LastName = x.LastName
                                        }).FirstOrDefault();

            mail.Recipient = appUsers.Email;

            string template = @"<html xmlns='http://www.w3.org/1999/xhtml'>
                                <head>
                                <meta http - equiv = 'Content-Type' content = 'text/html; charset=UTF-8' />
                                </head>
                                 <body style = 'background-color: #ffffff !important;'>
                                <table border = '0' cellpadding = '0' cellspacing = '0' height = '100%' width = '100%' id = 'bodyTable'>
                                <tr>
                                <td valign = 'top'>
                                  <table border = '0' cellpadding = '0' cellspacing = '0' width = '600' id = 'emailContainer' style='background-color: #ffffff !important;'>
                                    <tr>
                                      <td align = 'left' valign = 'top'>
                                         <a href = '{{SiteUrl}}' target = '_blank'>
                                            <img src = '' style = 'height:70px;display:block;margin: 10px 0 20px 0;'>
                                               </a>
                                                 </td>
                                                  </tr>
                                                    <tr>
                                                      <td align='left' valign='top'>
                                                       <table border='0' cellpadding='10' cellspacing='0' width='100%' id='emailBody'>
                                                         <tr valign='top'>
                                                           <td valign='top' style='padding-top:0px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;color:#333333;font-size:14px;line-height:24px;text-align:left;font-weight:none' align = 'left' > Hello {{firstName}} {{lastName}},
                                                            </td>
                                                           </tr>
                                                            <tr valign='top'>
                                                            <td valign='top' style='padding-top:10px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;color:#333333;font-size:14px;line-height:24px;text-align:left;font-weight:none' align = 'left' > I have added you to Naztec NET ORDER MARKETING, SALES,FINANCIAL & INVENTORY MANAGEMENT SYSTEM!Click the link below to get started by finishing up some simple screens.</td>
                                                                </tr>
                                                                     <tr>
                                                                         <td valign='top' align='left' style='padding-top:10px'>
                                                                                  <a href = '{{signUpLink}}' style = 'font-size:16px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;font-weight:none;color:#ffffff;text-decoration:none;background-color:#3572b0;border-top:2px solid #3572b0;border-bottom:11px solid #3572b0;border-radius:5px;display:inline-block;border-bottom:2px solid #234d77;border-radius:4px;color:#fff;padding:5px 15px;' target='_blank'> Go to Naztec Enterprise Application !</a>
                                                                                       </td>
                                                                                   </tr>
                                                                                     <tr valign = 'top'>
                                                                                        <td valign = 'top' style='padding-top:10px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;color:#333333;font-size:14px;line-height:24px;text-align:left;font-weight:none' align='left'>Cheers,
                                                                             <br> Naztec Tech Team
                                                                </td>
                                                            </tr>
                                                            <tr valign = 'top'>
                                                                 <td valign='top' style='padding-top:10px;font-family:Helvetica,Helvetica neue,Arial,Verdana,sans-serif;color:#333333;font-size:14px;line-height:24px;text-align:left;font-weight:none' align = 'left' > P.S.Having trouble with the Signup<a href = '{{signUpLink}}' target = '_blank'>Try this link instead</a>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </body>
                                            </html>'";

            mail.Subject = " Welcome " + appUsers.FirstName;

            var appSiteUrl = this.configurationManager.GetValue<string>(NaztecConstants.ApplicationSiteUrl);
            mail.MessageBody = template.Replace("{{firstName}}", appUsers.FirstName)
                                                    .Replace("{{lastName}}", appUsers.LastName)
                                                    .Replace("{{SiteUrl}}", appSiteUrl)
                                                    .Replace("{{signUpLink}}", callBackUrl);

            return mail;
        }
    }
}
