﻿namespace Naztec.Domain.Implementations
{
    using Naztec.Domain.Contracts;
    using System;
    using System.Configuration;

    public class ApplicationConfigurationManager : IApplicationConfigurationManager
    {
        public T GetValue<T>(string key)
        {
            var keyValue = ConfigurationManager.AppSettings[key];

            if (!string.IsNullOrWhiteSpace(keyValue))
            {
                return (T)Convert.ChangeType(keyValue, typeof(T));
            }

            return default(T);
        }
    }
}
