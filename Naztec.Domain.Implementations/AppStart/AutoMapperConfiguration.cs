namespace Naztec.Domain.Implementations.AppStart
{
    using System;
    using System.Linq;
    using System.Reflection;
    using AutoMapper;
    using StructureMap;
    using Naztec.Core.Contracts;

    public static class AutoMapperConfiguration
    {
        public static void Initialize(IContainer container)
        {
            Mapper.Configuration.ConstructServicesUsing((type) => container.GetInstance(type));

            var types = Assembly.GetCallingAssembly()
                                .GetExportedTypes()
                                .Union(
                                       Assembly.GetCallingAssembly()
                                               .GetReferencedAssemblies()
                                               .SelectMany(name => Assembly.Load(name).GetExportedTypes()));

            var maps = types.Where(x => typeof(IDefineMappings).IsAssignableFrom(x) &&
                                        !x.IsAbstract &&
                                        !x.IsInterface)
                            .Select(x => (IDefineMappings)Activator.CreateInstance(x))
                            .ToArray();

            foreach (var map in maps)
            {
                map.CreateMappings(Mapper.Configuration);
            }
        }
    }
}
