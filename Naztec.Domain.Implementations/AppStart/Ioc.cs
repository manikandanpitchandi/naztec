namespace Naztec.Domain.Implementations.AppStart
{
    using StructureMap;

    using Naztec.Data.Contracts;

    using Naztec.Domain.Contracts;
    using Naztec.Data.Implementations.UnitOfWorks;    

    public static class Ioc
    {
        public static void Configure(IContainer container, string connectionString)
        {
            container.Configure(x =>
                    {
                        x.For<IAdminUnitOfWorkFactory>().Use<AdminUnitOfWorkFactory>()
                                                 .Ctor<string>("connectionString")
                                                 .Is(connectionString);
                        x.For<IInventoryUnitOfWorkFactory>().Use<InventoryUnitOfWorkFactory>()
                                                .Ctor<string>("connectionString")
                                                .Is(connectionString);

                        x.For<IAuthService>().Use<AuthService>();
                        x.For<IListService>().Use<ListService>();
                        x.For<IEmailManager>().Use<EmailManager>();
                        x.For<IApplicationConfigurationManager>().Use<ApplicationConfigurationManager>();
                        x.For<IProfileService>().Use<ProfileService>();
                        x.For<IMenuService>().Use<MenuService>();
                        x.For<IScriptService>().Use<ScriptService>();
                        x.For<ISecurityManagementService>().Use<SecurityManagementService>();
                        x.For<ILogService>().Use<LogService>();
                        x.For<IHangfireWrapper>().Use<HangfireWrapper>();
                        x.For<IEmailService>().Use<EmailService>();
                        x.For<IInventoryService>().Use<InventoryService>();
                    });
        }
    }
}
