namespace Naztec.Domain.Implementations.Security
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin;
    using Microsoft.Owin.Security;

    using Naztec.Data.Entities;

    public class SecuritySignInServices : SignInManager<SecurityUser, Guid>
    {
        public SecuritySignInServices(SecurityUserServices userManager, IAuthenticationManager authenticationManager) :
            base(userManager, authenticationManager) 
        { 
        }

        public static SecuritySignInServices Create(IdentityFactoryOptions<SecuritySignInServices> options, IOwinContext context)
        {
            return new SecuritySignInServices(context.GetUserManager<SecurityUserServices>(), context.Authentication);
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(SecurityUser user)
        {
            return user.GenerateUserIdentityAsync((SecurityUserServices)UserManager);
        }        
    }
}
