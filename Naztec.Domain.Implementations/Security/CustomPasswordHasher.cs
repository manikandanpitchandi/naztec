namespace Naztec.Domain.Implementations.Security
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;

    //using log4net;

    using Microsoft.AspNet.Identity;

    public class CustomPasswordHasher : PasswordHasher
    {
        //private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override string HashPassword(string password)
        {
            return base.HashPassword(password);
        }

        public override PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            var passwordProperties = hashedPassword.Split('|');
            if (passwordProperties.Length != 2)
            {
                return base.VerifyHashedPassword(hashedPassword, providedPassword);
            }
            else
            {
                var passwordHash = passwordProperties[0];

                if (string.Equals(EncryptPassword(providedPassword, "&%#@?,:*"), passwordHash, StringComparison.CurrentCultureIgnoreCase))
                {
                    return PasswordVerificationResult.Success;
                }
                else
                {
                    return PasswordVerificationResult.Failed;
                }
            }
        }

        public string EncryptText(string strText)
        {
            return EncryptPassword(strText, "&%#@?,:*");
        }

        private static string EncryptPassword(string strText, string strEncrKey)
        {
            byte[] byKey = { };
            byte[] IV = { 18, 52, 86, 120, 144, 171, 205, 239 };

            try
            {
                byKey = Encoding.UTF8.GetBytes(strEncrKey.Substring(0, 8));

                using (var des = new DESCryptoServiceProvider())
                {
                    byte[] inputByteArray = Encoding.UTF8.GetBytes(strText);
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write))
                        {
                            cs.Write(inputByteArray, 0, inputByteArray.Length);
                            cs.FlushFinalBlock();
                            return Convert.ToBase64String(ms.ToArray());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.Error(ex);

                return ex.Message;
            }
        }
    }
}
