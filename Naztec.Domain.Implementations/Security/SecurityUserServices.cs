namespace Naztec.Domain.Implementations.Security
{
    using System;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin;

    using Naztec.Data.Entities;
    using Naztec.Data.Implementations.UnitOfWorks;
    using Security;

    public class SecurityUserServices : UserManager<SecurityUser, Guid>
    {
        public SecurityUserServices(IUserStore<SecurityUser, Guid> store)
            : base(store)
        {
            this.PasswordHasher = new CustomPasswordHasher();
        }

        public static SecurityUserServices Create(IdentityFactoryOptions<SecurityUserServices> options, IOwinContext context)
        {
            var dbContext = context.Get<SecurityIdentityContext>();
            return Create(options, dbContext);
        }

        public static SecurityUserServices Create(IdentityFactoryOptions<SecurityUserServices> options, SecurityIdentityContext dbContext)
        {
            var manager = new SecurityUserServices(new SecurityUserRepository(dbContext));

            manager.UserValidator = new UserValidator<SecurityUser, Guid>(manager) { AllowOnlyAlphanumericUserNames = false, RequireUniqueEmail = true };

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            manager.UserLockoutEnabledByDefault = true;

            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);

            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            manager.RegisterTwoFactorProvider(
                "PhoneCode",
                new PhoneNumberTokenProvider<SecurityUser, Guid>
                {
                    MessageFormat = "Your security code is: {0}"
                });

            manager.RegisterTwoFactorProvider(
                "EmailCode",
                new EmailTokenProvider<SecurityUser, Guid>
                {
                    Subject = "SecurityCode",
                    BodyFormat = "Your security code is {0}"
                });

            var dataProtectionProvider = options.DataProtectionProvider;

            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<SecurityUser, Guid>(
                        dataProtectionProvider.Create("ASP.NET Identity"));
            }

            return manager;
        }
    }
}
