namespace Naztec.Domain.Implementations.Security
{
    using System;
    using System.Security.Claims;
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity;
    using Naztec.Data.Entities;

    public static class SecurityIdentityExtensions
    {
        public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(this SecurityUser user, UserManager<SecurityUser, Guid> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);            

            // Add custom user claims here
            return userIdentity;
        }
    }
}
