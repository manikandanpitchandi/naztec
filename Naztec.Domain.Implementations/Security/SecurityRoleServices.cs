namespace Naztec.Domain.Implementations.Security
{
    using System;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin;
    using Naztec.Data.Entities;
    using Naztec.Data.Implementations.UnitOfWorks;

    public class SecurityRoleServices : RoleManager<SecurityRole, Guid>
    {        
        public SecurityRoleServices(IRoleStore<SecurityRole, Guid> roleStore)
            : base(roleStore)
        {
        }

        public static SecurityRoleServices Create(IdentityFactoryOptions<SecurityRoleServices> options, IOwinContext context)
        {
            return new SecurityRoleServices(
                new SecurityRoleRepository(context.Get<SecurityIdentityContext>()));
        }
    }
}
