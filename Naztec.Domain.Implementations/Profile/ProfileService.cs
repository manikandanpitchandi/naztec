﻿using AutoMapper;
using Naztec.Core.Entities;
using Naztec.Data.Contracts;
using Naztec.Data.Entities;
using Naztec.Domain.Contracts;
using Naztec.Domain.Entities;
using System;
using System.Linq;

namespace Naztec.Domain.Implementations
{
    public class ProfileService : IProfileService
    {
        private IAdminUnitOfWorkFactory uowFactory;
      
        public ProfileService(IAdminUnitOfWorkFactory uowFactory)
        {
            this.uowFactory = uowFactory;
        }

        public NaztecResponse SaveProfile(UserProfile item)
        {
            var response = new NaztecResponse();

            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var securityUser = uow.Repository<SecurityUser>().Query(x => x.Email == item.Email).Select().FirstOrDefault();

                var incomingEntity = Mapper.Map<AppUser>(item);

                incomingEntity.Id = securityUser.Id;

                var existingEntity = uow.Repository<AppUser>().Find(incomingEntity.Id);

                if (existingEntity == null)
                {
                    uow.Repository<AppUser>().Add(incomingEntity);
                }
                else
                {
                    existingEntity.SetValues(incomingEntity);
                }

                if (uow.SaveChanges().IsOk)
                {
                    uow.Commit();
                    return NaztecResponse.CreateSuccess("Profile Info added successfully!");
                }
                else
                {
                    uow.Rollback();

                    return NaztecResponse.CreateError("Profile Info addition failed.");
                }
            }
        }

        public NaztecResponse SaveTheme(string themeCss)
        {
            NaztecResponse response = new NaztecResponse();

            return response;
        }

        public UserProfile GetProfile(string email)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                return uow.Repository<AppUser>()
                            .Query(x => x.SecurityUser.Email == email)
                            .Select(x => new UserProfile
                            {
                                FirstName = x.FirstName,
                                LastName = x.LastName,
                                PhoneNumber = x.PhoneNumber,
                                ImageId = x.ImageId,
                                ProfileImage = x.ProfileImage
                            }).FirstOrDefault();

            }
        }

        public NaztecCUDResponse<ProfileImageItem> AddImages(ProfileImageItem item)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var response = new NaztecCUDResponse<ProfileImageItem>();

                var existingItem = uow.Repository<AppUser>().Find(item.AppUserId);

                if (existingItem.ImageId.ToString() != null)
                {
                    item.PreviousImageName = existingItem.ImageId;
                }

                if (existingItem != null)
                {
                    existingItem.ImageId = item.ImageName;
                    existingItem.ProfileImage = item.ImageBinaryData;
                }
                else
                {
                    return NaztecResponse.CreateError<NaztecCUDResponse<ProfileImageItem>>(string.Format("No Record Found."));
                }

                if (uow.SaveChanges().IsOk)
                {
                    response = NaztecResponse.CreateSuccess<NaztecCUDResponse<ProfileImageItem>>("Profile Image saved successfully.");

                    response.Item = item;

                    return response;
                }

                return response;
            }
        }

        public UserProfile GetProfileImageByImageName(Guid ImageName)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var response = new NaztecCUDResponse<UserProfile>();

                var existingItem = uow.Repository<AppUser>().Query(x => x.ImageId == ImageName).Select().FirstOrDefault();

                return Mapper.Map<UserProfile>(existingItem);
            }
        }

        public byte[] GetImageinByteArray(string imageString)
        {
            if (imageString != null && imageString != string.Empty)
            {
                string[] words = imageString.Split(',');

                return Convert.FromBase64String(words[1]);
            }

            return null;
        }


    }
}
