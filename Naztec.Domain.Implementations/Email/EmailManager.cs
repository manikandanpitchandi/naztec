﻿using Naztec.Core.Entities;
using System;
using System.Collections.Generic;
using log4net;
using Naztec.Domain.Contracts;
using Naztec.Domain.Entities;
using System.Net.Mail;

namespace Naztec.Domain.Implementations
{
    public class EmailManager : IEmailManager
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string mailServer;

        private int mailServerPort;

        private string mailServerUsername;

        private string mailServerPassword;

        private bool sendEmail;

        private int? mailPause;

        public EmailManager(IApplicationConfigurationManager configurationManager)
        {
            if (!string.IsNullOrWhiteSpace(configurationManager.GetValue<string>(NaztecConstants.IsTestEnvironment)) && configurationManager.GetValue<string>(NaztecConstants.IsTestEnvironment) == "false")
            {
                if (!string.IsNullOrWhiteSpace(configurationManager.GetValue<string>(NaztecConstants.SMTPServer)))
                {
                    this.mailServer = configurationManager.GetValue<string>(NaztecConstants.SMTPServer);
                }

                if (!string.IsNullOrWhiteSpace(configurationManager.GetValue<string>(NaztecConstants.SMTPServerPort)))
                {
                    this.mailServerPort = configurationManager.GetValue<int>(NaztecConstants.SMTPServerPort);
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(configurationManager.GetValue<string>(NaztecConstants.TestSMTPServer)))
                {
                    this.mailServer = configurationManager.GetValue<string>(NaztecConstants.TestSMTPServer);
                }

                if (!string.IsNullOrWhiteSpace(configurationManager.GetValue<string>(NaztecConstants.TestSMTPServerPort)))
                {
                    this.mailServerPort = configurationManager.GetValue<int>(NaztecConstants.TestSMTPServerPort);
                }

                if (!string.IsNullOrWhiteSpace(configurationManager.GetValue<string>(NaztecConstants.TestSMTPServerUsername)))
                {
                    this.mailServerUsername = configurationManager.GetValue<string>(NaztecConstants.TestSMTPServerUsername);
                }

                if (!string.IsNullOrWhiteSpace(configurationManager.GetValue<string>(NaztecConstants.TestSMTPServerPassword)))
                {
                    this.mailServerPassword = configurationManager.GetValue<string>(NaztecConstants.TestSMTPServerPassword);
                }
            }

            if (!string.IsNullOrWhiteSpace(configurationManager.GetValue<string>("SendEMail")))
            {
                this.sendEmail = configurationManager.GetValue<bool>("SendEMail");
            }
        }

        public NaztecResponse SendMail(string sender, string recipient, string subject, string message, bool isHtml)
        {
            return this.SendMail(sender, recipient, new List<string>(), subject, message, isHtml);
        }

        public NaztecResponse SendMail(string sender, string recipient, List<string> copyTo, string subject, string message, bool isHtml)
        {
            var response = new NaztecResponse();

            if (this.sendEmail)
            {
                try
                {
                    using (var mailClient = new SmtpClient())
                    {
                        using (var mailMessage = new MailMessage())
                        {
                            mailMessage.IsBodyHtml = isHtml;
                            mailMessage.From = new MailAddress(sender);
                            mailMessage.To.Add(new MailAddress(recipient));
                            mailMessage.Body = message;
                            mailMessage.Subject = subject;
                            mailMessage.Priority = MailPriority.Normal;

                            mailClient.Host = this.mailServer;
                            mailClient.Port = this.mailServerPort;

                            if (!string.IsNullOrWhiteSpace(this.mailServerUsername) && !string.IsNullOrWhiteSpace(this.mailServerPassword))
                            {
                                mailClient.Credentials = new System.Net.NetworkCredential(this.mailServerUsername, this.mailServerPassword);
                                mailClient.EnableSsl = false;
                            }

                            foreach (var cc in copyTo)
                            {
                                mailMessage.CC.Add(cc);
                            }

                            mailClient.Send(mailMessage);
                            System.Threading.Thread.Sleep(this.mailPause ?? 0);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex);

                    response.Error("Sending email failed");
                }
            }

            return response;
        }

        public NaztecResponse SendMail(NaztecEmailItem item)
        {
            return this.SendMail(item.Sender, item.Recipient, item.CC, item.Subject, item.MessageBody, item.IsHtml);
        }
    }
}
