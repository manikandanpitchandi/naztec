﻿namespace Naztec.Domain.Implementations
{
    using Naztec.Domain.Contracts;
    using Hangfire;
    using Hangfire.Annotations;
    using System;
    using System.Linq.Expressions;

    public class HangfireWrapper : IHangfireWrapper
    {
        public string Schedule<T>(Expression<Action<T>> methodCall, DateTimeOffset enqueueAt)
        {
            return BackgroundJob.Schedule<T>(methodCall, enqueueAt);
        }

        public string Enqueue<T>(Expression<Action<T>> methodCall)
        {
            return BackgroundJob.Enqueue<T>(methodCall);
        }

        public string Schedule<T>([InstantHandle][NotNull]Expression<Action<T>> methodCall, TimeSpan delay)
        {
            return BackgroundJob.Schedule<T>(methodCall, delay);
        }
    }
}
