﻿namespace Naztec.Domain.Implementations
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper.QueryableExtensions;
    using Contracts;
    using Naztec.Data.Contracts;
    using Naztec.Data.Entities;
    using Naztec.Domain.Entities;

    public class InventoryService : IInventoryService
    {
        private IInventoryUnitOfWorkFactory uowFactory;

        public InventoryService(IInventoryUnitOfWorkFactory uowFactory)
        {
            this.uowFactory = uowFactory;
        }


        public List<InventoryDTO> GetInventory()
        {
            using (IUnitOfWork uow = uowFactory.GetUnitOfWork())
            {
                return uow.Repository<Inventory>().Query()
                               .Select()
                               .ProjectTo<InventoryDTO>()
                               .ToList();
            }

        }
    }
}
