namespace Naztec.Domain.Implementations
{
    using Data.Entities;
    using Naztec.Data.Contracts;
    using Naztec.Domain.Contracts;
    using Naztec.Domain.Implementations.Security;
    using Entities;
    using log4net;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using System;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public class AuthService : IAuthService
    {
        private IAdminUnitOfWorkFactory uowFactory;
        private SecurityUserServices userService;
        private SecuritySignInServices signInService;
        private SecurityRoleServices roleService;

        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public AuthService(IAdminUnitOfWorkFactory uowFactory, SecurityUserServices userService, SecuritySignInServices signInService, SecurityRoleServices roleService)
        {
            this.uowFactory = uowFactory;
            this.userService = userService;
            this.signInService = signInService;
            this.roleService = roleService;
        }

        public async Task<LoginResponse> AppLogin(LoginRequest item)
        {
            LoginResponse response = new LoginResponse();

            if (string.IsNullOrEmpty(item.UserName) || string.IsNullOrEmpty(item.Password))
            {
                response.Error("Username or Password cannot be blank!");
                return response;
            }

            response.FullName = item.UserName;

            var signInStatus = await this.signInService.PasswordSignInAsync(item.UserName, item.Password, false, true);
            var identityUser = await this.userService.FindByNameAsync(item.UserName);

            switch (signInStatus)
            {
                case SignInStatus.Success:
                    if (identityUser != null && identityUser.EmailConfirmed)
                    {
                        response.SecurityUserId = identityUser.Id;
                        response.UserClaims = await identityUser.GenerateUserIdentityAsync(this.userService);

                        using (var uow = this.uowFactory.GetUnitOfWork())
                        {
                            var ecBaseUser = uow.Repository<AppUser>()
                                                    .Query(x => x.Id == identityUser.Id)
                                                    .Select(x => new
                                                    {
                                                        Id = x.Id,
                                                        FullName = x.FirstName + " " + x.LastName,
                                                        ImageId = x.ImageId != null ? x.ImageId : null,
                                                    }).FirstOrDefault();

                            response.Roles = uow.Repository<SecurityUserRole>()
                                                    .Query(x => x.UserId == ecBaseUser.Id)
                                                    .Select(x => x.RoleId).ToList();

                            if (ecBaseUser == null)
                            {
                                Log.Error("Security user doesn't have a matching User record.");
                                response.Error("Invalid Username / Password");
                                return response;
                            }
                            response.FullName = ecBaseUser.FullName;
                            response.ImageId = ecBaseUser.ImageId;
                        }
                    }
                    return response;
                case SignInStatus.Failure:
                    if (identityUser != null && identityUser.AccessFailedCount > 2)
                    {
                        var remainingAttempts = this.userService.MaxFailedAccessAttemptsBeforeLockout - identityUser.AccessFailedCount;

                        response.Error(string.Format("Invalid credentials. You have {0} more attempt(s) before your account gets locked out.", remainingAttempts.ToString()));
                        return response;
                    }

                    response.Error("Invalid Username/Password");
                    Log.Error("Security user doesn't have a matching User record.");
                    return response;
                case SignInStatus.LockedOut:
                    response.Error(string.Format("Your account has been locked out for {0} minutes due to multiple failed login attempts.", TimeSpan.Parse(this.userService.DefaultAccountLockoutTimeSpan.ToString()).Minutes));
                    return response;
            }

            return response;
        }

        public async Task<ForgotPasswordResponse> ForgotPassword(ForgotPassword item)
        {
            ForgotPasswordResponse response = new ForgotPasswordResponse();

            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var securityUser = uow.Repository<SecurityUser>().Query(x => x.Email == item.Email).Select().FirstOrDefault();

                if (securityUser == null)
                {
                    response.Error("Invalid User");

                    return response;
                }

                if (securityUser.Email != item.Email)
                {
                    response.Error("Mail Not Matching!!");

                    return response;
                }

                var token = await this.userService.GeneratePasswordResetTokenAsync(securityUser.Id);

                if (string.IsNullOrWhiteSpace(token))
                {
                    response.Error("Password reset token failed");
                    return response;
                }

                response.UserName = securityUser.UserName;
                response.Email = securityUser.Email;
                response.Token = token;

                return response;
            }
        }

        public string EmailBodyForForgotPassword(string userName, string callbackUrl)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                var name = uow.Repository<AppUser>().Query(x => x.Id == x.SecurityUser.Id && x.SecurityUser.UserName == userName).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();

                return this.EmailBuilderForForgotPassword(callbackUrl, name);
            }
        }

        public async Task<ResetPasswordResponse> ResetPassword(ResetPassword item)
        {
            ResetPasswordResponse response = new ResetPasswordResponse();

            var user = await this.userService.FindByEmailAsync(item.Email);

            if (user == null)
            {
                response.Error("Invalid User");

                return response;
            }

            if (item.Password != item.ConfirmPassword)
            {
                response.Error("Password & Confirm Password not Matching!!");

                return response;
            }

            var passowrdResult = await this.userService.PasswordValidator.ValidateAsync(item.Password);

            if (!passowrdResult.Succeeded)
            {
                response.Error(passowrdResult);
                return response;
            }

            var result = this.userService.ResetPassword(user.Id, item.Code, item.Password);

            if (!result.Succeeded)
            {
                response.Error("Reset Password Failed");

                return response;
            }

            user.IsPasswordChanged = true;

            result = await this.userService.UpdateAsync(user);

            if (!result.Succeeded)
            {
                response.Error("Reset Password Failed");

                return response;
            }

            return response;
        }

        private string EmailBuilderForForgotPassword(string callbackUrl, string userName)
        {
            StringBuilder sbMail = new StringBuilder();

            sbMail.Append("Dear " + userName + ",");

            sbMail.Append("<br /><br />");

            sbMail.Append("You are receiving this email as you requested that your password be changed. You may reset your password by following this <a href=\"" + callbackUrl + "\">link</a>.");

            sbMail.Append("<br /><br />");

            sbMail.Append("Thank you,");

            sbMail.Append("<br />");

            sbMail.Append("Support Team.");

            return sbMail.ToString();
        }

        public async Task<LoginResponse> ChangePassword(ChangePassword item)
        {
            using (var uow = this.uowFactory.GetUnitOfWork())
            {
                LoginResponse response = new LoginResponse();

                var securityUser = uow.Repository<SecurityUser>().Query(x => x.UserName == item.EmailId).Select().FirstOrDefault();

                if (securityUser == null)
                {
                    response.Error("Invalid Username");
                    return response;
                }

                var identityUser = await this.userService.FindAsync(securityUser.UserName, item.OldPassword);

                if (item.Password != item.ConfirmPassword)
                {
                    response.Error("Password & Confirm Password not Matching!!");
                }

                var passwordResult = await this.userService.PasswordValidator.ValidateAsync(item.Password);

                if (!passwordResult.Succeeded)
                {
                    response.Error(passwordResult);
                    return response;
                }

                var userResult = await this.userService.ChangePasswordAsync(securityUser.Id, item.OldPassword, item.Password);

                if (!userResult.Succeeded)
                {
                    response.Error(userResult);

                    return response;
                }
                else
                {
                    if (uow.SaveChanges().IsOk)
                    {
                        response.Success("Password Changed SuccessFully");
                    }
                    else
                    {
                        response.Error("Operation Failed!!");
                    }
                }

                return response;
            }
        }

        public async Task<LoginResponse> WriteToUserClaims(Guid securityUserId, string claimType, string claimValue)
        {
            LoginResponse response = new LoginResponse();

            var claims = await this.userService.GetClaimsAsync(securityUserId);

            var skinClaim = claims.FirstOrDefault(c => c.Type == claimType);
            if (skinClaim != null)
                await this.userService.RemoveClaimAsync(securityUserId, skinClaim);

            skinClaim = new System.Security.Claims.Claim(claimType, claimValue);
            await this.userService.AddClaimAsync(securityUserId, skinClaim);

            var identityUser = this.userService.FindById(securityUserId);
            response.UserClaims = await identityUser.GenerateUserIdentityAsync(this.userService);
            response.Success("Skin selection was updated.");

            return response;
        }
    }
}
